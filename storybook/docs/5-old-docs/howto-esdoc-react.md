# How to write ESDoc React annotations

> ESDoc annotations of React components depend on their type.

## Functional Components

_Functional Components_ are pretty straighforward, `props` and `defaultProps`
are tagged like any plain object with default values.

### Example

    /**
     * Greeting component.
     *
     * @param {Object} props
     *   A destructured object itself cannot be accessed,
     *   so its identifier is arbitrary,
     *   but needs to be declared nevertheless.
     * @param {string} props.name
     *   The name of the greetee.
     * @param {string} [props.type=Hello]
     *   The type of greeting.
     * @return {ReactElement}
     */
     const Hello = ({ name, type = 'Hello' } = {}) => (
       <h1>{type}, {name}!</h1>
     );

## Class Components

Class `props` are documented with `@reactProps` provided by `esdoc-react-plugin`
(tag `defaultProps` as `@ignored`).

### Example

    /**
     * Greeting component.
     *
     * @reactProps {string} name
     *   The name of the greetee.
     * @reactProps {string} [type=Hello]
     *   The type of greeting.
     */
    class Hello extends Component {
      /**
       * @ignore
       */
      static get defaultProps() {
        return {
          type: 'Hello',
        };
      }

      /**
       * @return {ReactElement}
       */
      render() {
        const { name, type } = this.props;

        return (
          <h1>{type}, {name}!</h1>
        );
      }
    }
