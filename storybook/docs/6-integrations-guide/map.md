# External UI Map component FAQ

### How to configure Zaaksystem to use externally hosted UI map component

Go to administration panel > koppelingen > kaart configuratie
Provide the url under which your application is hosted
The url will be used in iframe pointing to the content.

### What map needs to do to be working

It needs to handle and send messages through `postMessage` API.
To listen to events happening in zaaksysteem the listener has to be added to the window
`window.addEventListener('message', event => ...)`
To let Zaaksysteem know of changes happening on the map (eg. drawing features) post message has to be called
`window.top.postMessage(message, window.location.origin)`

Zaaksysteem will send events on following shapes:

```typescript
{
  data: {
    type: 'init',
    name: string,
    version: 5
    value: {
      center: [number, number],
      wmsLayers: { url: string; layers: string }[],
      initialFeature: GeoJSON.GeoJsonObject | null,
      canDrawFeatures: boolean,
      context: {
        data: { object, objectType, case, caseType }
        type:
        | 'ObjectMap'
        | 'ObjectView'
        | 'CaseForm'
        | 'CaseObjectForm'
        | 'CaseRegistrationForm'
        | 'CaseMap'
      }
    }
  }
} | {
  data: {
    type: 'setFeature';
    name: string;
    version: 5;
    value: GeoJSON.GeoJsonObject | null;
  }
} | {
  data: {
    type: 'setMarker';
    name: string;
    version: 5;
    value: GeoJSON.Point | null;
  }
}
```

Zaaksysteem will listen to messages of following type

```typescript
{
  type: 'featureChange';
  name: string;
  version: 5;
  value: GeoJSON.GeoJsonObject | null;
} | {
  type: 'click';
  name: string;
  version: 5;
  value: GeoJSON.Point;
}
```

### Useful links

- GeoJSON specification: https://tools.ietf.org/html/rfc7946#section-11.1
- Geo JSON typescript typings: https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/geojson/index.d.ts
- Default Zaaksysteem map implementation: https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/-/tree/master/apps/external-components/src/map.ts
- Internal Types: https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/-/tree/master/packages/ui/types/MapIntegration.ts
