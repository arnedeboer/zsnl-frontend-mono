// Generated on: Thu Sep 09 2021 10:49:52 GMT+0200 (Central European Summer Time)
// Environment used: https://development.zaaksysteem.nl
//
// This file was automatically generated. DO NOT MODIFY IT BY HAND.
// Instead, rerun generation of Communication domain.

/* eslint-disable */
export namespace APICommunication {
  export interface ThreadListRequestParams {
    filter?: {
      contact_uuid?: string;
      case_uuid?: string;
      message_types?: 'external' | 'contact_moment' | 'note';
    };
    [k: string]: any;
  }

  export interface ThreadListResponseBody {
    data: {
      id: string;
      type: 'message_thread';
      meta: {
        created: string;
        last_modified: string;
        number_of_messages: number;
        summary: string;
        unread_employee_count: number;
        unread_pip_count: number;
        attachment_count: number;
      };
      attributes: {
        thread_type:
          | 'note'
          | 'contact_moment'
          | 'pip_message'
          | 'email'
          | 'postex';
        last_message:
          | {
              message_type: 'note';
              slug: string;
              created_name: string;
              created: string;
            }
          | {
              message_type: 'contact_moment';
              slug: string;
              direction: 'incoming' | 'outgoing';
              channel:
                | 'assignee'
                | 'frontdesk'
                | 'phone'
                | 'mail'
                | 'email'
                | 'webform'
                | 'social_media'
                | 'external_application';
              created_name: string;
              recipient_name: string;
              created: string;
            }
          | {
              message_type: 'email';
              slug: string;
              subject: string;
              created_name: string;
              created: string;
              failure_reason?: string;
            }
          | {
              message_type: 'pip_message';
              slug: string;
              subject: string;
              created_name: string;
              created: string;
            }
          | {
              message_type: 'postex';
              slug: string;
              subject: string;
              created_name: string;
              created: string;
              failure_reason?: string;
            };
        [k: string]: any;
      };
      relationships?: {
        contact?: {
          data: {
            id: string;
            type: 'contact';
            attributes: {
              type: 'employee' | 'person' | 'organization';
              name: string;
              address: string | null;
              email_address: string | null;
            };
          };
        };
        case?: {
          data: {
            id: string;
            type: 'case';
            attributes: {
              display_id: number;
              description: string | null;
              case_type_name: string;
              status: 'new' | 'open' | 'stalled' | 'resolved';
            };
          };
        };
      };
      links: {
        thread_messages: {
          href: string;
        };
      };
    }[];
  }

  export interface SearchContactRequestParams {
    keyword: string;
    filter?: {
      type?: ('employee' | 'person' | 'organization')[];
    };
    [k: string]: any;
  }

  export interface SearchContactResponseBody {
    data: {
      id: string;
      type: 'contact';
      attributes: {
        type: 'employee' | 'person' | 'organization';
        name: string;
        address: string | null;
        email_address: string | null;
      };
    }[];
  }

  export interface GetMessageListRequestParams {
    thread_uuid: string;
    [k: string]: any;
  }

  export interface GetMessageListResponseBody {
    data: (
      | {
          id: string;
          type: 'contact_moment';
          meta: {
            last_modified?: string;
            created: string;
            message_date?: string;
            summary: string;
          };
          attributes: {
            content: string;
            direction: 'incoming' | 'outgoing';
            channel:
              | 'assignee'
              | 'frontdesk'
              | 'phone'
              | 'mail'
              | 'email'
              | 'webform'
              | 'social_media'
              | 'external_application';
          };
          relationships: {
            created_by: {
              data: {
                id: string;
                type: 'contact';
                attributes: {
                  type: 'employee' | 'person' | 'organization';
                  name: string;
                  address: string | null;
                  email_address: string | null;
                };
              };
            };
            recipient: {
              data: {
                id: string;
                type: 'contact';
                attributes: {
                  type: 'employee' | 'person' | 'organization';
                  name: string;
                  address: string | null;
                  email_address: string | null;
                };
              };
            };
            thread: {
              data: {
                id: string;
                type: 'message_thread';
              };
            };
          };
        }
      | {
          id: string;
          type: 'note';
          meta: {
            last_modified?: string;
            created: string;
            message_date?: string;
            summary: string;
          };
          attributes: {
            content: string;
          };
          relationships: {
            created_by: {
              data: {
                id: string;
                type: 'contact';
                attributes: {
                  type: 'employee' | 'person' | 'organization';
                  name: string;
                  address: string | null;
                  email_address: string | null;
                };
              };
            };
            thread: {
              data: {
                id: string;
                type: 'message_thread';
              };
            };
          };
        }
      | {
          id: string;
          type: 'external_message';
          meta: {
            last_modified?: string;
            created: string;
            message_date?: string;
            read_employee: string | null;
            read_pip: string | null;
            summary: string;
            attachment_count: number;
            failure_reason?: string;
          };
          attributes: {
            message_type: 'pip' | 'email';
            content: string;
            subject: string | null;
            participants?: {
              role: 'to' | 'from' | 'cc' | 'bcc';
              display_name: string;
              address: string;
              /**
               * UUID of the participant(uuid of employee/person/organization)
               */
              uuid?: string;
            }[];
            is_imported?: boolean;
          };
          relationships: {
            created_by?: {
              data: {
                id: string;
                type: 'contact';
                attributes: {
                  type: 'employee' | 'person' | 'organization';
                  name: string;
                  address: string | null;
                  email_address: string | null;
                };
              };
            };
            thread: {
              data: {
                id: string;
                type: 'message_thread';
              };
            };
            attachments: {
              data: {
                id: string;
                type: 'Attachment';
                attributes: {
                  name: string;
                  md5: string;
                  mimetype: string;
                  size: number;
                  date_created: string;
                };
                links: {
                  download: {
                    href: string;
                  };
                  preview?: {
                    href: string;
                    meta: {
                      'content-type'?: string;
                      [k: string]: any;
                    };
                  };
                };
              }[];
            };
          };
        }
    )[];
    relationships?: {
      case?: {
        data: {
          id: string;
          type: 'case';
          attributes: {
            display_id: number;
            description: string | null;
            case_type_name: string;
            status: 'new' | 'open' | 'stalled' | 'resolved';
          };
        };
      };
      [k: string]: any;
    };
  }

  export interface ImportEmailMessageResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface ImportEmailMessageRequestBody {
    file_uuid: string;
    case_uuid: string;
  }

  export interface CreateContactMomentResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface CreateContactMomentRequestBody {
    contact_moment_uuid?: string;
    thread_uuid: string;
    case_uuid?: string | null;
    contact_uuid: string;
    channel:
      | 'assignee'
      | 'frontdesk'
      | 'phone'
      | 'mail'
      | 'email'
      | 'webform'
      | 'social_media'
      | 'external_application';
    content: string;
    direction: 'incoming' | 'outgoing';
  }

  export interface CreateNoteResponseBody {
    data: {
      success: boolean;
    };
  }

  export type CreateNoteRequestBody = {
    note_uuid: string;
    thread_uuid: string;
    content: string;
  } & (
    | {
        case_uuid: string;
      }
    | {
        contact_uuid: string;
      }
  );

  export interface SearchCaseRequestParams {
    search_term: string;
    minimum_permission: 'search' | 'read' | 'write' | 'manage';
    filter?: {
      status?: ('new' | 'open' | 'stalled' | 'resolved')[];
    };
    limit?: number;
    [k: string]: any;
  }

  export interface SearchCaseResponseBody {
    data: {
      id: string;
      type: 'case';
      attributes: {
        display_id: number;
        description: string | null;
        case_type_name: string;
        status: 'new' | 'open' | 'stalled' | 'resolved';
      };
    }[];
  }

  export interface CreateExternalMessageResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface CreateExternalMessageRequestBody {
    case_uuid: string;
    content: string;
    message_uuid: string;
    thread_uuid: string;
    message_type?: 'pip' | 'email';
    direction?: 'incoming' | 'outgoing' | 'unspecified';
    subject: string;
    attachments: {
      id: string;
      filename: string;
      [k: string]: any;
    }[];
    participants?: {
      role: 'to' | 'from' | 'cc' | 'bcc';
      display_name: string;
      address: string;
      /**
       * UUID of the participant(uuid of employee/person/organization)
       */
      uuid?: string;
    }[];
  }

  export interface GetCaseListForContactRequestParams {
    contact_uuid: string;
    [k: string]: any;
  }

  export interface GetCaseListForContactResponseBody {
    data: {
      id: string;
      type: 'case';
      attributes: {
        display_id: number;
        description: string | null;
        case_type_name: string;
        status: 'new' | 'open' | 'stalled' | 'resolved';
      };
    }[];
  }

  export interface DownloadAttachmentRequestParams {
    id: string;
    [k: string]: any;
  }

  export interface LinkThreadToCaseResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface LinkThreadToCaseRequestBody {
    case_uuid: string;
    thread_uuid: string;
    type: 'email';
  }

  export interface DeleteMessageResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface DeleteMessageRequestBody {
    message_uuid: string;
  }

  export interface MarkMessagesReadResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface MarkMessagesReadRequestBody {
    /**
     * List of uuid of messages on thread to mark as read.
     */
    message_uuids: string[];
    /**
     * Context of the request.
     */
    context: 'pip' | 'employee';
  }

  export interface MarkMessagesUnreadResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface MarkMessagesUnreadRequestBody {
    /**
     * List of uuid of messages on thread to mark as read.
     */
    message_uuids: string[];
    /**
     * Context of the request.
     */
    context: 'pip' | 'employee';
  }

  export interface PreviewAttachmentRequestParams {
    id: string;
    [k: string]: any;
  }

  export interface GetContactMomentListResponseBody {
    data: {
      id: string;
      type: 'contact_moment_overview';
      attributes: {
        contact_uuid: string;
        contact: string;
        case_id: string;
        direction: string;
        uuid: string;
        created: string;
        summary: string;
        channel: string;
        thread_uuid: string;
        contact_type: string;
      };
    }[];
  }

  export interface MessageThreadEntity {
    id: string;
    type: 'message_thread';
    meta: {
      created: string;
      last_modified: string;
      number_of_messages: number;
      summary: string;
      unread_employee_count: number;
      unread_pip_count: number;
      attachment_count: number;
    };
    attributes: {
      thread_type:
        | 'note'
        | 'contact_moment'
        | 'pip_message'
        | 'email'
        | 'postex';
      last_message:
        | {
            message_type: 'note';
            slug: string;
            created_name: string;
            created: string;
          }
        | {
            message_type: 'contact_moment';
            slug: string;
            direction: 'incoming' | 'outgoing';
            channel:
              | 'assignee'
              | 'frontdesk'
              | 'phone'
              | 'mail'
              | 'email'
              | 'webform'
              | 'social_media'
              | 'external_application';
            created_name: string;
            recipient_name: string;
            created: string;
          }
        | {
            message_type: 'email';
            slug: string;
            subject: string;
            created_name: string;
            created: string;
            failure_reason?: string;
          }
        | {
            message_type: 'pip_message';
            slug: string;
            subject: string;
            created_name: string;
            created: string;
          }
        | {
            message_type: 'postex';
            slug: string;
            subject: string;
            created_name: string;
            created: string;
            failure_reason?: string;
          };
      [k: string]: any;
    };
    relationships?: {
      contact?: {
        data: {
          id: string;
          type: 'contact';
          attributes: {
            type: 'employee' | 'person' | 'organization';
            name: string;
            address: string | null;
            email_address: string | null;
          };
        };
      };
      case?: {
        data: {
          id: string;
          type: 'case';
          attributes: {
            display_id: number;
            description: string | null;
            case_type_name: string;
            status: 'new' | 'open' | 'stalled' | 'resolved';
          };
        };
      };
    };
    links: {
      thread_messages: {
        href: string;
      };
    };
  }

  export type MessageEntity =
    | {
        id: string;
        type: 'contact_moment';
        meta: {
          last_modified?: string;
          created: string;
          message_date?: string;
          summary: string;
        };
        attributes: {
          content: string;
          direction: 'incoming' | 'outgoing';
          channel:
            | 'assignee'
            | 'frontdesk'
            | 'phone'
            | 'mail'
            | 'email'
            | 'webform'
            | 'social_media'
            | 'external_application';
        };
        relationships: {
          created_by: {
            data: {
              id: string;
              type: 'contact';
              attributes: {
                type: 'employee' | 'person' | 'organization';
                name: string;
                address: string | null;
                email_address: string | null;
              };
            };
          };
          recipient: {
            data: {
              id: string;
              type: 'contact';
              attributes: {
                type: 'employee' | 'person' | 'organization';
                name: string;
                address: string | null;
                email_address: string | null;
              };
            };
          };
          thread: {
            data: {
              id: string;
              type: 'message_thread';
            };
          };
        };
      }
    | {
        id: string;
        type: 'note';
        meta: {
          last_modified?: string;
          created: string;
          message_date?: string;
          summary: string;
        };
        attributes: {
          content: string;
        };
        relationships: {
          created_by: {
            data: {
              id: string;
              type: 'contact';
              attributes: {
                type: 'employee' | 'person' | 'organization';
                name: string;
                address: string | null;
                email_address: string | null;
              };
            };
          };
          thread: {
            data: {
              id: string;
              type: 'message_thread';
            };
          };
        };
      }
    | {
        id: string;
        type: 'external_message';
        meta: {
          last_modified?: string;
          created: string;
          message_date?: string;
          read_employee: string | null;
          read_pip: string | null;
          summary: string;
          attachment_count: number;
          failure_reason?: string;
        };
        attributes: {
          message_type: 'pip' | 'email';
          content: string;
          subject: string | null;
          participants?: {
            role: 'to' | 'from' | 'cc' | 'bcc';
            display_name: string;
            address: string;
            /**
             * UUID of the participant(uuid of employee/person/organization)
             */
            uuid?: string;
          }[];
          is_imported?: boolean;
        };
        relationships: {
          created_by?: {
            data: {
              id: string;
              type: 'contact';
              attributes: {
                type: 'employee' | 'person' | 'organization';
                name: string;
                address: string | null;
                email_address: string | null;
              };
            };
          };
          thread: {
            data: {
              id: string;
              type: 'message_thread';
            };
          };
          attachments: {
            data: {
              id: string;
              type: 'Attachment';
              attributes: {
                name: string;
                md5: string;
                mimetype: string;
                size: number;
                date_created: string;
              };
              links: {
                download: {
                  href: string;
                };
                preview?: {
                  href: string;
                  meta: {
                    'content-type'?: string;
                    [k: string]: any;
                  };
                };
              };
            }[];
          };
        };
      };

  export interface ContactEntity {
    id: string;
    type: 'contact';
    attributes: {
      type: 'employee' | 'person' | 'organization';
      name: string;
      address: string | null;
      email_address: string | null;
    };
  }

  export interface CaseEntity {
    id: string;
    type: 'case';
    attributes: {
      display_id: number;
      description: string | null;
      case_type_name: string;
      status: 'new' | 'open' | 'stalled' | 'resolved';
    };
  }

  export interface ContactMomentOverviewEntity {
    id: string;
    type: 'contact_moment_overview';
    attributes: {
      contact_uuid: string;
      contact: string;
      case_id: string;
      direction: string;
      uuid: string;
      created: string;
      summary: string;
      channel: string;
      thread_uuid: string;
      contact_type: string;
    };
  }

  export interface MessageNote {
    id: string;
    type: 'note';
    meta: {
      last_modified?: string;
      created: string;
      message_date?: string;
      summary: string;
    };
    attributes: {
      content: string;
    };
    relationships: {
      created_by: {
        data: {
          id: string;
          type: 'contact';
          attributes: {
            type: 'employee' | 'person' | 'organization';
            name: string;
            address: string | null;
            email_address: string | null;
          };
        };
      };
      thread: {
        data: {
          id: string;
          type: 'message_thread';
        };
      };
    };
  }

  export interface MessageContactMoment {
    id: string;
    type: 'contact_moment';
    meta: {
      last_modified?: string;
      created: string;
      message_date?: string;
      summary: string;
    };
    attributes: {
      content: string;
      direction: 'incoming' | 'outgoing';
      channel:
        | 'assignee'
        | 'frontdesk'
        | 'phone'
        | 'mail'
        | 'email'
        | 'webform'
        | 'social_media'
        | 'external_application';
    };
    relationships: {
      created_by: {
        data: {
          id: string;
          type: 'contact';
          attributes: {
            type: 'employee' | 'person' | 'organization';
            name: string;
            address: string | null;
            email_address: string | null;
          };
        };
      };
      recipient: {
        data: {
          id: string;
          type: 'contact';
          attributes: {
            type: 'employee' | 'person' | 'organization';
            name: string;
            address: string | null;
            email_address: string | null;
          };
        };
      };
      thread: {
        data: {
          id: string;
          type: 'message_thread';
        };
      };
    };
  }

  export interface MessageExternal {
    id: string;
    type: 'external_message';
    meta: {
      last_modified?: string;
      created: string;
      message_date?: string;
      read_employee: string | null;
      read_pip: string | null;
      summary: string;
      attachment_count: number;
      failure_reason?: string;
    };
    attributes: {
      message_type: 'pip' | 'email';
      content: string;
      subject: string | null;
      participants?: {
        role: 'to' | 'from' | 'cc' | 'bcc';
        display_name: string;
        address: string;
        /**
         * UUID of the participant(uuid of employee/person/organization)
         */
        uuid?: string;
      }[];
      is_imported?: boolean;
    };
    relationships: {
      created_by?: {
        data: {
          id: string;
          type: 'contact';
          attributes: {
            type: 'employee' | 'person' | 'organization';
            name: string;
            address: string | null;
            email_address: string | null;
          };
        };
      };
      thread: {
        data: {
          id: string;
          type: 'message_thread';
        };
      };
      attachments: {
        data: {
          id: string;
          type: 'Attachment';
          attributes: {
            name: string;
            md5: string;
            mimetype: string;
            size: number;
            date_created: string;
          };
          links: {
            download: {
              href: string;
            };
            preview?: {
              href: string;
              meta: {
                'content-type'?: string;
                [k: string]: any;
              };
            };
          };
        }[];
      };
    };
  }

  export interface AttachmentEntity {
    id: string;
    type: 'Attachment';
    attributes: {
      name: string;
      md5: string;
      mimetype: string;
      size: number;
      date_created: string;
    };
    links: {
      download: {
        href: string;
      };
      preview?: {
        href: string;
        meta: {
          'content-type'?: string;
          [k: string]: any;
        };
      };
    };
  }

  export interface ThreadEntity {
    id: string;
    type: 'message_thread';
  }

  export interface MessageParticipant {
    role: 'to' | 'from' | 'cc' | 'bcc';
    display_name: string;
    address: string;
    /**
     * UUID of the participant(uuid of employee/person/organization)
     */
    uuid?: string;
  }
}
