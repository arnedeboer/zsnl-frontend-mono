// Generated on: Thu Sep 09 2021 10:49:52 GMT+0200 (Central European Summer Time)
// Environment used: https://development.zaaksysteem.nl
//
// This file was automatically generated. DO NOT MODIFY IT BY HAND.
// Instead, rerun generation of Catalog domain.

/* eslint-disable */
export namespace APICatalog {
  export interface GetFolderContentsRequestParams {
    folder_id?: string;
    [k: string]: any;
  }

  export interface GetFolderContentsResponseBody {
    data: {
      type: 'folder_entry';
      id: string;
      attributes: {
        type:
          | 'folder'
          | 'case_type'
          | 'object_type'
          | 'attribute'
          | 'email_template'
          | 'document_template'
          | 'custom_object_type';
        name: string;
        active: boolean;
        [k: string]: any;
      };
      links: {
        self: string;
        meta?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    links?: {
      self?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      parent?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      grandparent?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetEntryDetailRequestParams {
    item_id: string;
    type:
      | 'folder'
      | 'case_type'
      | 'object_type'
      | 'custom_object_type'
      | 'attribute'
      | 'email_template'
      | 'document_template';
    [k: string]: any;
  }

  export interface GetEntryDetailResponseBody {
    data: {
      schema?:
        | {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }
        | {
            type: 'case_type';
            id: string;
            attributes: {
              name?: string;
              active?: boolean;
              api_v1_endpoint?: string;
              stuf_identification?: string;
              current_version?: number;
              last_modified?: string;
              context?: ('internal' | 'external')[];
              [k: string]: any;
            };
            relationships: {
              used_in_case_types?: {
                id?: string;
                type?: 'case_type';
                attributes?: {
                  name?: string;
                  version?: number;
                  is_current_version?: boolean;
                  active?: boolean;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              used_in_object_types?: {
                id?: string;
                type?: 'object_type';
                attributes?: {
                  name?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            links?: {
              export: string;
              version_history: string;
              registration_form_internal?: string;
            };
            [k: string]: any;
          }
        | {
            type: 'object_type';
            id: string;
            attributes: {
              name?: string;
              last_modified?: string;
              [k: string]: any;
            };
            relationships: {
              used_in_case_types?: {
                id?: string;
                type?: 'case_type';
                attributes?: {
                  name?: string;
                  version?: number;
                  is_current_version?: boolean;
                  active?: boolean;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          }
        | {
            type: 'custom_object_type';
            id: string;
            attributes: {
              name?: string;
              title?: string;
              external_reference?: string;
              status?: string;
              version?: number;
              version_independent_uuid?: string;
              last_modified?: string;
              date_created?: string;
              [k: string]: any;
            };
            relationships: {
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          }
        | {
            type: 'attribute';
            id: string;
            attributes: {
              name?: string;
              magic_string?: string;
              entry_type?: string;
              is_multiple?: boolean;
              last_modified?: string;
              [k: string]: any;
            };
            relationships: {
              used_in_case_types?: {
                id?: string;
                type?: 'case_type';
                attributes?: {
                  name?: string;
                  version?: number;
                  is_current_version?: boolean;
                  active?: boolean;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              used_in_object_types?: {
                id?: string;
                type?: 'object_type';
                attributes?: {
                  name?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          }
        | {
            type: 'email_template';
            id: string;
            attributes: {
              name?: string;
              last_modified?: string;
              [k: string]: any;
            };
            relationships: {
              used_in_case_types?: {
                id?: string;
                type?: 'case_type';
                attributes?: {
                  name?: string;
                  version?: number;
                  is_current_version?: boolean;
                  active?: boolean;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          }
        | {
            type: 'document_template';
            id: string;
            attributes: {
              name?: string;
              download_uri?: string;
              last_modified?: string;
              [k: string]: any;
            };
            relationships: {
              used_in_case_types?: {
                id?: string;
                type?: 'case_type';
                attributes?: {
                  name?: string;
                  version?: number;
                  is_current_version?: boolean;
                  active?: boolean;
                  [k: string]: any;
                };
                [k: string]: any;
              }[];
              folder?: {
                type: 'folder';
                id: string;
                attributes: {
                  name?: string;
                  [k: string]: any;
                };
                links?: {
                  self?: string;
                  [k: string]: any;
                };
                [k: string]: any;
              };
              [k: string]: any;
            };
            [k: string]: any;
          };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface MoveFolderEntriesResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface MoveFolderEntriesRequestBody {
    folder_enties: {
      type:
        | 'folder'
        | 'case_type'
        | 'object_type'
        | 'attribute'
        | 'email_template'
        | 'document_template';
      id: string;
      [k: string]: any;
    }[];
    destination_folder_id: string;
    [k: string]: any;
  }

  export interface ChangeCaseTypeOnlineStatusResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface ChangeCaseTypeOnlineStatusRequestBody {
    active: boolean;
    case_type_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface GetAttributeDetailRequestParams {
    attribute_id: string;
    [k: string]: any;
  }

  export interface GetAttributeDetailResponseBody {
    data: {
      schema?: {
        type: string;
        id: string;
        attributes: {
          attribute_type?: string;
          name?: string;
          public_name?: string;
          magic_string?: string;
          type_multiple?: boolean;
          sensitive_field?: boolean;
          help?: string;
          value_default?: string;
          category_name?: string;
          category_uuid?: string;
          document_origin?: string;
          document_trust_level?: string;
          document_category?: string;
          relationship_data?: {
            [k: string]: any;
          };
          attribute_values?: string[];
          /**
           * Can be any value - string, number, boolean, array or object.
           */
          appointment_location_id?: {
            [k: string]: any;
          };
          /**
           * Can be any value - string, number, boolean, array or object.
           */
          appointment_product_id?: {
            [k: string]: any;
          };
          appointment_interface_uuid?: string | null;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditAttributeRequestParams {
    [k: string]: any;
  }

  export interface EditAttributeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditAttributeRequestBody {
    attribute_uuid?: string;
    fields?: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateAttributeRequestParams {
    [k: string]: any;
  }

  export interface CreateAttributeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateAttributeRequestBody {
    attribute_uuid?: string;
    fields: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GenerateMagicStringRequestParams {
    string_input: string;
    [k: string]: any;
  }

  export interface GenerateMagicStringResponseBody {
    data: {
      schema?: {
        magic_string: string;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetActiveAppointmentIntegrationsRequestParams {
    [k: string]: any;
  }

  export interface GetActiveAppointmentIntegrationsResponseBody {
    [k: string]: any;
  }

  export interface GetActiveAppointmentV2IntegrationsRequestParams {
    [k: string]: any;
  }

  export interface GetActiveAppointmentV2IntegrationsResponseBody {
    [k: string]: any;
  }

  export interface CreateFolderResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateFolderRequestBody {
    folder_uuid: string;
    name: string;
    parent_uuid: string;
    [k: string]: any;
  }

  export interface RenameFolderResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface RenameFolderRequestBody {
    folder_uuid: string;
    name: string;
    [k: string]: any;
  }

  export interface SearchAttributeByNameRequestParams {
    search_string: string;
    type?: string;
    [k: string]: any;
  }

  export interface SearchAttributeByNameResponseBody {
    data: {
      type?: string;
      id?: string;
      attribute?: {
        name?: string;
        magic_string?: string;
        value_type?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    [k: string]: any;
  }

  export interface GetEmailTemplateDetailRequestParams {
    email_template_id: string;
    [k: string]: any;
  }

  export interface GetEmailTemplateDetailResponseBody {
    data: {
      schema?: {
        attributes: {
          attachments?: {
            name?: string;
            uuid?: string;
            [k: string]: any;
          }[];
          category_uuid?:
            | string
            | {
                [k: string]: any;
              };
          label?: string;
          message?: string;
          sender?: string;
          sender_address?: string;
          subject?: string;
          [k: string]: any;
        };
        id: string;
        type: 'email_template';
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateAnEmailTemplateRequestParams {
    [k: string]: any;
  }

  export interface CreateAnEmailTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateAnEmailTemplateRequestBody {
    email_template_uuid: string;
    fields: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditAnEmailTemplateRequestParams {
    [k: string]: any;
  }

  export interface EditAnEmailTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditAnEmailTemplateRequestBody {
    email_template_uuid: string;
    fields: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface SearchCatalogRequestParams {
    keyword?: string;
    [k: string]: any;
  }

  export interface SearchCatalogResponseBody {
    data: {
      type: 'folder_entry';
      id: string;
      attributes: {
        type:
          | 'folder'
          | 'case_type'
          | 'object_type'
          | 'attribute'
          | 'email_template'
          | 'document_template'
          | 'custom_object_type';
        name: string;
        active: boolean;
        [k: string]: any;
      };
      links: {
        self: string;
        meta?: string;
        [k: string]: any;
      };
      [k: string]: any;
    }[];
    links?: {
      self?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      parent?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      grandparent?: {
        href?: string;
        name?: string | null;
        id?: string | null;
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetActiveDocumentIntegrationsRequestParams {
    [k: string]: any;
  }

  export interface GetActiveDocumentIntegrationsResponseBody {
    [k: string]: any;
  }

  export interface CreateDocumentTemplateRequestParams {
    [k: string]: any;
  }

  export interface CreateDocumentTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CreateDocumentTemplateRequestBody {
    document_template_uuid: string;
    fields: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetDocumentTemplateDetailRequestParams {
    document_template_id: string;
    [k: string]: any;
  }

  export interface GetDocumentTemplateDetailResponseBody {
    data: {
      schema?: {
        attributes: {
          category_uuid?:
            | string
            | {
                [k: string]: any;
              };
          name?: string;
          interface_uuid?: string;
          template_external_name?: string;
          help?: string;
          [k: string]: any;
        };
        id: string;
        type: 'document_template';
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditDocumentTemplateRequestParams {
    [k: string]: any;
  }

  export interface EditDocumentTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EditDocumentTemplateRequestBody {
    document_template_uuid: string;
    fields: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface GetCaseTypeHistoryRequestParams {
    case_type_id: string;
    [k: string]: any;
  }

  export interface GetCaseTypeHistoryResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateCaseTypeVersionRequestParams {
    [k: string]: any;
  }

  export interface UpdateCaseTypeVersionResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface UpdateCaseTypeVersionRequestBody {
    case_type_id: string;
    version_id: string;
    reason: {
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteAttributeRequestParams {
    [k: string]: any;
  }

  export interface DeleteAttributeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteAttributeRequestBody {
    attribute_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface DeleteEmailTemplateRequestParams {
    [k: string]: any;
  }

  export interface DeleteEmailTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteEmailTemplateRequestBody {
    email_template_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface DeleteFolderRequestParams {
    [k: string]: any;
  }

  export interface DeleteFolderResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteFolderRequestBody {
    folder_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface DeleteCaseTypeRequestParams {
    [k: string]: any;
  }

  export interface DeleteCaseTypeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteCaseTypeRequestBody {
    case_type_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface DeleteObjectTypeRequestParams {
    [k: string]: any;
  }

  export interface DeleteObjectTypeResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteObjectTypeRequestBody {
    case_type_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface DeleteDocumentTemplateRequestParams {
    [k: string]: any;
  }

  export interface DeleteDocumentTemplateResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DeleteDocumentTemplateRequestBody {
    document_template_uuid: string;
    reason: string;
    [k: string]: any;
  }

  export interface RenameUserRequestParams {
    [k: string]: any;
  }

  export interface RenameUserResponseBody {
    data?: {
      success?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface RenameUserRequestBody {
    dry_run?: string;
    users: {
      old_username: string;
      new_username: string;
      [k: string]: any;
    }[];
    integration_uuid: string;
    [k: string]: any;
  }

  /**
   * Can be any value - string, number, boolean, array or object.
   */
  export interface AnyValue {
    [k: string]: any;
  }

  export interface AttributeDetail {
    type: 'attribute';
    id: string;
    attributes: {
      name?: string;
      magic_string?: string;
      entry_type?: string;
      is_multiple?: boolean;
      last_modified?: string;
      [k: string]: any;
    };
    relationships: {
      used_in_case_types?: {
        id?: string;
        type?: 'case_type';
        attributes?: {
          name?: string;
          version?: number;
          is_current_version?: boolean;
          active?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      used_in_object_types?: {
        id?: string;
        type?: 'object_type';
        attributes?: {
          name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AttributeDetailForEdit {
    type: string;
    id: string;
    attributes: {
      attribute_type?: string;
      name?: string;
      public_name?: string;
      magic_string?: string;
      type_multiple?: boolean;
      sensitive_field?: boolean;
      help?: string;
      value_default?: string;
      category_name?: string;
      category_uuid?: string;
      document_origin?: string;
      document_trust_level?: string;
      document_category?: string;
      relationship_data?: {
        [k: string]: any;
      };
      attribute_values?: string[];
      /**
       * Can be any value - string, number, boolean, array or object.
       */
      appointment_location_id?: {
        [k: string]: any;
      };
      /**
       * Can be any value - string, number, boolean, array or object.
       */
      appointment_product_id?: {
        [k: string]: any;
      };
      appointment_interface_uuid?: string | null;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface AttributeDetailSearch {
    type?: string;
    id?: string;
    attribute?: {
      name?: string;
      magic_string?: string;
      value_type?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CaseTypeDetail {
    type: 'case_type';
    id: string;
    attributes: {
      name?: string;
      active?: boolean;
      api_v1_endpoint?: string;
      stuf_identification?: string;
      current_version?: number;
      last_modified?: string;
      context?: ('internal' | 'external')[];
      [k: string]: any;
    };
    relationships: {
      used_in_case_types?: {
        id?: string;
        type?: 'case_type';
        attributes?: {
          name?: string;
          version?: number;
          is_current_version?: boolean;
          active?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      used_in_object_types?: {
        id?: string;
        type?: 'object_type';
        attributes?: {
          name?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    links?: {
      export: string;
      version_history: string;
      registration_form_internal?: string;
    };
    [k: string]: any;
  }

  export interface CatalogEntry {
    type: 'folder_entry';
    id: string;
    attributes: {
      type:
        | 'folder'
        | 'case_type'
        | 'object_type'
        | 'attribute'
        | 'email_template'
        | 'document_template'
        | 'custom_object_type';
      name: string;
      active: boolean;
      [k: string]: any;
    };
    links: {
      self: string;
      meta?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CatalogEntryAttributes {
    type:
      | 'folder'
      | 'case_type'
      | 'object_type'
      | 'attribute'
      | 'email_template'
      | 'document_template'
      | 'custom_object_type';
    name: string;
    active: boolean;
    [k: string]: any;
  }

  export interface CatalogEntryCaseTypeRelation {
    id?: string;
    type?: 'case_type';
    attributes?: {
      name?: string;
      version?: number;
      is_current_version?: boolean;
      active?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export type CatalogEntryDetail =
    | {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        type: 'case_type';
        id: string;
        attributes: {
          name?: string;
          active?: boolean;
          api_v1_endpoint?: string;
          stuf_identification?: string;
          current_version?: number;
          last_modified?: string;
          context?: ('internal' | 'external')[];
          [k: string]: any;
        };
        relationships: {
          used_in_case_types?: {
            id?: string;
            type?: 'case_type';
            attributes?: {
              name?: string;
              version?: number;
              is_current_version?: boolean;
              active?: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          used_in_object_types?: {
            id?: string;
            type?: 'object_type';
            attributes?: {
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        links?: {
          export: string;
          version_history: string;
          registration_form_internal?: string;
        };
        [k: string]: any;
      }
    | {
        type: 'object_type';
        id: string;
        attributes: {
          name?: string;
          last_modified?: string;
          [k: string]: any;
        };
        relationships: {
          used_in_case_types?: {
            id?: string;
            type?: 'case_type';
            attributes?: {
              name?: string;
              version?: number;
              is_current_version?: boolean;
              active?: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        type: 'custom_object_type';
        id: string;
        attributes: {
          name?: string;
          title?: string;
          external_reference?: string;
          status?: string;
          version?: number;
          version_independent_uuid?: string;
          last_modified?: string;
          date_created?: string;
          [k: string]: any;
        };
        relationships: {
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        type: 'attribute';
        id: string;
        attributes: {
          name?: string;
          magic_string?: string;
          entry_type?: string;
          is_multiple?: boolean;
          last_modified?: string;
          [k: string]: any;
        };
        relationships: {
          used_in_case_types?: {
            id?: string;
            type?: 'case_type';
            attributes?: {
              name?: string;
              version?: number;
              is_current_version?: boolean;
              active?: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          used_in_object_types?: {
            id?: string;
            type?: 'object_type';
            attributes?: {
              name?: string;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        type: 'email_template';
        id: string;
        attributes: {
          name?: string;
          last_modified?: string;
          [k: string]: any;
        };
        relationships: {
          used_in_case_types?: {
            id?: string;
            type?: 'case_type';
            attributes?: {
              name?: string;
              version?: number;
              is_current_version?: boolean;
              active?: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      }
    | {
        type: 'document_template';
        id: string;
        attributes: {
          name?: string;
          download_uri?: string;
          last_modified?: string;
          [k: string]: any;
        };
        relationships: {
          used_in_case_types?: {
            id?: string;
            type?: 'case_type';
            attributes?: {
              name?: string;
              version?: number;
              is_current_version?: boolean;
              active?: boolean;
              [k: string]: any;
            };
            [k: string]: any;
          }[];
          folder?: {
            type: 'folder';
            id: string;
            attributes: {
              name?: string;
              [k: string]: any;
            };
            links?: {
              self?: string;
              [k: string]: any;
            };
            [k: string]: any;
          };
          [k: string]: any;
        };
        [k: string]: any;
      };

  export interface CatalogEntryLinks {
    self: string;
    meta?: string;
    [k: string]: any;
  }

  export interface CatalogEntryObjectTypeRelation {
    id?: string;
    type?: 'object_type';
    attributes?: {
      name?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DocumentTemplateDetail {
    type: 'document_template';
    id: string;
    attributes: {
      name?: string;
      download_uri?: string;
      last_modified?: string;
      [k: string]: any;
    };
    relationships: {
      used_in_case_types?: {
        id?: string;
        type?: 'case_type';
        attributes?: {
          name?: string;
          version?: number;
          is_current_version?: boolean;
          active?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface DocumentTemplateDetailForEdit {
    attributes: {
      category_uuid?:
        | string
        | {
            [k: string]: any;
          };
      name?: string;
      interface_uuid?: string;
      template_external_name?: string;
      help?: string;
      [k: string]: any;
    };
    id: string;
    type: 'document_template';
    [k: string]: any;
  }

  export interface EmailTemplateDetail {
    type: 'email_template';
    id: string;
    attributes: {
      name?: string;
      last_modified?: string;
      [k: string]: any;
    };
    relationships: {
      used_in_case_types?: {
        id?: string;
        type?: 'case_type';
        attributes?: {
          name?: string;
          version?: number;
          is_current_version?: boolean;
          active?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface EmailTemplateDetailForEdit {
    attributes: {
      attachments?: {
        name?: string;
        uuid?: string;
        [k: string]: any;
      }[];
      category_uuid?:
        | string
        | {
            [k: string]: any;
          };
      label?: string;
      message?: string;
      sender?: string;
      sender_address?: string;
      subject?: string;
      [k: string]: any;
    };
    id: string;
    type: 'email_template';
    [k: string]: any;
  }

  export interface FolderDetail {
    type: 'folder';
    id: string;
    attributes: {
      name?: string;
      [k: string]: any;
    };
    links?: {
      self?: string;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface IntegrationDetail {
    type: 'integration';
    id: string;
    attributes: {
      module?: string;
      name?: string;
      active?: boolean;
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface MagicString {
    magic_string: string;
    [k: string]: any;
  }

  export interface MoveableCatalogEntry {
    type:
      | 'folder'
      | 'case_type'
      | 'object_type'
      | 'attribute'
      | 'email_template'
      | 'document_template';
    id: string;
    [k: string]: any;
  }

  export interface ObjectTypeDetail {
    type: 'object_type';
    id: string;
    attributes: {
      name?: string;
      last_modified?: string;
      [k: string]: any;
    };
    relationships: {
      used_in_case_types?: {
        id?: string;
        type?: 'case_type';
        attributes?: {
          name?: string;
          version?: number;
          is_current_version?: boolean;
          active?: boolean;
          [k: string]: any;
        };
        [k: string]: any;
      }[];
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }

  export interface CustomObjectTypeDetail {
    type: 'custom_object_type';
    id: string;
    attributes: {
      name?: string;
      title?: string;
      external_reference?: string;
      status?: string;
      version?: number;
      version_independent_uuid?: string;
      last_modified?: string;
      date_created?: string;
      [k: string]: any;
    };
    relationships: {
      folder?: {
        type: 'folder';
        id: string;
        attributes: {
          name?: string;
          [k: string]: any;
        };
        links?: {
          self?: string;
          [k: string]: any;
        };
        [k: string]: any;
      };
      [k: string]: any;
    };
    [k: string]: any;
  }
}
