// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable no-unused-vars, no-console */

const { compile } = require('json-schema-to-typescript');
const handle = require('../../library/promiseHandle');

function createOperationTypeName(operationId, postFix) {
  const typeName = `${operationId}`
    .split('_')
    .map(
      ([firstLetter, ...remaining]) =>
        `${firstLetter.toUpperCase()}${remaining.join('')}`
    )
    .join('');

  return `${typeName}${postFix}`;
}

async function compileSchema(schema, name) {
  const [type, typeError] = await handle(
    compile(schema, name, {
      declareExternallyReferenced: false,
      unreachableDefinitions: false,
      bannerComment: '',
    })
  );

  if (typeError) throw Error(`Error compiling schema ${name}: ${typeError}`);

  return type;
}

function compileResponseBody(schema, operationId) {
  const { allOf, ...restSchema } = schema;
  const toCompile = Object.keys(restSchema).length ? restSchema : schema;

  return compileSchema(
    toCompile,
    createOperationTypeName(operationId, 'ResponseBody')
  );
}

function compileRequestBody(requestBody, operationId) {
  const { schema } =
    requestBody.content['multipart/form-data'] ||
    requestBody.content['application/json'];
  const { allOf, ...restSchema } = schema;

  const toCompile = Object.keys(restSchema).length ? restSchema : schema;
  return compileSchema(
    toCompile,
    createOperationTypeName(operationId, 'RequestBody')
  );
}

function compileRequestParams(schema, operationId) {
  const flattenedSchema = schema.reduce(
    (acc, item) => {
      return {
        ...acc,
        properties: {
          ...acc.properties,
          [item.name]: item.schema,
        },
        required: item.required ? [...acc.required, item.name] : acc.required,
      };
    },
    { properties: {}, required: [] }
  );

  return compileSchema(
    flattenedSchema,
    createOperationTypeName(operationId, 'RequestParams')
  );
}

function hasSuccessfulResponse(responses) {
  return (
    responses &&
    responses[200] &&
    responses[200].content &&
    responses[200].content['application/json'] &&
    responses[200].content['application/json'].schema
  );
}

function compileRequestResponseTypes(schema) {
  const promises = Object.values(schema.paths).reduce((acc, pathValue) => {
    const allOperations = Object.values(pathValue).reduce(
      (operations, operationValue) => {
        try {
          const requestParams = operationValue.parameters
            ? compileRequestParams(
                operationValue.parameters,
                operationValue.operationId
              )
            : null;
          const responseBody = hasSuccessfulResponse(operationValue.responses)
            ? compileResponseBody(
                operationValue.responses[200].content['application/json']
                  .schema,
                operationValue.operationId
              )
            : null;
          const requestBody = operationValue.requestBody
            ? compileRequestBody(
                operationValue.requestBody,
                operationValue.operationId
              )
            : null;

          return [...operations, requestParams, responseBody, requestBody];
        } catch (error) {
          console.log(error);
          throw Error(error);
        }
      },
      []
    );

    return [...acc, ...allOperations];
  }, []);

  return promises.filter(Boolean);
}

function compileEntityTypes(schema) {
  const promises = Object.values(schema).reduce((schemas, value) => {
    return [
      ...schemas,
      ...Object.entries(value).reduce((entities, [key, schemaItem]) => {
        return key === 'schemas'
          ? [...entities, ...compileEntityTypes(value)]
          : [...entities, compileSchema(schemaItem, key)];
      }, []),
    ];
  }, []);

  return promises.filter(Boolean);
}

async function compileTypes(schemas) {
  const typesPromises = schemas.map(([domain, api_docs, entities]) => {
    const entitiesPromise = Promise.all(
      entities.map(entity => Promise.all(compileEntityTypes(entity)))
    );

    return Promise.all([
      Promise.resolve(domain),
      Promise.all(compileRequestResponseTypes(api_docs)),
      entitiesPromise,
    ]);
  });

  const [types, typesError] = await handle(Promise.all(typesPromises));

  if (typesError) throw Error(`Error compiling types ${typesError}`);

  return types;
}

module.exports = compileTypes;
