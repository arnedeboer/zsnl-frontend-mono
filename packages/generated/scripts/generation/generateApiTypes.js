// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable no-console */

const fetchAndResolveApiSpecs = require('./library/fetchAndResolveApiSpecs');
const compileTypes = require('./library/compileTypes');
const writeTypes = require('./library/writeTypes');

const OUTPUT_PATH = './types/';

process.on('unhandledRejection', error => {
  throw error;
});

function countTypes(entities) {
  return entities.reduce((acc, cur) => {
    const count = Array.isArray(cur) ? countTypes(cur) : 1;
    return acc + count;
  }, 0);
}

async function generateApiTypes(environmentUrl, schemas) {
  const startTime = Date.now();

  console.group(`Generating types from ${environmentUrl}`);
  console.log('for the following domains:');
  Object.keys(schemas).map(item => console.log(` - ${item}`));

  const resolvedSchemas = await fetchAndResolveApiSpecs(
    environmentUrl,
    schemas
  );
  const types = await compileTypes(resolvedSchemas);
  console.groupEnd();

  console.group(`Generating done`);
  await writeTypes(types, OUTPUT_PATH, environmentUrl);

  const numTypes = types.reduce(
    (acc, [, api_docs, entities]) =>
      acc + countTypes(api_docs) + countTypes(entities),
    0
  );
  console.groupEnd();

  console.log(
    `⭐ Successfully generated ${numTypes} types in ${
      (Date.now() - startTime) / 1000
    } seconds\n`
  );
}

module.exports = generateApiTypes;
