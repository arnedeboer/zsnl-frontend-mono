// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const test = require('tape');
const { bind } = require('./instance');

test('bind()', assert => {
  const foo = {
    bar() {
      return this.quux;
    },
    quux: 42,
  };

  bind(foo, 'bar');

  // deconstruct the method to call it as a function
  const { bar } = foo;

  const actual = bar();
  const expected = 42;
  const message = 'binds an object property to the object';

  assert.equal(actual, expected, message);
  assert.end();
});
