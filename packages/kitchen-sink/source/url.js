// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const { keys, entries } = Object;

export const ENCODED_NULL_BYTE = '%00';

/**
 * Get the path segments from an absolute path with an optional query.
 *
 * @param {string} path
 * @return {Array}
 */
export function getSegments(path) {
  const [pathComponent] = path.split('?');
  const [, ...segments] = pathComponent.split('/');

  return segments;
}

/**
 * Get the second path segment from an absolute path with an optional query.
 *
 * @param {string} path
 * @return {string}
 */
export function getSegment(path) {
  const [, segment] = getSegments(path);

  return segment;
}

export function flattenParamObject(object, paramName) {
  return Array.isArray(object)
    ? { [paramName]: object }
    : keys(object).reduce(
        (acc, key) => ({
          ...acc,
          [`${paramName}[${key}]`]: object[key],
        }),
        {}
      );
}

/**
 * Build a paramString from an the keyValues of an object
 *
 * @param {Object} params
 * @return {string}
 */
export function buildParamString(params) {
  const flattenedParams = keys(params)
    .filter(param => params[param])
    .reduce((acc, param) => {
      return typeof params[param] === 'object'
        ? { ...acc, ...flattenParamObject(params[param], param) }
        : { ...acc, [param]: params[param] };
    }, {});

  const encodeValue = value => {
    if (value === ENCODED_NULL_BYTE) {
      return value;
    }

    return Array.isArray(value)
      ? value.map(encodeURIComponent).join(',')
      : encodeURIComponent(value);
  };

  const paramsAsString = entries(flattenedParams)
    .map(([param, value]) => `${param}=${encodeValue(value)}`)
    .join('&');

  const preParam = paramsAsString.length ? '?' : '';

  return `${preParam}${paramsAsString}`;
}

/**
 * Build a URL from the base url and given params
 *
 * @param {string} url
 * @param {Object} params
 * @return {string}
 */
export function buildUrl(url, params) {
  return `${url}${buildParamString(params)}`;
}

/**
 * @param {string} params
 * @return {Object}
 */
export const objectifyParams = params => {
  if (!params) {
    return {};
  }

  return params.split('&').reduce((acc, param) => {
    const [name, value] = param.split('=');

    acc[name] = decodeURIComponent(value);

    return acc;
  }, {});
};
