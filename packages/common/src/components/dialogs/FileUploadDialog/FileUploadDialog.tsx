// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { FormikValues } from 'formik';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import translateFormDefinition from '@zaaksysteem/common/src/components/form/library/translateFormDefinition';

import { getFormDefinition } from './FileUploadDialogFormDefinition';
import locale from './locale';

export type FileUploadDialogPropsType = {
  onConfirm: (files: any) => void;
  onClose: () => void;
  open: boolean;
  multiValue?: boolean;
};

export const FileUploadDialog: React.FunctionComponent<
  FileUploadDialogPropsType
> = ({ onConfirm, onClose, open, multiValue = true }) => {
  const [saving, setSaving] = useState(false);
  const [t] = useTranslation();

  const handleOnSubmit = async (formValues: FormikValues) => {
    setSaving(true);

    await onConfirm(formValues);

    setSaving(false);
  };

  const title = multiValue
    ? t('FileLinkerDialog:title.multiple')
    : t('FileLinkerDialog:title.single');

  return (
    <I18nResourceBundle resource={locale} namespace="FileLinkerDialog">
      <FormDialog
        formDefinition={translateFormDefinition(
          getFormDefinition(multiValue),
          t
        )}
        onSubmit={handleOnSubmit}
        title={title}
        scope="FileLinkerDialog"
        icon="insert_drive_file"
        onClose={onClose}
        saveLabel={t('FileLinkerDialog:save')}
        saving={saving}
        open={open}
      />
    </I18nResourceBundle>
  );
};
