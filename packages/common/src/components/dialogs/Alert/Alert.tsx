// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '../library/createDialogActions';

type AlertPropsType = {
  open?: boolean;
  title: string;
  scope?: string;
  icon?: string;
  onClose?: () => void;
  primaryButton: {
    text: string;
    onClick?: (event: React.MouseEvent) => void;
    action?: (event: React.MouseEvent) => void;
  };
  secondaryButton?: {
    text: string;
    onClick?: (event: React.MouseEvent) => void;
    action?: (event: React.MouseEvent) => void;
  };
  container?: React.ReactInstance | null;
};

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
});

export const Alert: React.ComponentType<AlertPropsType> = ({
  open = false,
  primaryButton,
  secondaryButton,
  title,
  scope,
  children,
  onClose,
  container,
}) => {
  const dialogActions = getDialogActions(primaryButton, secondaryButton, scope);

  return (
    <Dialog
      aria-label={title}
      open={open}
      disableBackdropClick={true}
      onClose={onClose}
      container={container}
    >
      <DialogTitle title={title} scope={scope} />
      <DialogContent>{children}</DialogContent>
      {dialogActions && <DialogActions>{dialogActions}</DialogActions>}
    </Dialog>
  );
};

export default Alert;
