// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles(
  ({ palette: { primary }, typography }: Theme) => {
    return {
      wrapper: {
        display: 'flex',
        marginBottom: 12,
        padding: 10,
        backgroundColor: primary.lightest,
        borderRadius: 6,
        ...typography.body1,
        '& span': {
          fontWeight: 'bold',
        },
      },
    };
  }
);
