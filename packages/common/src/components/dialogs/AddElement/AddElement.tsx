// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source';
import { withStyles } from '@material-ui/styles';
import {
  Dialog,
  DialogTitle,
  DialogDivider,
  DialogContent,
} from '@mintlab/ui/App/Material/Dialog';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { Body2 } from '@mintlab/ui/App/Material/Typography';
import { addScopeAttribute } from '@mintlab/ui/App/library/addScope';
import { addElementStylesheet } from './AddElement.style';
import {
  AddElementPropsType,
  SectionType,
  ClassesType,
} from './AddElement.types';

const Section = ({
  actions,
  classes,
}: {
  actions: SectionType[];
  classes: ClassesType;
}) => (
  <div className={classes.elementWrapper}>
    {actions.map(({ action, type, title, icon, disabled }: SectionType) => (
      <button
        className={classes.element}
        key={type}
        type="button"
        onClick={action}
        disabled={disabled}
        {...addScopeAttribute('catalog', 'add-element', 'button', type)}
      >
        <div className={classes.elementIcon}>
          {icon ? (
            typeof icon === 'string' ? (
              <ZsIcon style={{ fontSize: 30 }}>{icon}</ZsIcon>
            ) : (
              icon
            )
          ) : null}
        </div>
        <div className={classes.elementTitle}>
          <Body2>{title}</Body2>
        </div>
      </button>
    ))}
  </div>
);

/**
 * @return {ReactElement}
 */
const AddElement: React.FunctionComponent<AddElementPropsType> = ({
  t,
  classes,
  hide,
  sections,
  title,
  titleIcon = 'add',
}) => {
  const handleClose = () => hide();
  const labelId = unique();
  const scope = 'catalog-add-element-dialog';

  return (
    <Dialog
      //@ts-ignore
      id={scope}
      open={true}
      onClose={handleClose}
      scope={scope}
    >
      <DialogTitle
        elevated={true}
        icon={titleIcon}
        id={labelId}
        title={title || t('catalog:addElement.title')}
        onCloseClick={handleClose}
        scope={scope}
      />
      <DialogContent padded={false}>
        {sections.map((section, index) => (
          <React.Fragment key={index}>
            {index > 0 && (
              <div className={classes.dividerWrapper}>
                <DialogDivider />
              </div>
            )}
            <Section actions={section} classes={classes} />
          </React.Fragment>
        ))}
      </DialogContent>
    </Dialog>
  );
};

export default withStyles(addElementStylesheet)(AddElement);
