// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { ContactType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';

type RequestorTypesType = string[];

type RequestorTypesV0Type = (
  | 'niet_natuurlijk_persoon'
  | 'preset_client'
  | 'natuurlijk_persoon_na'
  | 'natuurlijk_persoon'
  | 'medewerker'
)[];

export type SavedCaseTypeType = {
  value?: string;
  label?: string;
  data: {
    id?: string;
    name?: string;
    type_of_requestors?: RequestorTypesV0Type;
    initiator_source?: string;
  };
};

export type TranslateRequestorTypesType = (
  requestorTypes: RequestorTypesV0Type
) => RequestorTypesType;

export type GetShouldPrefillCaseTypeType = (
  contactTypeOption?: ContactType | null,
  savedCaseTypeOption?: SavedCaseTypeType | null
) => boolean;

export type GetSavedCaseTypeType = () => Promise<SavedCaseTypeType | null>;

export type CaseCreateDialogPropsType = {
  selectedDocumentUuid?: string;
  contact?: {
    uuid: string;
    type: SubjectTypeType;
  };
  open: boolean;
  onClose: () => void;
  container?: React.ReactInstance | null;
  contactChannel?: ContactChannelType;
  savedCaseType?: SavedCaseTypeType | null;
};

export type CaseCreateDialogInnerPropsType = Pick<
  CaseCreateDialogPropsType,
  'open' | 'onClose' | 'container' | 'contactChannel' | 'savedCaseType'
> & {
  contactOption?: ContactType | null;
  legacyDocumentId?: number;
  documentName?: string;
};

export type ContactChannelType =
  | 'behandelaar'
  | 'balie'
  | 'telefoon'
  | 'post'
  | 'email'
  | 'webformulier'
  | 'sociale media'
  | 'externe applicatie';
