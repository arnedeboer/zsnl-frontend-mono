// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { ContactType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import {
  getSavedCaseType,
  getContactSelectOption,
  getLegacyDocumentId,
  getDocumentName,
} from './CaseCreateDialog.library';
import {
  CaseCreateDialogPropsType,
  SavedCaseTypeType,
} from './CaseCreateDialog.types';
import { CaseCreateDialogForm } from './CaseCreateDialogForm';
import locale from './CaseCreateDialog.locale';

/**
 * Acts as a wrapper around the Case Create form, and loads in
 * the given contact's details and legacy document data from the backend
 * when applicable, and passes these to the form component.
 */
export const CaseCreateDialog: React.ComponentType<
  CaseCreateDialogPropsType
> = props => {
  const {
    selectedDocumentUuid,
    contact,
    open,
    onClose,
    container,
    contactChannel,
  } = props;
  const [legacyDocumentId, setLegacyDocumentId] = useState(0);
  const [documentName, setDocumentName] = useState('');
  const [contactOption, setContactOption] = useState<ContactType | null>(null);
  const [savedCaseType, setSavedCaseType] = useState<SavedCaseTypeType | null>(
    null
  );
  const [loaded, setLoaded] = useState(false);

  React.useEffect(() => {
    (async () => {
      if (!open) return;

      setLoaded(false);

      setSavedCaseType(await getSavedCaseType());

      if (contact) setContactOption(await getContactSelectOption(contact));
      if (selectedDocumentUuid) {
        setLegacyDocumentId(await getLegacyDocumentId(selectedDocumentUuid));
        setDocumentName(await getDocumentName(selectedDocumentUuid));
      }

      setLoaded(true);
    })();
  }, [open]);

  return loaded ? (
    <I18nResourceBundle resource={locale} namespace="caseCreate">
      <CaseCreateDialogForm
        open={open}
        onClose={onClose}
        container={container}
        contactOption={contactOption}
        legacyDocumentId={legacyDocumentId}
        documentName={documentName}
        contactChannel={contactChannel}
        savedCaseType={savedCaseType}
      />
    </I18nResourceBundle>
  ) : null;
};
