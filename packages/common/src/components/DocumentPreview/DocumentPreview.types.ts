// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type DocumentPreviewViewerType =
  | 'pdf-viewer'
  | 'image-viewer'
  | 'native';

export interface DocumentPreviewPropsType {
  /**
   * Document preview title
   */
  title?: string;

  /**
   * If provided renders a close cross in the header
   */
  onClose?: () => void;

  url?: string;

  contentType?: 'application/pdf' | string;

  /**
   * Defaults to false, only relevant for Viewer
   */
  pagination?: boolean;

  /**
   * Optional file download url. If not provided, url will be used
   */
  downloadUrl?: string;

  forceView?: DocumentPreviewViewerType;
}
