// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// @ts-ignore
// eslint-disable-next-line import/no-unresolved
import { useState } from '@storybook/addons';
//@ts-ignore
import { stories, text, boolean, select } from '@mintlab/ui/App/story';
import { DocumentPreview } from './DocumentPreview';
import { DocumentPreviewModal } from './DocumentPreviewModal';
import { DocumentPreviewPropsType } from './DocumentPreview.types';

const getPDFProps = (): DocumentPreviewPropsType => ({
  title: text('Title', 'Lorum-ipsum.pdf'),
  contentType: 'application/pdf',
  url: text(
    'PDF url',
    'https://s1.q4cdn.com/806093406/files/doc_downloads/test.pdf'
  ),
  onClose: boolean('Closeable', true) ? () => {} : undefined,
});

const getImageProps = (): DocumentPreviewPropsType => ({
  title: text('Title', 'Lorum-ipsum.jpg'),
  contentType: 'image/jpg',
  url: text(
    'Image url',
    'https://upload.wikimedia.org/wikipedia/commons/f/ff/Pizigani_1367_Chart_10MB.jpg'
  ),
  onClose: boolean('Closeable', true) ? () => {} : undefined,
});

const views = ['viewer', 'native'];

stories(module, __dirname, {
  Default() {
    return (
      <div style={{ height: 1000 }}>
        <DocumentPreview
          {...getPDFProps()}
          forceView={select('Viewer', views, 'viewer')}
        />
      </div>
    );
  },

  Native() {
    const props: DocumentPreviewPropsType = {
      ...getPDFProps(),
      forceView: 'native',
    };

    return (
      <div style={{ height: 1000 }}>
        <DocumentPreview {...props} />
      </div>
    );
  },

  PDFViewer() {
    const props: DocumentPreviewPropsType = {
      ...getPDFProps(),
      forceView: 'pdf-viewer',
      pagination: boolean('Pagination', true),
    };

    return (
      <div style={{ height: 1000 }}>
        <DocumentPreview {...props} />
      </div>
    );
  },

  ImageViewer() {
    const props: DocumentPreviewPropsType = {
      ...getImageProps(),
      forceView: 'image-viewer',
    };

    return (
      <div style={{ height: 500 }}>
        <DocumentPreview {...props} />
      </div>
    );
  },

  WithoutHeader() {
    return (
      <div style={{ height: 1000 }}>
        <DocumentPreview
          contentType="application/pdf"
          url={text(
            'PDF url',
            'https://s1.q4cdn.com/806093406/files/doc_downloads/test.pdf'
          )}
        />
      </div>
    );
  },

  Modal() {
    const [isOpen, setOpen] = useState(false);
    return (
      <React.Fragment>
        <button onClick={() => setOpen(true)} type="button">
          Open modal
        </button>
        <DocumentPreviewModal
          open={isOpen}
          {...getPDFProps()}
          onClose={() => setOpen(false)}
          forceView={select('Viewer', views, 'viewer')}
        />
      </React.Fragment>
    );
  },
});
