// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Suspense } from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import {
  DocumentPreviewPropsType,
  DocumentPreviewViewerType,
} from './DocumentPreview.types';
import { DocumentPreviewHeader } from './DocumentPreviewHeader/DocumentPreviewHeader';
import { Native } from './Native/Native';
import { isPdfSupported } from './library/featureDetection';
import { useDocumentPreviewStyles } from './DocumentPreview.style';
import ImageViewer from './Viewer/ImageViewer';

const PDFViewer = React.lazy(
  () => import(/* webpackChunkName: "pdf-viewer" */ './Viewer/PDFViewer')
);

const getViewerType = (
  contentType: DocumentPreviewPropsType['contentType']
): DocumentPreviewViewerType => {
  if (contentType === 'application/pdf') {
    return isPdfSupported() ? 'native' : 'pdf-viewer';
  }

  return 'image-viewer';
};

export const DocumentPreview = React.forwardRef<
  HTMLDivElement,
  DocumentPreviewPropsType
>((props, ref) => {
  const classes = useDocumentPreviewStyles();
  const { forceView, contentType } = props;

  const view = forceView ? forceView : getViewerType(contentType);
  const displayHeader = [props.title, props.onClose, props.downloadUrl].some(
    item => typeof item !== 'undefined'
  );
  const viewerProps = {
    ...props,
    className: classes.viewer,
  };

  return (
    <div ref={ref} tabIndex={-1} className={classes.wrapper}>
      {displayHeader && (
        <DocumentPreviewHeader {...props} downloadUrl={props.downloadUrl} />
      )}
      <Suspense fallback={<Loader delay={200} />}>
        {view === 'pdf-viewer' && <PDFViewer {...viewerProps} />}
        {view === 'image-viewer' && <ImageViewer {...viewerProps} />}
        {view === 'native' && <Native {...viewerProps} />}
      </Suspense>
    </div>
  );
});

DocumentPreview.displayName = 'DocumentPreview';

export default DocumentPreview;
