// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as yup from 'yup';
import { FormikErrors } from 'formik';

export type FlatFormValue = string | number | boolean;
export type NestedFormValue = {
  value: FlatFormValue;
  label?: FormValue;
  [key: string]: any;
};
export type FormValue = FlatFormValue | NestedFormValue;

export type AnyFormValueType = FormValue | FormValue[] | null;

export interface GenericFormDefinitionFieldAttributes<
  FormShape,
  Config,
  ValueType
> {
  type: string;
  name: keyof FormShape & string;
  hidden?: boolean;
  required?: boolean;
  disabled?: boolean;
  value: ValueType;
  placeholder?: string;
  label?: string;
  config?: Config;
  multiValue?: boolean;
  choices?: any[];
  applyBackgroundColor?: boolean;
  getError?: (
    errors: FormikErrors<FormShape>,
    name: keyof FormShape & string
  ) => string | undefined;
  [key: string]: any;
}

export type PartialFormValuesType<FormShape> = Partial<FormShape> & {
  [key: string]: any;
};

export type FormValuesType<FormShape = any> = FormShape;

export type RegisterValidationForNamespaceType = (
  schema: yup.ObjectSchema<{ [k: string]: yup.AnySchema }>,
  namespace: string
) => void;

export type WithNamespaceType<FormShape> = (
  value: keyof FormShape & string
) => keyof FormShape & string;
