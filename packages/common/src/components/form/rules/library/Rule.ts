// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormDefinition } from '../../types/formDefinition.types';
import getFieldByName, {
  getAllFieldsAsObject,
} from '../../library/getFieldByName';
import {
  RuleEngineAllFieldsCondition,
  RuleEngineCondition,
} from './conditions';
import { RuleEngineActionExecution } from './actions';
import triggerActions from './triggerActions';

export class Rule<Fields = any> {
  private ifFn: RuleEngineCondition | undefined;
  private ifFnAll: RuleEngineAllFieldsCondition<keyof Fields> | undefined;
  private thenFn: RuleEngineActionExecution[] = [];
  private elseFn: RuleEngineActionExecution[] = [];
  private activeAction: string | null = null;
  private activeFieldName?: keyof Fields;

  when(fn: RuleEngineAllFieldsCondition<keyof Fields>, arg2?: never): this;
  when(fieldName: keyof Fields, fn: RuleEngineCondition): this;
  when(arg1: any, arg2: any): this {
    if (typeof arg1 === 'string') {
      this.activeFieldName = arg1 as keyof Fields;
      this.ifFn = arg2;
    } else {
      this.ifFnAll = arg1;
    }

    return this;
  }

  then(fn: RuleEngineActionExecution) {
    this.activeAction = 'then';
    this.thenFn.push(fn);
    return this;
  }

  else(fn: RuleEngineActionExecution) {
    this.activeAction = 'else';
    this.elseFn.push(fn);
    return this;
  }

  and(fn: RuleEngineActionExecution) {
    switch (this.activeAction) {
      case 'then':
        return this.then(fn);

      case 'else':
        return this.else(fn);

      default:
        throw new Error(
          'Cannot call and(...) before calling then(...) or when(...)'
        );
    }
  }

  /* eslint complexity: [2, 9] */
  execute(formDefinition: FormDefinition<Fields>) {
    const field = getFieldByName<Fields>(formDefinition, this.activeFieldName);
    const allFields = getAllFieldsAsObject<Fields>(formDefinition);

    if (field && this.ifFn && this.ifFn(field)) {
      return triggerActions(this.thenFn, formDefinition);
    } else if (this.ifFnAll && this.ifFnAll(allFields)) {
      return triggerActions(this.thenFn, formDefinition);
    } else if ((field && this.ifFn) || this.ifFnAll) {
      return triggerActions(this.elseFn, formDefinition);
    } else {
      return formDefinition;
    }
  }
}

export default Rule;
