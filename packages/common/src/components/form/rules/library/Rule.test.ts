// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { AnyFormDefinitionField } from '../../types';
import Rule from './Rule';
import { hasValue, valueEquals } from './conditions';
import { setRequired, hideFields } from './actions';

describe('Rule engine', () => {
  describe('Rule creation', () => {
    const getTestData = (): AnyFormDefinitionField<any>[] => [
      {
        name: 'test',
        format: 'text',
        type: 'text',
        value: 'test value',
      },
      {
        name: 'result',
        format: 'text',
        type: 'text',
        value: '',
      },
    ];

    test('Can construct simple rule', () => {
      const rule = new Rule()
        .when('test', hasValue)
        .then(setRequired(['result']));

      const [, result] = rule.execute(getTestData());
      //@ts-ignore
      expect(result.required).toEqual(true);
    });

    test('Rule can have a `else` action', () => {
      const rule = new Rule()
        .when('test', valueEquals('something'))
        .then(setRequired(['result']))
        .else(hideFields(['result']));

      const [, result] = rule.execute(getTestData());
      //@ts-ignore
      expect(result.required).toBeFalsy();
      //@ts-ignore
      expect(result.hidden).toEqual(true);
    });

    test('Rule can have multiple `then` actions', () => {
      const rule = new Rule()
        .when('test', hasValue)
        .then(setRequired(['result']))
        .and(hideFields(['result']));

      const [, result] = rule.execute(getTestData());
      //@ts-ignore
      expect(result.required).toEqual(true);
      //@ts-ignore
      expect(result.hidden).toEqual(true);
    });

    test('Rule can have multiple `else` actions', () => {
      const rule = new Rule()
        .when('test', valueEquals('something'))
        .else(setRequired(['result']))
        .and(hideFields(['result']));

      const [, result] = rule.execute(getTestData());
      //@ts-ignore
      expect(result.required).toEqual(true);
      //@ts-ignore
      expect(result.hidden).toEqual(true);
    });

    test('Rule can implement condition over all fields', () => {
      const rule = new Rule()
        .when(fields => hasValue(fields.test))
        .then(setRequired(['result']));

      const [, result] = rule.execute(getTestData());
      //@ts-ignore
      expect(result.required).toEqual(true);
    });
  });
});
