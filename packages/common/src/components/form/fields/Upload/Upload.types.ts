// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export interface FileObject {
  uuid?: string;
  value: string | null;
  status: 'init' | 'pending' | 'failed';
  errorCode?: string;
  key: string;
  label: any;
}
