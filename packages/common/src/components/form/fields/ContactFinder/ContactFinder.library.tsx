// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { ContactFinderConfigType, ContactType } from './ContactFinder.types';

type SearchContactRequestParams = APICommunication.SearchContactRequestParams;
type SearchContactResponseBody = APICommunication.SearchContactResponseBody;

export const fetchContactChoices =
  (
    config = defaultConfig as ContactFinderConfigType,
    onError: OpenServerErrorDialogType
  ) =>
  async (input: string) => {
    const filters = config.subjectTypes
      ? {
          'filter[type]': config.subjectTypes.join(','),
        }
      : {};

    const body = await request<SearchContactResponseBody>(
      'GET',
      buildUrl<SearchContactRequestParams>(
        `/api/v2/communication/search_contact`,
        {
          keyword: input,
          ...filters,
        }
      )
    ).catch(onError);

    return body
      ? body.data
          .map<ContactType | null>(contact => {
            const item = createDefaultItem(contact);
            return config.itemResolver
              ? config.itemResolver(contact, item)
              : item;
          })
          .filter((item): item is ContactType => Boolean(item))
      : [];
  };

const defaultConfig: Required<ContactFinderConfigType> = {
  subjectTypes: [],
  itemResolver: (contact, defaultItem) => defaultItem,
};

const createDefaultItem = ({
  id,
  attributes: { name, address, type },
}: APICommunication.ContactEntity): ContactType => ({
  value: id,
  label: name,
  ...(address ? { subLabel: address } : {}),
  type: type,
  icon: (
    <ZsIcon size="tiny">
      {type === 'organization'
        ? 'entityType.inverted.organization'
        : 'entityType.inverted.person'}
    </ZsIcon>
  ),
});
