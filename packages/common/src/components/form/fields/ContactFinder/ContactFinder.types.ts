// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ReactElement } from 'react';
import { APICommunication } from '@zaaksysteem/generated';

export type SearchContactType = APICommunication.ContactEntity;
export type SubjectTypeType =
  | undefined
  | ('employee' | 'person' | 'organization')[];

export type ContactType = {
  value: string;
  label: string;
  subLabel?: string;
  type: 'employee' | 'person' | 'organization';
  icon?: ReactElement;
};

type ItemResolverType = (
  contact: APICommunication.ContactEntity,
  defaultItem: ContactType
) => ContactType | null;

export type ContactFinderConfigType = {
  itemResolver?: ItemResolverType;
  subjectTypes?: SubjectTypeType;
  [key: string]: any;
};
