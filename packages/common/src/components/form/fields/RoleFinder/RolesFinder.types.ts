// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';

type RolesType = APICaseManagement.GetRolesResponseBody['data'];
type RoleType = RolesType[number];

export type GetSummaryType = (role: RoleType) => string;

export type SortRolesType = (roles: RolesType) => RolesType;

type MappedRolesType = {
  value: string;
  label: string;
}[];

export type MapRolesType = (roles: RolesType) => MappedRolesType;
