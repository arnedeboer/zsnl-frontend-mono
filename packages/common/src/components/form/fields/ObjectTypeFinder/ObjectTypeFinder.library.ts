// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';

export const fetchObjectTypeChoices =
  (onError: OpenServerErrorDialogType) => async (keyword: string) => {
    const body = await request<APICaseManagement.SearchResponseBody>(
      'GET',
      buildUrl<APICaseManagement.SearchRequestParams>('/api/v2/cm/search', {
        keyword,
        type: 'custom_object_type' as any,
      })
    ).catch(onError);

    return body
      ? (body.data || []).map(({ id = '', meta }) => ({
          value: id,
          label: meta?.summary || '',
        }))
      : [];
  };
