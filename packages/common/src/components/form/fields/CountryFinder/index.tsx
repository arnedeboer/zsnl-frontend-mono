// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { ReadonlyValuesContainer } from '../../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../../types/Form2.types';
import { CountryFinder } from './CountryFinder';

const CountryFinderWrapper: FormFieldComponentType<any> = props => {
  const { value, readOnly } = props;

  return readOnly ? (
    <ReadonlyValuesContainer value={props.value} />
  ) : (
    <CountryFinder {...props} value={value || null} />
  );
};

export default CountryFinderWrapper;
