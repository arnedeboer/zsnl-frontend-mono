// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormFieldComponentType } from '../../types/Form2.types';
import { fetchCountries } from './CountryFinder.library';

export const CountryFinder: FormFieldComponentType<ValueType<string>, any> = ({
  value,
  multiValue,
  styles,
  config,
  ...restProps
}) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  return (
    <React.Fragment>
      <DataProvider
        autoProvide={true}
        provider={fetchCountries(openServerErrorDialog)}
        providerArguments={[]}
      >
        {({ data, busy, provide }) => {
          const normalizedChoices = data || [];

          return (
            <Select
              {...restProps}
              choices={normalizedChoices}
              value={value}
              isClearable={true}
              loading={busy}
              getChoices={provide}
              disabled={!isPopulatedArray(normalizedChoices)}
              isMulti={multiValue}
              multiValueLabelIcon={config?.multiValueLabelIcon}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};
