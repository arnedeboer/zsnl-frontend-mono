// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICatalog } from '@zaaksysteem/generated';

export const generateMagicString = (
  input: string,
  onError: any
): Promise<string> =>
  request<APICatalog.GenerateMagicStringResponseBody>(
    'GET',
    // dont use buildUrl because it breaks how spaces are handled
    '/api/v2/admin/catalog/generate_magic_string?string_input=' +
      encodeURIComponent(input),
    null,
    { type: 'json', cached: true }
  )
    .catch(onError)
    .then(body => body?.data.magic_string);
