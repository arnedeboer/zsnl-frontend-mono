// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { ReadonlyValuesContainer } from '../../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../../types/Form2.types';
import { usePersonNumberStylesheet } from './PersonalNumber.style';
import { getPersonalNumber } from './getPersonalNumber';

const bulletPoint = '\u25CF';
const personalNumberMask = bulletPoint.repeat(9);

const PersonalNumber: FormFieldComponentType<string> = props => {
  const [value, setValue] = useState(personalNumberMask);
  const [t] = useTranslation('common');
  const classes = usePersonNumberStylesheet();
  const {
    readOnly,
    value: updatedValue,
    definition: {
      config: { uuid, authenticated },
    },
  } = props;

  const updateValue = (newValue: string | undefined) => {
    if (newValue) {
      setValue(newValue);
    } else {
      setValue('');
    }
  };

  return !readOnly ? (
    <div className={classes.wrapper}>
      <TextField
        {...props}
        value={updatedValue ?? value}
        disabled={value === personalNumberMask || authenticated}
      />
      {value === personalNumberMask && (
        <Button
          className={classes.button}
          presets={['contained', 'primary']}
          action={() => getPersonalNumber(updateValue, uuid)}
        >
          {t('forms.view')}
        </Button>
      )}
    </div>
  ) : (
    <ReadonlyValuesContainer value={personalNumberMask} />
  );
};

export default PersonalNumber;
