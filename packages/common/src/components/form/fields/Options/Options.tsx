// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@mintlab/ui/App/Material/Button';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import Render from '@mintlab/ui/App/Abstract/Render';
import { Subtitle2 } from '@mintlab/ui/App/Material/Typography';
import ErrorLabel from '@mintlab/ui/App/Zaaksysteem/FormHelpers/library/ErrorLabel';
//@ts-ignore
import { unique, asArray } from '@mintlab/kitchen-sink/source';
import {
  DragDropContext,
  Draggable,
  Droppable,
  DropResult,
} from 'react-beautiful-dnd';
import { FormFieldComponentType } from '../../types/Form2.types';
import Option from './Option';
import OptionsList from './OptionsList';
import { useOptionsStylesheet } from './Options.style';
import { ConfigType, ItemValueType } from './Options.types';

/* eslint complexity: [2, 8] */
const Options: FormFieldComponentType<ItemValueType[], ConfigType> = ({
  value: valueProp,
  t,
  setFieldValue,
  name,
  config: { allowNew = false, showHideInactive = false },
}) => {
  const [newItem, setNewItem] = useState('');
  const [showInactive, setShowInactive] = useState(true);
  const [error, setError] = useState(null as any);
  const value = valueProp || [];
  const classes = useOptionsStylesheet();

  const filteredItems = asArray(value).filter(filteredItem =>
    showInactive ? true : filteredItem.active
  );

  const hasInactiveItems = asArray(value).filter(
    thisItem => !thisItem.active
  ).length;

  const addItem = () => {
    if (!newItem.trim()) return;

    if (
      value.find(
        thisItem => thisItem.value.toLowerCase() === newItem.toLowerCase()
      )
    ) {
      setError(t('attribute:dialog.duplicateName'));
      return;
    }

    const newItems = [
      ...value,
      {
        id: unique(),
        value: newItem,
        active: true,
        isNew: true,
      },
    ];

    setFieldValue(name, newItems);
    setNewItem('');
  };

  const onDragEnd = (result: DropResult) => {
    if (!result.destination) return;
    const newItems = reorder(result.source.index, result.destination.index);
    setFieldValue(name, newItems);
  };

  const reorder = (startIndex: number, endIndex: number) => {
    const result = Array.from(value);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  };

  const deleteItem = (item: ItemValueType) => {
    const newItems = value.filter(thisItem => item.id !== thisItem.id);
    setFieldValue(name, newItems);
  };

  const switchActive = (item: ItemValueType) => {
    const newItems = value.map(thisItem => {
      if (item.value === thisItem.value) {
        return {
          ...item,
          active: !thisItem.active,
        };
      }
      return thisItem;
    });
    setFieldValue(name, newItems);
  };

  const onKeyPress = (event: any) => {
    const { key } = event;

    setError(null);

    if (key.toLowerCase() === 'enter') {
      addItem();
    }
  };

  return (
    <div className={classes.optionsWrapper}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="attribute-options-droppable">
          {(provided, snapshot) => (
            <OptionsList provided={provided} snapshot={snapshot}>
              {filteredItems.map((item, index) => (
                <Draggable
                  key={item.value}
                  draggableId={item.value}
                  index={index}
                >
                  {(draggableProvided, draggableSnapshot) => (
                    <Option
                      item={item}
                      //@ts-ignore
                      index={index}
                      provided={draggableProvided}
                      snapshot={draggableSnapshot}
                      onDelete={deleteItem}
                      onSwitchActive={switchActive}
                      t={t}
                    />
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </OptionsList>
          )}
        </Droppable>
      </DragDropContext>
      {allowNew ? (
        <TextField
          classes={{
            formControl: classes.addNew,
          }}
          name="attribute-options-new"
          value={newItem}
          placeholder={t('attribute:dialog.namePlaceholder')}
          onKeyPress={onKeyPress}
          onChange={({ target }: any) => setNewItem(target.value)}
          InputProps={{
            endAdornment: (
              <InputAdornment
                classes={{
                  root: classes.adornment,
                }}
                position="end"
              >
                <Button
                  action={addItem}
                  label={t('attribute:dialog.addOption')}
                  presets={['primary', 'medium', 'icon']}
                >
                  add
                </Button>
              </InputAdornment>
            ),
          }}
        />
      ) : null}

      <Render condition={error}>
        <ErrorLabel label={error} classes={{ root: classes.errorLabel }} />
      </Render>

      <Render
        condition={Boolean(
          showHideInactive && asArray(value).length && hasInactiveItems
        )}
      >
        <Button
          action={() => setShowInactive(!showInactive)}
          presets={['text']}
          classes={{
            root: classes.showHideButton,
          }}
          label={
            showInactive
              ? t('attribute:dialog.hideInactive')
              : t('attribute:dialog.showInactive')
          }
        >
          <Subtitle2
            classes={{
              root: classes.showHide,
            }}
          >
            {showInactive
              ? t('attribute:dialog.hideInactive')
              : t('attribute:dialog.showInactive')}
          </Subtitle2>
        </Button>
      </Render>
    </div>
  );
};

export default Options;
