// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import {
  DroppableProvided,
  DraggableProvided,
  DroppableStateSnapshot,
  DraggableStateSnapshot,
} from 'react-beautiful-dnd';
import * as i18next from 'i18next';

export type ConfigType = {
  allowNew: boolean;
  showHideInactive: boolean;
};

export type ItemValueType = {
  id: string;
  value: string;
  active: boolean;
  isNew: boolean;
};

export type OptionsListType = {
  provided: DroppableProvided;
  snapshot: DroppableStateSnapshot;
  children: React.ReactNode[];
};

export type OptionPropsType = {
  item: ItemValueType;
  provided: DraggableProvided;
  snapshot: DraggableStateSnapshot;
  onDelete: (item: ItemValueType) => void;
  onSwitchActive: (item: ItemValueType) => void;
  t: i18next.TFunction;
};
