// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { ReadonlyLinkContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

export const WebAddressField: FormFieldComponentType<string> = props => {
  const { value, readOnly } = props;

  return readOnly ? (
    <ReadonlyLinkContainer value={props.value} />
  ) : (
    <TextField {...props} value={value || ''} />
  );
};
