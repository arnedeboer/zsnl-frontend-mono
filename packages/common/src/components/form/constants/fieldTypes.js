// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const CHECKBOX = 'checkbox';
export const CHECKBOX_GROUP = 'checkbox_group';
export const RADIO_GROUP = 'radio_group';
export const EMAIL = 'email';
export const MAGICSTRING = 'magicString';
export const NAME = 'name';
export const OPTIONS = 'options';
export const RELATIONSHIP = 'relationship';
export const SELECT = 'select';
export const TEXT = 'text';
export const NUMERIC = 'numeric';
export const MULTI_VALUE_TEXT = 'multiValueText';
export const TEXTAREA = 'textarea';
export const TYPES = 'types';
export const VERSIONS = 'versions';
export const UPLOAD = 'file';
export const FLATVALUE_SELECT = 'flatValueSelect';
export const CHOICE_CHIP = 'choiceChip';
export const DATEPICKER = 'datePicker';
export const KEYBOARDDATEPICKER = 'keyboardDatePicker';
export const GEOJSON_MAP_INPUT = 'geoJsonInput';
export const IBAN = 'iban';
export const CURRENCY = 'valuta';
export const WEB_ADDRESS = 'webAddress';
export const ADDRESS_MAP = 'address';
export const ADDRESS_BOX = 'addressBox';
export const CASE_FINDER = 'caseFinder';
export const OBJECT_FINDER = 'objectFinder';
export const OBJECT_TYPE_FINDER = 'objectTypeFinder';
export const ROLE_FINDER = 'roleFinder';
export const CASE_ROLE_FINDER = 'caseRoleFinder';
export const CASE_DOCUMENT_FINDER = 'caseDocumentFinder';
export const CASE_TYPE_FINDER = 'caseTypeFinder';
export const CONTACT_FINDER = 'contactFinder';
export const DEPARTMENT_FINDER = 'departmentFinder';
export const CASE_REQUESTOR = 'caseRequestor';
export const PERSONAL_NUMBER = 'personalNumber';
export const PHONE_NUMBER = 'phoneNumber';
export const MAGIC_STRING_GENERATOR = 'magicStringGenerator';
export const CONTACT_FINDER_WITH_CONTACT_IMPORTER =
  'contactFinderWithContactImporter';
export const COUNTRY_FINDER = 'countryFinder';
