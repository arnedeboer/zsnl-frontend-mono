// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment, useState } from 'react';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { Alert } from '@zaaksysteem/common/src/components/dialogs/Alert/Alert';
import { DocumentItemType } from '../../../FileExplorer/types/FileExplorerTypes';
import { useRejectedStyles } from './Rejected.styles';

type RejectedButtonPropsType = {
  t: i18next.TFunction;
  rowData: DocumentItemType;
  classes: any;
};
const Rejected: React.FunctionComponent<RejectedButtonPropsType> = ({
  t,
  rowData,
  classes,
}) => {
  const [alertOpen, setAlertOpen] = useState(false);
  const rejectedClasses = useRejectedStyles();
  return (
    <Fragment>
      <Button
        className={classes.statusIndicator}
        component="a"
        action={(event: React.MouseEvent) => {
          event.stopPropagation();
          event.preventDefault();
          setAlertOpen(true);
        }}
      >
        {t('DocumentExplorer:columns.name.rejected')}
      </Button>
      <Alert
        open={alertOpen}
        onClose={() => setAlertOpen(false)}
        title={t('DocumentExplorer:columns.name.rejectedTitle')}
        primaryButton={{
          text: t('common:dialog.close'),
          action: event => {
            event.stopPropagation();
            event.preventDefault();
            setAlertOpen(false);
          },
        }}
      >
        <div className={rejectedClasses.table}>
          <div className={rejectedClasses.row}>
            <span>{t('DocumentExplorer:columns.name.rejectedBy')}:</span>
            <span>{rowData.rejectionName}</span>
          </div>
          <div className={rejectedClasses.row}>
            <span>{t('DocumentExplorer:columns.name.rejectedReason')}:</span>
            <span> {rowData.rejectionReason}</span>
          </div>
        </div>
      </Alert>
    </Fragment>
  );
};

export default Rejected;
