// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useState, useEffect, useRef } from 'react';
import { InfiniteLoader } from 'react-virtualized';
import { RefObject } from 'react';

export type GetDataReturnType<T> = {
  rows: T[];
  totalResults?: number;
};

export type getDataFuncType<T, P> = ({
  pageNum,
  ...params
}: {
  pageNum: number;
} & P) => Promise<GetDataReturnType<T>>;

export type ParamsType<T, P> = {
  ref: RefObject<InfiniteLoader>;
  pageLength: number;
  getData: getDataFuncType<T, P>;
  getDataParams: P;
};

export type ReturnObjType<T> = {
  list: T[];
  isRowLoaded: ({ index }: { index: number }) => boolean;
  loadMoreRows: ({ startIndex }: { startIndex: number }) => Promise<any>;
  resetList: () => void;
  loading: boolean;
  totalResults?: number;
};

export function useInfiniteScroll<T, P>({
  ref,
  pageLength,
  getData,
  getDataParams,
}: ParamsType<T, P>): ReturnObjType<T> {
  const [list, setList] = useState([] as T[]);
  const [totalResults, setTotalResults] = useState<number | undefined>(
    undefined
  );
  const [loading, setLoading] = useState(false);
  const [pagesLoaded, setPagesLoaded] = useState(0);
  const firstTimeRender = useRef(true);

  useEffect(() => {
    const filtersReset = !firstTimeRender.current && pagesLoaded === 0 && ref;
    if (filtersReset) {
      //@ts-ignore
      ref.current.resetLoadMoreRowsCache();
      loadMoreRows({ startIndex: 1 });
    }
  }, [pagesLoaded]);

  useEffect(() => {
    firstTimeRender.current = false;
  }, []);

  function isRowLoaded({ index }: { index: number }) {
    return !!list[index];
  }

  const calculateNextPageNumber = ({ startIndex }: { startIndex: number }) => {
    const nextStart = pagesLoaded * pageLength;
    const nextEnd = nextStart + pageLength;

    if (startIndex === 0) {
      return 1;
    } else if (startIndex >= nextStart && startIndex <= nextEnd) {
      return pagesLoaded + 1;
    } else {
      return pagesLoaded;
    }
  };

  const resetList = () => {
    setList([]);
    setPagesLoaded(0);
  };

  const loadMoreRows = ({ startIndex }: { startIndex: number }) => {
    const pageNum = calculateNextPageNumber({ startIndex });

    if (pageNum === pagesLoaded || loading) {
      return Promise.resolve([]);
    }

    setLoading(true);
    return getData({ pageNum, ...getDataParams }).then(data => {
      const { rows, totalResults } = data;
      setPagesLoaded(pageNum);
      setLoading(false);
      if (rows) {
        const newList = [...list, ...rows];
        setList(newList);
      }

      setTotalResults(totalResults);
      return data;
    });
  };

  return { list, isRowLoaded, loadMoreRows, resetList, loading, totalResults };
}

export default useInfiniteScroll;
