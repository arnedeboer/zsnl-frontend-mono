// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { AnyAction } from 'redux';
import { AJAX_STATE_INIT } from '../../library/redux/ajax/createAjaxConstants';
import {
  handleAjaxStateChange,
  WithAjaxState,
} from '../../library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '../../library/redux/ajax/createAjaxAction';
import { SESSION_FETCH } from './session.constants';

export type SessionType = {
  active_interfaces: ['email'];
  configurable: {
    bag_local_only: boolean;
    bag_priority_gemeentes: string[];
    show_object_v1: boolean;
    show_object_v2: boolean;
  };
  logged_in_user: {
    display_name: string;
    id: string;
    system_roles: string[];
    uuid?: string;
    capabilities: ('admin' | string)[];
  };
};

export interface SessionStateType extends WithAjaxState {
  data?: SessionType;
}

export interface SessionRootStateType {
  session: SessionStateType;
}

const initialState: SessionStateType = {
  state: AJAX_STATE_INIT,
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
const fetchSuccess = (
  state: SessionStateType,
  action: AjaxAction<{ [key: string]: any }>
): SessionStateType => {
  const data = action?.payload?.response?.result?.instance;
  return {
    ...state,
    data: data ? data : undefined,
  };
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function session(
  state: SessionStateType = initialState,
  action: AnyAction
): SessionStateType {
  const handleAjaxState = handleAjaxStateChange(SESSION_FETCH);

  switch (action.type) {
    case SESSION_FETCH.SUCCESS:
      return fetchSuccess(
        handleAjaxState<SessionStateType>(
          state,
          action as AjaxAction<{
            [key: string]: any;
          }>
        ),
        action as AjaxAction<{ [key: string]: any }>
      );
    default:
      return state;
  }
}
