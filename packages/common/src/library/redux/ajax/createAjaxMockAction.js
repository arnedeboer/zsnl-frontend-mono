// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint-disable valid-jsdoc */

/**
 * @param {{PENDING: string, ERROR: string, SUCCESS: string}} constants
 * @returns {(options: { method: string, url: string, data: Object, payload: Object, headers: Object})
 *  => (dispatch: Function)
 *    => Promise
 * }
 */
export const createAjaxAction =
  constants =>
  (mockedResponse, responseTime = 300) =>
  ({ url, payload }) =>
  dispatch => {
    console.warn(
      `Using createAjaxMockAction for ${url} with a fake response time of ${responseTime} MS`
    );

    dispatch({
      type: constants.PENDING,
      ajax: true,
      payload: {
        ...payload,
      },
    });

    const merge = body => ({
      ajax: true,
      payload: {
        ...payload,
        response: body,
      },
    });

    setTimeout(() => {
      dispatch({
        ...merge(mockedResponse),
        type: constants.SUCCESS,
      });
    }, responseTime);
  };
