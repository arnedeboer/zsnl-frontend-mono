// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  AJAX_STATE_ERROR,
  AJAX_STATE_PENDING,
  AJAX_STATE_VALID,
  AjaxActionConstants,
  AjaxState,
} from './createAjaxConstants';
import { AjaxAction } from './createAjaxAction';

/**
 * Reducer for handling Ajax state changes (pending, valid and error)
 */

export interface WithAjaxState {
  state: AjaxState;
}

type HandleAjaxStateChange = (
  constants: AjaxActionConstants,
  key?: string
) => <S extends WithAjaxState>(state: S, action: AjaxAction<any>) => S;

export const handleAjaxStateChange: HandleAjaxStateChange =
  (constants, key = 'state') =>
  (state, action) => {
    switch (action.type) {
      case constants.PENDING:
        return {
          ...state,
          [key]: AJAX_STATE_PENDING,
        };

      case constants.SUCCESS:
        return {
          ...state,
          [key]: AJAX_STATE_VALID,
        };

      case constants.ERROR:
        return {
          ...state,
          [key]: AJAX_STATE_ERROR,
        };

      default:
        return state;
    }
  };

export default handleAjaxStateChange;
