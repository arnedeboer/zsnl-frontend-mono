// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';

export const translateKeyGlobally = (
  t: i18next.TFunction,
  code: string
): string | null => {
  const language = i18next.default.language;
  const namespaces = Object.keys(i18next.default.store.data[language]);
  return namespaces.reduce((acc, ns) => {
    const key = `${ns}:${code}`;
    if (i18next.default.exists(key)) {
      acc = t(key);
    }
    return acc;
  }, null);
};
