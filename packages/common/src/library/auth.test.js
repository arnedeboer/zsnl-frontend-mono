// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getUserNavigation, isLoggedIn } from './auth';

describe('The `auth` module', () => {
  describe('exports a `getUserNavigation` function that', () => {
    test('returns `false` if the `logged_in_user` property is falsy', () => {
      const actual = isLoggedIn({
        logged_in_user: null,
      });
      const asserted = false;

      expect(actual).toBe(asserted);
    });

    test('returns `true` if the `logged_in_user` property is an object', () => {
      const actual = isLoggedIn({
        logged_in_user: {},
      });
      const asserted = true;

      expect(actual).toBe(asserted);
    });
  });

  describe('exports a `getUserNavigation` function that', () => {
    test('filters an array of objects on its `capability` property values', () => {
      const navigation = [
        {
          capability: 'foo',
        },
        {
          capability: 'bar',
        },
        {
          capability: 'quux',
        },
      ];
      const capabilities = ['foo', 'quux'];

      const actual = getUserNavigation(navigation, capabilities);
      const asserted = [
        {
          capability: 'foo',
        },
        {
          capability: 'quux',
        },
      ];

      expect(actual).toEqual(asserted);
    });

    test('does not filter anything if `capability` includes `admin`', () => {
      const navigation = [
        {
          capability: 'foo',
        },
        {
          capability: 'bar',
        },
      ];
      const capabilities = ['quux', 'admin', 'qwerty'];

      const actual = getUserNavigation(navigation, capabilities);
      const asserted = [
        {
          capability: 'foo',
        },
        {
          capability: 'bar',
        },
      ];

      expect(actual).toEqual(asserted);
    });
  });
});
