// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { CommunicationContextType } from '../../types/Context.types';
import { COMMUNICATION_SET_CONTEXT } from './communincation.context.constants';
import { CommunicationSetContextActionPayload } from './communication.context.actions';

export type CommunicationContextState = CommunicationContextType & {
  capabilities: { canCreate: boolean };
};

const initialState: CommunicationContextState = {
  rootPath: '/',
  context: 'inbox',
  capabilities: {
    allowSplitScreen: false,
    canAddAttachmentToCase: false,
    canAddSourceFileToCase: false,
    canAddThreadToCase: false,
    canCreate: false,
    canCreateContactMoment: false,
    canCreatePipMessage: false,
    canCreateEmail: false,
    canCreateNote: false,
    canCreateMijnOverheid: false,
    canDeleteMessage: false,
    canImportMessage: false,
    canFilter: false,
    canSelectCase: false,
    canSelectContact: false,
    canOpenPDFPreview: false,
  },
};

const context: Reducer<
  CommunicationContextState,
  ActionWithPayload<CommunicationSetContextActionPayload>
> = (state = initialState, action) => {
  if (action.type === COMMUNICATION_SET_CONTEXT) {
    const {
      canCreateContactMoment,
      canCreateEmail,
      canCreateMijnOverheid,
      canCreateNote,
      canCreatePipMessage,
    } = action.payload.capabilities;
    return {
      ...state,
      ...action.payload,
      capabilities: {
        ...action.payload.capabilities,
        canCreate: [
          canCreateContactMoment,
          canCreateEmail,
          canCreateMijnOverheid,
          canCreateNote,
          canCreatePipMessage,
        ].some(Boolean),
      },
    };
  }

  return state;
};

export default context;
