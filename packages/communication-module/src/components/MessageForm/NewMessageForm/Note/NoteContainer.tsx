// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import GenericMessageForm from '../../GenericMessageForm/GenericMessageForm';
import { saveNote } from '../../../../store/add/communication.add.actions';
import { CommunicationRootStateType } from '../../../../store/communication.reducer';
import { SaveNoteFormValuesType } from '../../../../types/Message.types';
import { GenericMessageFormPropsType } from '../../GenericMessageForm/GenericMessageForm.types';
import formDefinition from './note.formDefinition';

type PropsFromStateType = Pick<
  GenericMessageFormPropsType<SaveNoteFormValuesType>,
  'busy' | 'formDefinition' | 'formName'
>;

type PropsFromDispatchType = Pick<
  GenericMessageFormPropsType<SaveNoteFormValuesType>,
  'save'
>;

type PropsFromOwnType = Pick<GenericMessageFormPropsType, 'cancel'>;

const mapStateToProps = (
  stateProps: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      add: { state },
    },
  } = stateProps;

  return {
    formDefinition,
    busy: state === AJAX_STATE_PENDING,
    formName: 'note',
  };
};

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => {
  return {
    save(values) {
      dispatch(saveNote({ values }) as any);
    },
  };
};

const connected = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  PropsFromOwnType,
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(GenericMessageForm);

export default connected;
