// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  FormDefinition,
  FormDefinitionField,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { CaseRequestorConfigType } from '@zaaksysteem/common/src/components/form/fields/CaseRequestor/CaseRequestor.types';
import {
  createDisplayName,
  isNotPresetRequestor,
} from '@zaaksysteem/common/src/components/form/fields/CaseRoleFinder/CaseRoleFinder.library';
import { CASE_SELECTOR } from '../../../library/communicationFieldTypes.constants';
import { SaveMessageFormValuesType } from '../../../types/Message.types';
import { CommunicationContextContextType } from '../../../types/Context.types';

type SimpleExternalMessageFormDefinitionPayloadType = {
  caseUuid?: string;
  contactUuid?: string;
  context: CommunicationContextContextType;
};

/* eslint complexity: [2, 8] */
export const getSimpleExternalMessageFormDefinition = ({
  caseUuid,
  contactUuid,
  context,
}: SimpleExternalMessageFormDefinitionPayloadType): FormDefinition<SaveMessageFormValuesType> => [
  ...(caseUuid && context !== 'pip'
    ? [
        <
          FormDefinitionField<
            SaveMessageFormValuesType,
            CaseRequestorConfigType
          >
        >{
          name: 'requestor',
          type: fieldTypes.CASE_REQUESTOR,
          value: null,
          applyBackgroundColor: true,
          config: {
            caseUuid,
            itemFilter: isNotPresetRequestor,
            valueResolver: role => ({
              label: createDisplayName(role),
              value: role.subject?.id,
            }),
          },
        },
      ]
    : []),
  ...(contactUuid
    ? [
        <FormDefinitionField<SaveMessageFormValuesType>>{
          name: 'case_uuid',
          type: CASE_SELECTOR,
          value: caseUuid || '',
          required: true,
          placeholder: 'communication:addFields.selectCase',
          label: 'communication:addFields.selectCase',
          isSearchable: false,
          config: {
            contactUuid,
          },
          readOnly: Boolean(caseUuid),
        },
      ]
    : [
        <FormDefinitionField<SaveMessageFormValuesType>>{
          name: 'case_uuid',
          value: caseUuid,
          type: fieldTypes.TEXT,
          hidden: true,
        },
      ]),
  {
    name: 'subject',
    type: fieldTypes.TEXT,
    multi: true,
    format: 'text',
    value: '',
    required: true,
    placeholder: 'communication:addFields.subjectPlaceholder',
    label: 'communication:addFields.subject',
  },
  {
    name: 'content',
    type: fieldTypes.TEXTAREA,
    multi: true,
    format: 'text',
    value: '',
    required: true,
    placeholder: 'communication:addFields.messagePlaceholder',
    label: 'communication:addFields.message',
    isMultiline: true,
    rows: 7,
  },
  ...(caseUuid && context !== 'pip'
    ? [
        <FormDefinitionField<SaveMessageFormValuesType>>{
          name: 'attachments',
          type: fieldTypes.CASE_DOCUMENT_FINDER,
          value: null,
          placeholder: 'communication:addFields.attachmentsPlaceholder',
          multiValue: true,
          applyBackgroundColor: true,
          config: { caseUuid },
        },
      ]
    : [
        <FormDefinitionField<SaveMessageFormValuesType>>{
          name: 'attachments',
          type: fieldTypes.UPLOAD,
          value: null,
          required: false,
          format: 'file',
          uploadDialog: true,
          multiValue: true,
          label: 'communication:addFields.attachments',
        },
      ]),
];

export default getSimpleExternalMessageFormDefinition;
