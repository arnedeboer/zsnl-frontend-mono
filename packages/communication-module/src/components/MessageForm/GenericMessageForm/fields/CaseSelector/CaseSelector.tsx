// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { fetchCases } from '../../../../../library/requests';
import { CaseChoiceType } from '../../../../../types/Message.types';
import locale from './locale';

export type CaseSelectorConfigType = {
  contactUuid: string;
};

const fetchCasesHandler = (contactUuid: string): Promise<CaseChoiceType[]> => {
  return fetchCases(contactUuid).then(response =>
    response.data
      .filter(caseItem => caseItem.attributes.status !== 'resolved')
      .map<CaseChoiceType>(caseItem => ({
        value: caseItem.id,
        label: `${caseItem.attributes.display_id}: ${caseItem.attributes.case_type_name}`,
        subLabel: caseItem.attributes.description,
      }))
  );
};

export interface CaseSelectorPropsType
  extends FormFieldPropsType<unknown, CaseSelectorConfigType> {}

const CaseSelector: React.ComponentType<CaseSelectorPropsType> = ({
  config,
  ...restProps
}) => {
  const [t] = useTranslation('CaseSelector');

  if (!config || !config.contactUuid) {
    console.error(
      `The CaseSelector component cannot be used without config.contactUuid.`
    );
    return null;
  }

  return (
    <DataProvider
      autoProvide={true}
      provider={fetchCasesHandler}
      providerArguments={[config.contactUuid]}
    >
      {({ data, busy }) => {
        const normalizedChoices = data || [];
        return (
          <FlatValueSelect
            {...restProps}
            choices={normalizedChoices}
            isClearable={true}
            loading={busy}
            components={{
              Option: MultilineOption,
            }}
            filterOption={() => true}
            translations={{
              'form:choose': t('choose'),
            }}
          />
        );
      }}
    </DataProvider>
  );
};

export default (props: CaseSelectorPropsType) => (
  <I18nResourceBundle resource={locale} namespace="CaseSelector">
    <CaseSelector {...props} />
  </I18nResourceBundle>
);
