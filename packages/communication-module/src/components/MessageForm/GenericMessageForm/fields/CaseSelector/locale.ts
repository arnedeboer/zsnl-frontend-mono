// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    choose: 'Selecteer een zaak',
  },
};

export default locale;
