// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { withTheme, WithTheme } from '@material-ui/styles';

export interface MessageSenderIconPropsType extends WithTheme<any> {
  type: 'person' | 'organization' | 'employee';
}

const MessageSenderIcon: React.ComponentType<MessageSenderIconPropsType> = ({
  type,
  theme,
}) => (
  <ZsIcon
    color={theme.mintlab.greyscale.darkest}
    backgroundColor={theme.mintlab.greyscale.dark}
    size="small"
  >
    {`entityType.inverted.${type}`}
  </ZsIcon>
);

export default withTheme(MessageSenderIcon);
