// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useMessageContentStyle = makeStyles(
  ({ mintlab: { greyscale }, breakpoints }: any) => ({
    content: {
      color: greyscale.offblack,
      marginTop: 20,
      marginLeft: 0,
      whiteSpace: 'pre-line',
      [breakpoints.up('sm')]: {
        marginLeft: 50,
      },
    },
    body: {
      lineHeight: 1.9,
    },
    background: {
      backgroundColor: greyscale.dark,
      borderRadius: 8,
      padding: 20,
    },
  })
);
