// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { CommunicationRootStateType } from '../../../store/communication.reducer';
import { addAttachmentToCaseAction } from '../../../store/thread/communication.thread.actions';
import { threadNotRelatedOrRelatedCaseNotResolvedSelector } from '../../../store/selectors/threadNotRelatedOrRelatedCaseNotResolvedSelector';
import { AttachmentPropsType } from './Attachment/Attachment';
import AttachmentList, { AttachmentListPropsType } from './AttachmentList';

type PropsFromStateType = Pick<AttachmentPropsType, 'canBeAddedToCase'> & {
  canOpenPDFPreview: boolean;
};

const mapStateToProps = (
  state: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      context: {
        capabilities: { canAddAttachmentToCase, canOpenPDFPreview },
      },
    },
  } = state;

  return {
    canBeAddedToCase:
      canAddAttachmentToCase &&
      threadNotRelatedOrRelatedCaseNotResolvedSelector(state),
    canOpenPDFPreview,
  };
};

type PropsFromDispatchType = Pick<AttachmentListPropsType, 'addToCase'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  addToCase(payload: string) {
    const action = addAttachmentToCaseAction(payload);

    dispatch(action as any);
  },
});

const AttachmentListContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(AttachmentList);

export default AttachmentListContainer;
