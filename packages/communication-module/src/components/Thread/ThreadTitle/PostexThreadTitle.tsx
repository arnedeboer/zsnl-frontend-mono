// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { ExternalMessageType } from '../../../types/Message.types';
import ThreadTitle, { ThreadTitlePropsType } from './ThreadTitle';

export type PostexThreadTitlePropsType = {
  message: ExternalMessageType;
  showAddThreadToCaseDialog: () => void;
  canAddThreadToCase: boolean;
} & Pick<ThreadTitlePropsType, 'context' | 'showLinkToCase'>;

const PostexThreadTitle: React.FunctionComponent<
  PostexThreadTitlePropsType
> = ({ message, showAddThreadToCaseDialog, canAddThreadToCase, ...rest }) => {
  const [t] = useTranslation('communication');
  const addThreadToCaseAction = canAddThreadToCase
    ? [
        {
          action: showAddThreadToCaseDialog,
          label: t('threads.actions.addThreadToCase'),
        },
      ]
    : [];
  const actionButtons = [...addThreadToCaseAction];

  return (
    <ThreadTitle
      {...rest}
      title={message.subject}
      actionButtons={actionButtons}
    />
  );
};

export default PostexThreadTitle;
