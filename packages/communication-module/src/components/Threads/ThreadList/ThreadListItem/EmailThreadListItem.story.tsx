// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { boolean, number, text } from '@mintlab/ui/App/story';
import { stories } from '../../../../story';
import EmailThreadListItem from './EmailThreadListItem';

const Wrapper: React.ComponentType = ({ children }) => (
  <div style={{ maxWidth: 400 }}>{children}</div>
);

const getProps = () => ({
  createdByName: 'Foo bar',
  date: new Date(),
  id: '1',
  isUnread: boolean('Unread', false),
  messageCount: number('Number of messages', 2),
  selected: boolean('Selected', false),
  showLinkToCase: false,
  subject: text('Subject', 'Lorum ipsum dolar sit amet'),
  type: 'email',
  summary: '',
  hasAttachment: boolean('Has attachment', false),
  style: {},
  rootPath: '',
  numberOfMessages: number('Number of messages', 2),
});

stories(module, `${__dirname}/EmailThreadListItem`, {
  Default() {
    return (
      <Wrapper>
        <EmailThreadListItem {...getProps()} />
      </Wrapper>
    );
  },

  Selected() {
    const selectedProps = {
      ...getProps(),
      selected: true,
    };
    return (
      <Wrapper>
        <EmailThreadListItem {...selectedProps} />
      </Wrapper>
    );
  },

  Unread() {
    const unreadProps = {
      ...getProps(),
      isUnread: true,
    };
    return (
      <Wrapper>
        <EmailThreadListItem {...unreadProps} />
      </Wrapper>
    );
  },
});
