// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import AutoSizer from 'react-virtualized/dist/es/AutoSizer';
//@ts-ignore
import List from 'react-virtualized/dist/es/List';
import { ThreadType } from '../../../types/Thread.types';
import { CommunicationContextContextType } from '../../../types/Context.types';
import NoteThreadListItem from './ThreadListItem/NoteThreadListItem';
import ContactMomentThreadListItem from './ThreadListItem/ContactMomentThreadListItem';
import PipMessageThreadListItem from './ThreadListItem/PipMessageThreadListItem';
import EmailThreadListItem from './ThreadListItem/EmailThreadListItem';
import PostexThreadListItem from './ThreadListItem/PostexThreadListItem';

interface RowRendererType {
  key: number;
  index: number;
  style: any;
}

interface RenderListType {
  width: number;
  height: number;
}

export interface ThreadListPropsType {
  threads: ThreadType[];
  selectedThreadId?: string;
  rootPath: string;
  showLinkToCase: boolean;
  context: CommunicationContextContextType;
}

/* eslint complexity: [2, 9] */
const ThreadList: React.ComponentType<ThreadListPropsType> = ({
  threads,
  selectedThreadId,
  rootPath,
  showLinkToCase,
  context,
}) => {
  const rowRenderer = ({ index, style }: RowRendererType) => {
    const data = threads[index];
    const lastMessage = data.lastMessage;
    const { id } = data;
    const selected = selectedThreadId === id;
    const props = {
      selected,
      rootPath,
      style,
      showLinkToCase,
      caseNumber: data.caseNumber,
      createdByName: lastMessage.createdByName,
      isUnread: context === 'pip' ? data.unreadPip : data.unreadEmployee,
      ...data,
    };

    switch (lastMessage.type) {
      case 'contact_moment':
        return (
          <ContactMomentThreadListItem
            {...props}
            key={id}
            withName={lastMessage.withName}
            channel={lastMessage.channel}
          />
        );

      case 'email':
        return (
          <EmailThreadListItem
            {...props}
            key={id}
            messageCount={data.numberOfMessages}
            subject={lastMessage.subject}
            failureReason={lastMessage.failureReason || ''}
          />
        );

      case 'note':
        return <NoteThreadListItem {...props} key={id} />;

      case 'postex':
        return (
          <PostexThreadListItem
            {...props}
            key={id}
            messageCount={data.numberOfMessages}
            subject={lastMessage.subject}
            failureReason={lastMessage.failureReason || ''}
          />
        );

      case 'pip_message':
        return (
          <PipMessageThreadListItem
            {...props}
            key={id}
            messageCount={data.numberOfMessages}
            subject={lastMessage.subject}
          />
        );
    }
  };

  return (
    //@ts-ignore
    <AutoSizer>
      {({ width, height }: RenderListType) => (
        //@ts-ignore
        <List
          height={height}
          rowCount={threads.length}
          rowHeight={110}
          rowRenderer={rowRenderer}
          width={width}
        />
      )}
    </AutoSizer>
  );
};

export default ThreadList;
