// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { addThreadToCaseAction } from '../../../store/thread/communication.thread.actions';
import AddThreadToCaseDialog from './AddThreadToCaseDialog';

export type AddThreadToCaseDialogPropsType = {
  onSubmit: (caseUuid: string, threadUuid: string) => void;
  [key: string]: any;
};
type PropsFromDispatchType = Pick<AddThreadToCaseDialogPropsType, 'onSubmit'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  onSubmit(caseUuid: string, threadUuid: string) {
    const action = addThreadToCaseAction(caseUuid, threadUuid);

    dispatch(action as any);
  },
});

const AddThreadToCaseDialogContainer = connect<{}, PropsFromDispatchType>(
  null,
  mapDispatchToProps
)(AddThreadToCaseDialog);

export default AddThreadToCaseDialogContainer;
