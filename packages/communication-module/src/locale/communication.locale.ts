// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as CHANNELS from '../library/communicationChannels.constants';

export default {
  nl: {
    forms: {
      add: 'Toevoegen',
      cancel: 'Annuleren',
      new: 'Nieuw',
      send: 'Versturen',
      openPreview: 'Voorbeeld',
      closePreview: 'Voorbeeld sluiten',
    },
    threadTypes: {
      contact_moment: 'Contactmoment',
      note: 'Notitie',
      email: 'E-mail',
      message: 'Bericht',
      messageTemp: 'PIP-bericht',
      pip_message: 'PIP',
      mijn_overheid: 'MijnOverheid',
      postex: 'Postex',
    },
    navigation: {
      back: 'Terug',
    },
    thread: {
      tags: {
        pip_message: 'PIP',
        email: 'E-mail',
        case: 'Zaak',
        postex: 'Postex',
      },
      noContent: 'Er is geen communicatie om weer te geven',
      counterTooltip: 'Bevat {{count}} berichten',
      note: {
        messageTitle: 'Door {{user}}',
        title: 'Notitie van {{createdByName}}',
      },
      contactMoment: {
        messageTitle: '{{sender}} met {{receiver}}',
        title: 'Contactmoment via {{channel}}',
        subTitle: '{{createdByName}} met {{withName}}',
      },
      pipMessage: {
        title: 'Van: {{createdByName}}',
        subTitle: '(behandelaar)',
      },
      email: {
        title: 'Van: {{createdByName}}',
        subTitle: '(behandelaar)',
      },
      postex: {
        title: 'Van: {{createdByName}}',
        subTitle: '(behandelaar)',
      },
      dateFormat: 'D MMM',
      dateFormatLong: 'D MMMM YYYY - H:mm',
      searchPlaceholder: 'Typ hier uw zoekterm',
      searchLabel: 'Zoek berichten',
      searchClose: 'Sluiten',
      linkToCase: 'Ga naar zaak {{caseNumber}}',
      messageActions: {
        addSourceFileToCaseOriginal: 'Opslaan in documenten (bronbestand)',
        addSourceFileToCasePDF: 'Opslaan in documenten (PDF)',
        markAsUnread: 'Markeren als ongelezen',
        delete: 'Verwijderen',
      },
      deleteMessage: {
        title: 'Bericht verwijderen?',
        body: 'Weet u zeker dat u dit bericht wilt verwijderen? Deze actie kan niet ongedaan gemaakt worden.',
      },
      emailMessageHeader: {
        recipient: 'Aan',
        cc: 'CC',
        bcc: 'BCC',
      },
      postexMessageHeader: {
        recipient: 'Aan',
      },
    },
    placeholder: 'Selecteer een item om te lezen',
    direction: {
      incoming: 'Inkomend',
      outgoing: 'Uitgaand',
    },
    channels: {
      [CHANNELS.TYPE_CHANNEL_ASSIGNEE]: 'Behandelaar',
      [CHANNELS.TYPE_CHANNEL_FRONTDESK]: 'Balie',
      [CHANNELS.TYPE_CHANNEL_PHONE]: 'Telefoon',
      [CHANNELS.TYPE_CHANNEL_MAIL]: 'Post',
      [CHANNELS.TYPE_CHANNEL_EMAIL]: 'E-mail',
      [CHANNELS.TYPE_CHANNEL_WEBFORM]: 'Webformulier',
      [CHANNELS.TYPE_CHANNEL_SOCIALMEDIA]: 'Sociale media',
      [CHANNELS.TYPE_CHANNEL_EXTERNAL]: 'Externe applicatie',
      [CHANNELS.TYPE_CHANNEL_POSTEX]: 'Postex',
    },
    replyForm: {
      openButtonLabel: 'Bericht beantwoorden',
      placeholder: 'Reageren is niet mogelijk.',
    },
    addFields: {
      contact: 'Zoek een contact...',
      case: 'Zoek een zaak...',
      selectCase: 'Zaak',
      channel: 'Via',
      direction: 'Richting',
      content: 'Samenvatting contactmoment',
      htmlEmailTemplateDescription:
        'Maakt gebruik van: {{htmlEmailTemplateName}}',
      message: 'Bericht',
      messagePlaceholder: 'Typ hier uw bericht',
      note: 'Notitie',
      subject: 'Onderwerp',
      subjectPlaceholder: 'Typ hier uw onderwerp',
      addReply: 'Nieuw bericht',
      recipientTypes: {
        requestor: 'Aanvrager',
        colleague: 'Collega',
        role: 'Betrokkene',
        authorized: 'Gemachtigde',
        other: 'Overig',
      },
      recipientType: 'Type ontvanger',
      colleague: 'Zoek een collega…',
      rolePlaceholder: 'Selecteer een betrokkene…',
      attachments: 'Bestanden',
      attachmentsPlaceholder: 'Selecteer zaakdocumenten…',
      authorizedPlaceholder: 'Selecteer een gemachtigde…',
      caseRequestorError:
        'Het e-mailadres van de aanvrager is niet beschikbaar',
    },
    attachments: {
      actions: {
        download: 'Download',
        addToCaseDocuments: 'Opslaan in documenten',
      },
    },
    threads: {
      actions: {
        addThreadToCase: 'Toevoegen aan zaak',
        import: 'Bericht importeren',
      },
    },
    dialogs: {
      addThreadToCase: {
        title: 'Voeg aan zaak toe',
        select: 'Selecteer uw zaak',
      },
      importMessage: {
        title: 'Bericht importeren',
        select: {
          hint: 'Ondersteunde bestandstypen zijn: .msg en .eml',
        },
      },
    },
    serverErrors: {
      'communication/thread/not_created':
        'Er is iet mis gegaan bij het aanmaken.',
      'communication/thread/cache/not_created':
        '$t(serverErrors.communication/thread/not_created)',
      'communication/thread/not_allowed':
        'U bent niet gemachtigd om deze conversatie te bekijken.',
      'communication/thread/not_found':
        'De conversatie kon niet worden gevonden.',
      'communication/contact/not_found': 'Onbekende verzender/ontvanger.',
      'communication/case/not_found': 'Onbekende zaak.',
      'communication/case/not_allowed':
        'U bent niet gemachtigd deze zaak op te vragen.',
      'communication/message/type/not_created':
        '$t(serverErrors.communication/thread/not_created)',
      'communication/message/not_created':
        'Het bericht kon niet worden toegevoegd.',
      'document/filestore/not_found': 'Het bestand kon niet worden gevonden.',
      'document/filestore/not_allowed':
        'Het bestand is al aan de zaak gekoppeld.',
      'communication/case/case_resolved':
        'Kan actie niet uitvoeren. Zaak is gesloten.',
      'communication/message/no_permission_for_delete':
        'Kan actie niet uitvoeren. U heeft onvoldoende rechten.',
    },
    snackMessages: {
      attachmentAddedToCase: 'De bijlage is opgeslagen in documenten',
      sourceFileAddedToCase: 'Het bericht is opgeslagen in documenten',
      threadAddedToCase: 'Succesvol toegevoegd aan zaak',
      messageImported: 'Het bericht is succesvol geïmporteerd',
    },
  },
};
