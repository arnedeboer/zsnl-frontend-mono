// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const TYPE_CHANNEL_FRONTDESK = 'frontdesk';
export const TYPE_CHANNEL_ASSIGNEE = 'assignee';
export const TYPE_CHANNEL_MAIL = 'mail';
export const TYPE_CHANNEL_PHONE = 'phone';
export const TYPE_CHANNEL_EMAIL = 'email';
export const TYPE_CHANNEL_WEBFORM = 'webform';
export const TYPE_CHANNEL_SOCIALMEDIA = 'social_media';
export const TYPE_CHANNEL_EXTERNAL = 'external';
export const TYPE_CHANNEL_POSTEX = 'postex';
