// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, text, StorybookMessage } from '../../story';
import Clone from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Clone message={text('Greeting', 'Read the README!')}>
        <StorybookMessage />
      </Clone>
    );
  },
});
