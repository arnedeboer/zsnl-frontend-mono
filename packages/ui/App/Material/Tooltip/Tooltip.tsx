// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import MuiTooltip, { TooltipProps } from '@material-ui/core/Tooltip';
import { useTooltipStyles } from './Tooltip.style';

type TooltipPropsType = TooltipProps & {
  type?: 'default' | 'info' | 'error';
  disabled?: boolean;
  classes?: {
    popper?: string;
    all?: string;
    default?: string;
    wrapper?: string;
  };
  title: string | React.ReactElement;
  children: any;
};

export const Tooltip: React.ComponentType<TooltipPropsType> = ({
  children,
  classes = {},
  title,
  placement = 'top',
  type = 'default',
  disabled = false,
  ...rest
}) => {
  const tooltipClasses = { ...useTooltipStyles(), ...classes };

  return (
    <MuiTooltip
      title={disabled ? '' : title}
      placement={placement}
      classes={{
        tooltip: classNames(tooltipClasses.all, tooltipClasses[type]),
        popper: tooltipClasses.popper,
      }}
      {...rest}
      disableFocusListener={disabled}
      disableHoverListener={disabled}
      disableTouchListener={disabled}
    >
      <div className={tooltipClasses.wrapper}>{children}</div>
    </MuiTooltip>
  );
};

export default Tooltip;
