// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Tooltip } from './Tooltip';
export * from './Tooltip';
export default Tooltip;
