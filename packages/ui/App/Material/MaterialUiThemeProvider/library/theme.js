// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createMuiTheme } from '@material-ui/core/styles';

const { assign } = Object;

/**
 * Mintlab theme greyscale.
 *
 * @type {Object}
 */
export const greyscale = {
  black: '#2A2E33',
  offBlack: '#2F3338',
  darkest: '#4E5764',
  evenDarker: '#AEB3B9',
  darker: '#D7D7DB',
  dark: '#F2F2F2',
  main: '#878787',
  light: '#F7F7F8',
  lighter: '#FFFFFF',
};

/**
 * Mintlab theme color palette.
 *
 * Note that `review` and `support` are custom palette properties,
 * and `darker` and `lighter` are custom PaletteIntention properties.
 * They are provided with the palette for convenience, but have no
 * support in the Material-UI theme API.
 *
 * @see https://material-ui.com/customization/themes/#palette
 * @type {Object}
 */
export const palette = {
  primary: {
    darkest: '#161559',
    darker: '#134991',
    dark: '#1860C3',
    main: '#1B73E7',
    light: '#97C1EF',
    lighter: '#E1EDFF',
    lightest: '#EFF5FF',
  },
  secondary: {
    darkest: '#14441F',
    darker: '#1D652E',
    dark: '#26883E',
    main: '#30AA4D',
    light: '#83CC94',
    lighter: '#ACDCB8',
    lightest: '#D6EEDB',
  },
  elephant: {
    darkest: '#1C1C1C',
    darker: '#383838',
    dark: '#6B6B6B',
    main: '#9B9B9B',
    light: '#C6C6C6',
    lighter: '#DDDDDD',
    lightest: '#E8E8E8',
  },
  thundercloud: {
    darkest: '#0F0F29',
    darker: '#4E5763',
    dark: '#606979',
    main: '#8E95A0',
    light: '#ACB4BE',
    lighter: '#C5CAD1',
    lightest: '#DFDFE5',
  },
  cloud: {
    darkest: '#E4E4EA',
    darker: '#E8E8ED',
    dark: '#EFEFEF',
    main: '#F1F3F4',
    light: '#F5F7F7',
    lighter: '#F7F8F9',
    lightest: '#FFFFFF',
  },
  northsea: {
    darkest: '#0F0F29',
    darker: '#5A6477',
    dark: '#67718A',
    main: '#6A768B',
    light: '#8E95A0',
    lighter: '#C4C7CD',
    lightest: '#E0E2E5',
  },
  basalt: {
    darkest: '#1B1C1E',
    darker: '#252C31',
    dark: '#3B424B',
    main: '#4E5B68',
    light: '#637082',
    lighter: '#707D8E',
    lightest: '#8C949F',
  },
  coral: {
    darkest: '#661816',
    darker: '#992522',
    dark: '#CC312D',
    main: '#FF3D38',
    light: '#FF8B88',
    lighter: '#FFB1AF',
    lightest: '#FFD8D7',
  },
  pansy: {
    darkest: '#29174A',
    darker: '#3E226E',
    dark: '#522D94',
    main: '#6739B9',
    light: '#A488D5',
    lighter: '#C2B0E3',
    lightest: '#E1D7F1',
  },
  bluebird: {
    darkest: '#003662',
    darker: '#005292',
    dark: '#006DC3',
    main: '#0088F4',
    light: '#66B8F8',
    lighter: '#99CEFB',
    lightest: '#CCE7FD',
  },
  jade: {
    darkest: '#003B37',
    darker: '#005B52',
    dark: '#00796D',
    main: '#009789',
    light: '#66C1B8',
    lighter: '#99D5D0',
    lightest: '#CBEAE7',
  },
  lizard: {
    darkest: '#1B4F3A',
    darker: '#287756',
    dark: '#369E73',
    main: '#43C690',
    light: '#8EDDBC',
    lighter: '#B4E8D3',
    lightest: '#D9F4E9',
  },
  grass: {
    darkest: '#1D461F',
    darker: '#2D692E',
    dark: '#3A8D3D',
    main: '#48B04C',
    light: '#92D094',
    lighter: '#B6DFB7',
    lightest: '#DBEFDB',
  },
  sahara: {
    darkest: '#654B00',
    darker: '#977001',
    dark: '#CA9600',
    main: '#FCBB00',
    light: '#FDD666',
    lighter: '#FDE499',
    lightest: '#FEF1CC',
  },
  error: {
    dark: '#C62443',
    main: '#FF345B',
    light: '#FBE9E7',
  },
  review: {
    dark: '#D18100',
    main: '#FF9D00',
    light: '#FFF5D9',
    support: '#FFE3DB',
  },
  danger: {
    dark: '#C62443',
    main: '#FF345B',
    light: '#FBE9E7',
    support: '#FFE5E2',
  },
  support: {
    main: '#004EE3',
  },
  common: {
    white: greyscale.lighter,
    black: greyscale.black,
  },
  vivid: {
    1: '#0058FF',
    2: '#F4B949',
    3: '#0AD7E8',
    4: '#088DFF',
    5: '#6066FF',
    6: '#872FFF',
  },
};

/**
 * Mintlab theme radii.
 *
 * @type {Object}
 */
export const radius = {
  button: 8,
  buttonSmall: 5,
  card: 2,
  horizontalMenuButton: 30,
  notificationButton: 30,
  verticalMenuButton: 30,
  bannerButton: 30,
  dialog: 10,
  genericInput: 30,
  sheet: 8,
  snackbar: 30,
  dropdownMenu: 15,
  dropdownButton: 8,
  chip: 12,
  chipRemove: '50%',
  wysiwyg: 8,
  textField: 8,
  tooltip: 8,
  tableRow: 8,
  select: 8,
  defaultFormElement: 8,
};

/**
 * Mintlab theme icon sizes.
 *
 * @type {Array}
 */
export const iconSizes = [
  'tiny',
  'extraSmall',
  'small',
  'medium',
  'large',
  'huge',
];

/**
 * Mintlab theme icon sizes.
 * 28 is base. Every iteration is 1.25 bigger or smaller
 *
 * @type {Object}
 */
export const icon = {
  tiny: 14,
  extraSmall: 18,
  small: 23,
  medium: 28,
  large: 35,
  huge: 44,
};

/**
 * Mintlab theme shadows.
 *
 * @type {Object}
 */
export const shadows = {
  insetTop: 'inset 0.1px 1px 3px 0.1px rgba(0,0,0,0.10)',
  flat: '0.1px 1px 3px 0.1px rgba(0,0,0,0.12)',
  medium: '3px 3px 10px 2px rgba(0,0,0,0.05)',
  sharp: '0 1px 2px rgba(0,0,0,0.3)',
};

export const ICON_SELECTOR = '& > span:first-child > svg';

/**
 * Mintlab theme typography.
 *
 * @type {Object}
 */
const hBase = {
  fontWeight: 400,
  color: greyscale.black,
};

// inline override for third party configuration
/* eslint id-length: [error, { min: 3, exceptions: [h1, h2, h3, h4, h5, h6] }] */
export const typography = {
  useNextVariants: true,
  h1: assign({ fontSize: '2.187rem' }, hBase),
  h2: assign({ fontSize: '1.875rem' }, hBase),
  h3: assign({ fontSize: '1.5625rem' }, hBase),
  h4: assign({ fontSize: '1.375rem' }, hBase),
  h5: assign({ fontSize: '1.25rem' }, hBase),
  h6: assign({ fontSize: '1.125rem' }, hBase),
};

/**
 * Global overrides for Material-UI components.
 * This is preferable over using `withStyles` in the component facade.
 *
 * @type {Object}
 */
export const overrides = {
  MuiButton: {
    root: {
      borderRadius: radius.button,
      textTransform: 'none',
    },
    label: {
      // ZS-TODO: vertical alignment compensation; better ideas?
      paddingTop: '0.1rem',
      [ICON_SELECTOR]: {
        marginRight: '0.4rem',
      },
    },
    containedSecondary: {
      color: '#fff',
    },
  },
  MuiDialog: {
    paper: {
      borderRadius: radius.dialog,
    },
  },
  MuiDialogActions: {
    root: {
      '& > button': {
        marginRight: '12px',
      },
    },
  },
  MuiPaper: {
    root: {
      padding: '8px',
    },
  },
  MuiDivider: {
    root: {
      margin: '6px 0px',
    },
  },
};

/**
 * Material-UI theme.
 *
 * @type {Object}
 */
export const theme = createMuiTheme({
  overrides,
  palette,
  typography,
  zIndex: {
    inputLabel: 100,
    statusBar: 1250,
    options: 1260,
  },
  mintlab: {
    greyscale,
    icon,
    iconSizes,
    radius,
    shadows,
  },
});
