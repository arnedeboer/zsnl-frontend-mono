// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, action } from '../../story';
import Icon from '../Icon/Icon';
import Breadcrumbs from './Breadcrumbs';

const items = [
  {
    label: 'Home',
    path: '/',
  },
  {
    label: 'Level 1',
    path: '/level',
  },
  {
    label: 'Level 2',
    path: '/level/level',
  },
  {
    label: 'Level 3',
    path: '/level/level/level',
  },
  {
    label: 'Level 4',
    path: '/level/level/level/level',
  },
];

const ZERO = 0;

stories(module, __dirname, {
  Default() {
    return (
      <Breadcrumbs
        items={items}
        onItemClick={event => {
          event.preventDefault();
          action('breadcrumb-click')(event);
        }}
        scope="story"
      />
    );
  },
  Shortened() {
    return (
      <Breadcrumbs
        maxItems={4}
        items={items}
        onItemClick={event => {
          event.preventDefault();
          action('breadcrumb-click')(event);
        }}
      />
    );
  },
  CustomRenderer() {
    const itemRenderer = ({ item, onItemClick, classes, index }) => {
      const icon = index === ZERO ? 'home' : 'folder';

      return (
        <a href={item.path} onClick={onItemClick} className={classes.link}>
          <span
            style={{
              verticalAlign: 'middle',
              display: 'inline-block',
              paddingRight: '10px',
            }}
          >
            <Icon size="medium">{icon}</Icon>
          </span>
          <span>{item.label}</span>
        </a>
      );
    };

    const lastItemRenderer = ({ item, classes }) => (
      <span className={classes.last}>
        <span
          style={{
            verticalAlign: 'middle',
            display: 'inline-block',
            paddingRight: '10px',
          }}
        >
          <Icon size="medium">folder</Icon>
        </span>
        <span>{item.label}</span>
      </span>
    );

    return (
      <Breadcrumbs
        items={items}
        onItemClick={event => {
          event.preventDefault();
          action('breadcrumb-click')(event);
        }}
        itemRenderer={itemRenderer}
        lastItemRenderer={lastItemRenderer}
      />
    );
  },
});
