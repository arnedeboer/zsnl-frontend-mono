// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=Overline
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const Overline = ({ children, classes }) => (
  <Typography variant="overline" classes={classes}>
    {children}
  </Typography>
);
