// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=H5
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} [props.classes]
 * @return {ReactElement}
 */
export const H5 = ({ children, classes }) => (
  <Typography variant="h5" classes={classes}>
    {children}
  </Typography>
);
