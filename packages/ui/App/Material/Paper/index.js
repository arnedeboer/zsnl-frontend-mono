// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Paper } from './Paper';
export * from './Paper';
export default Paper;
