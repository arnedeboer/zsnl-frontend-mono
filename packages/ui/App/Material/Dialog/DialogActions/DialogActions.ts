// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { withStyles } from '@material-ui/styles';
import DialogActions from '@material-ui/core/DialogActions';

const dialogActionsStyleSheet = () => ({
  root: {
    margin: 0,
    padding: '16px 8px',
  },
});

export default withStyles(dialogActionsStyleSheet)(DialogActions);
