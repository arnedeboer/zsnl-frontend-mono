// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/styles';

export const useDialogStyles = makeStyles(() => ({
  paper: {
    padding: 0,
  },
}));
