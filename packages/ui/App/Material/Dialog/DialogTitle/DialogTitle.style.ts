// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

const defaultRoot = {
  display: 'flex',
  alignItems: 'center',
  paddingTop: 0,
  paddingBottom: 0,
  height: 70,
  zIndex: 1,
};

export const useDialogTitleStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    rootNormal: defaultRoot,
    rootElevated: {
      ...defaultRoot,
      boxShadow: '0px 0px 5px 0px rgba(0,0,0,0.3)',
      '& + *': {
        paddingTop: 16,
      },
      '&>*:nth-child(1)': {
        color: greyscale.darkest,
      },
    },
    typography: {
      flex: '1 0 auto',
    },
    icon: {
      paddingRight: 8,
    },
    closeButton: {
      paddingLeft: 8,
    },
  })
);
