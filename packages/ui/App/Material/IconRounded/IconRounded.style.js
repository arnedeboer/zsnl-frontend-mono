// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Style Sheet with theme access for the {@link IconRounded} component.
 * See the CSS file for the autoprefixed CSS grid.
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const iconRoundedStyleSheet = () => ({
  icon: {
    margin: 'auto',
  },
  wrapper: {},
});
