// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { useTheme } from '@material-ui/styles';
import { Theme } from '@mintlab/ui/types/Theme';
import { SvgIconProps } from '@material-ui/core/SvgIcon/SvgIcon';
import * as iconMap from './library';

export type IconNameType = keyof typeof iconMap;
export type IconSizeType = keyof Theme['mintlab']['icon'] | number;

export const iconNames = Object.keys(iconMap).reduce(
  (acc, key) => ({ [key]: key, ...acc }),
  {}
) as Record<IconNameType, IconNameType>;

export const Icon: React.ComponentType<{
  children: IconNameType;
  color?: SvgIconProps['color'];
  size?: IconSizeType;
  classes?: Record<string, string>;
  style?: any;
}> = ({
  children,
  classes = {},
  color = 'inherit',
  size = 'medium',
  style,
}) => {
  const {
    mintlab: { icon },
  } = useTheme<Theme>();
  const IconComponent = iconMap[children as IconNameType];
  const iconSize = typeof size === 'string' ? icon[size] : size;

  return (
    <span
      className={classes.wrapper}
      style={{
        display: 'inline-flex',
        fontSize: `${iconSize}px`,
        height: `${iconSize}px`,
        ...style,
      }}
    >
      <IconComponent
        classes={cloneWithout(classes, 'wrapper')}
        color={color as any}
        fontSize="inherit"
      />
    </span>
  );
};

export default Icon;
