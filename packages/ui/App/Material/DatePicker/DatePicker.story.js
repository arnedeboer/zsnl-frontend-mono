// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { stories, select, text } from '../../story';
import { LoadableDatePicker, LoadableKeyboardDatePicker } from '.';

stories(module, __dirname, {
  Default() {
    const [value, setValue] = useState(null);
    const handleChange = event => setValue(event.target.value);
    const handleClose = () => setValue(null);

    return (
      <div style={{ width: '300px', backgroundColor: '#f5f5f5' }}>
        <LoadableDatePicker
          name="story"
          value={value}
          onChange={handleChange}
          onClose={handleClose}
          variant={select('Type', ['dialog', 'inline', 'static'], 'inline')}
          placeholder={text('Placeholder', 'Datum toevoegen')}
          fullWidth
        />
      </div>
    );
  },
  Keyboard() {
    const [value, setValue] = useState(null);
    const handleChange = event => setValue(event.target.value);
    const handleClose = () => setValue(null);

    return (
      <div style={{ width: '300px', backgroundColor: '#f5f5f5' }}>
        <LoadableKeyboardDatePicker
          name="story"
          value={value}
          onChange={handleChange}
          onClose={handleClose}
          placeholder="DD-MM-JJJJ"
          format="dd-MM-yyyy"
          fullWidth
        />
      </div>
    );
  },
});
