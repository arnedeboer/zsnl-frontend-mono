// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  React,
  stories,
  action,
  event,
  boolean,
  select,
  text,
} from '../../story';
import * as icons from '../Icon/library';
import Button from '.';

const { keys } = Object;

const factory = {
  color(defaultValue = 'default') {
    return select(
      'Color',
      ['default', 'primary', 'secondary', 'review', 'danger'],
      defaultValue
    );
  },
  icon() {
    return select('Icon', ['', ...keys(icons)], '');
  },
  iconSize(defaultValue = 'medium', name) {
    return select(
      name,
      ['extraSmall', 'small', 'medium', 'large'],
      defaultValue
    );
  },
  size(defaultValue = 'medium') {
    return select('Size', ['small', 'medium', 'large'], defaultValue);
  },
  variant(defaultValue = 'text') {
    return select(
      'Variant',
      ['text', 'outlined', 'semiContained', 'contained'],
      defaultValue
    );
  },
};

stories(module, __dirname, {
  All() {
    return (
      <Button
        action={action('Click')}
        disabled={boolean('Disabled', false)}
        icon={factory.icon()}
        iconSize={factory.iconSize('medium', 'Icon Size')}
        presets={[factory.color(), factory.size(), factory.variant()]}
        scope="story"
      >
        {text('Greeting', 'Hello, world!')}
      </Button>
    );
  },
  Link() {
    return (
      <Button
        action={text('URL', 'https://mintlab.nl')}
        disabled={boolean('Disabled', false)}
        presets={[
          factory.color('primary'),
          factory.size(),
          factory.variant('contained'),
        ]}
      >
        {text('Greeting', 'Hello, world!')}
      </Button>
    );
  },
  Icon() {
    return (
      <Button
        action={event(Button)}
        disabled={boolean('Disabled', false)}
        label="Get out of your lazy bed!"
        presets={[
          factory.color('primary'),
          factory.iconSize('medium', 'Size'),
          'icon',
        ]}
      >
        alarm
      </Button>
    );
  },
});
