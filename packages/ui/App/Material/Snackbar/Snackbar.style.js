// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Style Sheet for the Snackbar component
 * @return {JSS}
 */
export const snackbarStyleSheet = ({ mintlab: { greyscale, radius } }) => ({
  root: {
    padding: '0 16px 0 24px',
    borderRadius: radius.snackbar,
    minHeight: '50px',
    display: 'flex',
  },
  message: {
    flex: 1,
  },
  iconButton: {
    width: '36px',
    height: '36px',
    padding: 0,
    '&:hover': {
      backgroundColor: `${greyscale.lighter}33`,
    },
  },
});
