// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Switch } from './Switch';
export * from './Switch';
export default Switch;
