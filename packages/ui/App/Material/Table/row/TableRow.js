// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import MuiTableRow from '@material-ui/core/TableRow';
import { withStyles } from '@material-ui/styles';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import TableCell from '../cell/TableCell';
import Checkbox from '../../Checkbox';
import Render from '../../../Abstract/Render';
import { tableStyleSheet } from '../Table.style';
import { addScopeProp } from '../../../library/addScope';

const noop = () => {};

/**
 * @param {Object} props
 *
 * @return {ReactElement}
 */
const createCheckboxCell = ({
  classes,
  disabled,
  heading,
  checkboxes,
  selected,
  onCheckboxClick = noop,
  scope,
}) => {
  const clickProps = () => {
    if (!disabled) {
      return { onClick: onCheckboxClick };
    }
  };

  return (
    <Render condition={checkboxes}>
      <TableCell
        key="checkbox-cell"
        className={classNames(
          classes.checkboxCell,
          heading && classes.tableHeadCell
        )}
      >
        <Render condition={!heading}>
          <Checkbox
            disabled={disabled}
            checked={selected}
            formControlClasses={{
              root: classes.checkboxFormControlLabel,
            }}
            {...addScopeProp(scope)}
            {...clickProps()}
          />
        </Render>
      </TableCell>
    </Render>
  );
};

/**
 * @param {Object} props
 * @return {Object} Props without Component props
 */
const restPropsWithout = props =>
  cloneWithout(
    props,
    'children',
    'selectable',
    'checkboxes',
    'selected',
    'heading',
    'classes',
    'onCheckboxClick',
    'className',
    'scope',
    'onClick',
    'onDoubleClick'
  );

/**
 * *Material Design* **TableRow**.
 * - facade for *Material-UI* `TableRow`
 * - all props except `children`, `selectable`, `checkboxes`,
 * `heading`, `classes`, `onCheckboxClick`,  are spread to that component
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Table
 * @see /npm-mintlab-ui/documentation/consumer/manual/Table.html
 * @see https://material-ui.com/api/snackbar/
 *
 * @reactProps {React.children} children
 * @reactProps {boolean} pointer
 * @reactProps {boolean} checkboxes
 * @reactProps {selected} selected
 * @reactProps {boolean} heading
 * @reactProps {Object} classes
 * @reactProps {Function} onCheckboxClick
 * @reactProps {string} className
 *
 * @param {Object} props
 *
 * @return {ReactElement}
 */
const TableRow = props => {
  const {
    children,
    disabled,
    pointer,
    selected,
    heading,
    classes,
    className,
    ...restProps
  } = props;

  const clickProps = () => {
    if (!disabled) {
      const { onClick, onDoubleClick } = restProps;
      return { onClick, onDoubleClick };
    }
  };

  return (
    <MuiTableRow
      className={classNames({
        [classes.tableRow]: !heading,
        [className]: className,
        [classes.pointer]: pointer,
        [classes.disabled]: disabled,
      })}
      selected={selected}
      classes={{
        selected: classes.selectedRow,
      }}
      {...restPropsWithout(restProps)}
      {...clickProps()}
    >
      <React.Fragment>
        {createCheckboxCell(props)}
        {children}
      </React.Fragment>
    </MuiTableRow>
  );
};

export default withStyles(tableStyleSheet)(TableRow);
