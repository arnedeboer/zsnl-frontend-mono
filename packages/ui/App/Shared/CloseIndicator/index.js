// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as CloseIndicator } from './CloseIndicator';
export * from './CloseIndicator';
export default CloseIndicator;
