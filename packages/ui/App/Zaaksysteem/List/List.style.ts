// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

const mainPadding = 15;
const mainRadius = 4;

export const useListStyle = makeStyles(
  ({ palette, mintlab: { greyscale, shadows }, typography }: Theme) => ({
    listContainer: {
      padding: 15,
      backgroundColor: greyscale.dark,
      borderRadius: 4,
    },
    itemContainer: {
      boxShadow: shadows.flat,
      padding: mainPadding,
      borderRadius: mainRadius,
      marginBottom: mainPadding,
      backgroundColor: 'white',
    },
    itemContainerHover: {
      '&:hover': {
        backgroundColor: greyscale.darker,
      },
    },
    itemContainerDragging: {
      backgroundColor: palette.sahara.lighter,
      transition: 'all 0.03s',
    },
    itemContainerSimple: {
      display: 'flex',
      justifyContent: 'space-between',
      '&>button': {
        padding: 0,
      },
      ...typography.body2,
    },
    removeButton: { flex: 0 },
  })
);
