// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, text, boolean } from '../../story';
import Icon from '../../Material/Icon';
import MultilineOption from './Option/MultilineOption';
import Select from '.';

const DELAY = 2500;

const defaultChoices = [
  {
    value: 'chocolate',
    label: 'Chocolate',
    alternativeLabels: ['Yummy', 'I love this flavor'],
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
    alternativeLabels: ['Fruit', 'Red'],
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
  {
    value: 'knackebrod',
    label: 'KNÄCKEBRÖD RÅG',
    alternativeLabels: [
      'hrökkbrauð',
      'knækbrød',
      'knekkebrød',
      'hårt bröd',
      'näkkileipä',
    ],
  },
];

const alternateChoices = [
  {
    value: 'banana',
    label: 'Banana',
  },
  {
    value: 'lemon',
    label: 'Lemon',
  },
  {
    value: 'cherry',
    label: 'Cherry',
  },
];

const choicesCreatable = [
  {
    value: 'chocolate',
    label: 'Chocolate',
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
];

const dropdownOptionsCreatable = [
  {
    value: '1',
    label: 'Regular text',
  },
  {
    value: '2',
    label: 'Súdwest-Fryslân',
  },
  {
    value: '4',
    label: 'KNÄCKEBRÖD RÅG',
  },
];

const choicesMultiline = [
  {
    value: 'chocolate',
    label: 'Chocolate',
    subLabel: 'The yummie brown stuff',
    icon: <Icon size="small">favorite</Icon>,
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
    subLabel: 'The cherry on the top, wait...',
    icon: <Icon size="small">favorite</Icon>,
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
    subLabel:
      'This must be the longest subLabel ever displayed in a React Select option',
    icon: <Icon size="small">favorite</Icon>,
  },
];

const [, defaultValue] = defaultChoices;

const stores = {
  Form: {
    value: defaultValue,
  },
  Generic: {
    value: null,
    choices: [],
  },
  Creatable: {
    value: choicesCreatable,
  },
  MultilineOption: {
    value: defaultValue,
    choices: choicesMultiline,
  },
  MultivalueAsync: {
    value: null,
    choices: [],
    loadedFirst: false,
  },
};

/**
 * @param {SyntheticEvent} event
 * @param {Object} store
 * @return {undefined}
 */
const onChange = ({ value }, store) => store.set({ value });

stories(
  module,
  __dirname,
  {
    Form({ store, value }) {
      return (
        <div
          style={{
            width: '15rem',
            backgroundColor: '#F7F7F8',
          }}
        >
          <Select
            choices={defaultChoices}
            error={text('Error', '')}
            isMulti={boolean('Multiple choices', false)}
            value={value}
            onChange={event => onChange(event, store)}
            name="story"
            id="story"
            isClearable={boolean('Clearable', true)}
            loading={boolean('Loading', false)}
            scope="story"
            multiValueLabelIcon={() =>
              boolean('Multiple choices Label', false) ? (
                <Icon size="small">favorite</Icon>
              ) : null
            }
          />
        </div>
      );
    },
    Generic({ store, choices, value }) {
      const startAdornment = boolean('Start adornment', true) ? (
        <Icon size="small">people</Icon>
      ) : null;

      return (
        <div
          style={{
            width: '15rem',
          }}
        >
          <Select
            generic={true}
            choices={choices}
            loading={!choices.length}
            autoLoad={true}
            getChoices={() => {
              setTimeout(() => {
                store.set({
                  choices: defaultChoices,
                });
              }, DELAY);
            }}
            value={value}
            onChange={event => onChange(event, store)}
            name="story"
            startAdornment={startAdornment}
            isClearable={boolean('Clearable', true)}
          />
        </div>
      );
    },
    Creatable({ store, value }) {
      return (
        <div
          style={{
            width: '30rem',
          }}
        >
          <Select
            creatable={true}
            error={text('Error', '')}
            value={value}
            onChange={event => onChange(event, store)}
            options={dropdownOptionsCreatable}
            name="story"
          />
        </div>
      );
    },
    MultilineOption({ choices, store, value }) {
      return (
        <div
          style={{
            width: '15rem',
          }}
        >
          <Select
            generic={true}
            choices={choices}
            value={value}
            onChange={event => onChange(event, store)}
            name="story"
            components={{
              Option: MultilineOption,
            }}
            isClearable={boolean('Clearable', true)}
          />
        </div>
      );
    },
    MultivalueAsync({ choices, store, value, loadedFirst }) {
      const simulateChoices = async () => {
        setTimeout(() => {
          store.set({
            choices: loadedFirst ? alternateChoices : defaultChoices,
          });
          store.set({
            loadedFirst: true,
          });
        }, DELAY);
        return [];
      };

      return (
        <div
          style={{
            width: '15rem',
            backgroundColor: '#F7F7F8',
          }}
        >
          <Select
            choices={choices}
            getChoices={simulateChoices}
            error={text('Error', '')}
            isMulti={true}
            value={value}
            onChange={event => onChange(event, store)}
            name="story"
            id="story"
            isClearable={boolean('Clearable', true)}
            loading={false}
            scope="story"
            filterOption={() => true}
          />
        </div>
      );
    },
  },
  stores
);
