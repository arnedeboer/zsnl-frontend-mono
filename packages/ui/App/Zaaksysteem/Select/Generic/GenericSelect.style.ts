// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { StyleSheetCreatorType } from '../types/SelectStyleSheetType';

export const selectStyleSheet: StyleSheetCreatorType = ({
  theme: {
    palette: { primary, common },
    typography: { fontFamily },
    mintlab: { greyscale },
    typography,
  },
  focus,
}) => {
  const color = focus ? primary.dark : greyscale.offBlack;
  const colorRules = {
    color,
    '&:hover': {
      color,
    },
  };

  const getOptionBgColor = (state: any) => {
    if (state.isSelected) return primary.light;
    if (state.isFocused) return primary.lighter;
    return common.white;
  };

  const getOptionTextColor = (state: any) => {
    if (state.isSelected) return greyscale.lighter;
    return greyscale.offBlack;
  };

  return {
    container(base) {
      return {
        ...base,
        width: '100%',
        fontFamily,
        fontWeight: typography.fontWeightLight,
      };
    },
    indicatorSeparator(base) {
      return {
        ...base,
        display: 'none',
      };
    },
    control(base) {
      return {
        ...base,
        height: '36px',
        minHeight: '36px',
        boxShadow: 'none',
        border: 'none',
        borderBottom: 'none',
        backgroundColor: focus ? primary.lighter : greyscale.light,
        paddingLeft: '16px',
        '& .startAdornment': {
          ...colorRules,
          margin: '3px 8px 0px 0px',
        },
      };
    },
    valueContainer(base) {
      return {
        ...base,
      };
    },
    placeholder(base) {
      return {
        ...base,
        ...colorRules,
        opacity: 0.5,
      };
    },
    dropdownIndicator(base) {
      return {
        ...base,
        ...colorRules,
        padding: '0px 4px',
      };
    },
    clearIndicator(base) {
      return {
        ...base,
        ...colorRules,
        padding: 0,
      };
    },
    singleValue(base) {
      return {
        ...base,
        ...colorRules,
      };
    },
    input(base) {
      return {
        ...base,
        ...colorRules,
        input: {
          fontWeight: typography.fontWeightLight,
          fontFamily,
        },
      };
    },
    option(base, state) {
      return {
        ...base,
        backgroundColor: getOptionBgColor(state),
        color: getOptionTextColor(state),
      };
    },
  };
};

export default selectStyleSheet;
