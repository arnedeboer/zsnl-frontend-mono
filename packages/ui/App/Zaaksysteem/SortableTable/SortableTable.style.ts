// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

const overflowEllipsis = {
  'white-space': 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
};

export const useSortableTableStyles = makeStyles(
  ({
    palette: { primary, basalt, cloud },
    typography,
    mintlab: { greyscale, shadows },
  }: any) => ({
    flexContainer: {
      display: 'flex',
      alignItems: 'center',
      boxSizing: 'border-box',
    },
    tableHeader: {
      color: basalt.lightest,
    },
    sortHeader: {
      color: basalt.lightest,
    },
    tableRow: {
      borderBottom: `1px solid ${greyscale.dark}`,
      alignItems: 'center',
    },
    tableRowHover: {
      '&:hover': {
        cursor: 'pointer',
        backgroundColor: primary.lightest,
      },
    },
    tableCellHeader: {
      display: 'flex',
      alignItems: 'center',
      '& >* svg': {
        marginLeft: 10,
      },
      '&:hover': {
        cursor: 'pointer',
      },
      textTransform: 'none',
      fontWeight: typography.fontWeightMedium,
    },
    tableCellHeaderDisabled: {
      '&:hover': {
        cursor: 'default',
      },
    },
    tableCell: {
      padding: 10,
      whiteSpace: 'normal',
    },
    tableCellFixedHeight: {
      ...overflowEllipsis,
      '& *': {
        ...overflowEllipsis,
      },
    },
    sortAsc: {
      transform: 'rotate(-90deg)',
    },
    sortDesc: {
      transform: 'rotate(90deg)',
    },
    noRowsMessage: {
      padding: 20,
    },
    selectCell: {
      marginLeft: 10,
      overflow: 'visible',
      '&>*': {
        overflow: 'visible',
      },
    },
    rowSelected: {
      backgroundColor: primary.lightest,
    },
    loader: {
      marginLeft: 20,
    },
    draggingRow: {
      border: `2px dotted ${cloud.darkest}`,
      ...typography.body1,
      padding: 4,
      boxShadow: shadows.medium,
    },
    dragHandle: {
      color: cloud.darkest,
    },
  })
);
