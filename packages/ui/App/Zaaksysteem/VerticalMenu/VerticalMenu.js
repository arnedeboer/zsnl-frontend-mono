// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@material-ui/styles';
import { addScopeProp, addScopeAttribute } from '../../library/addScope';
import VerticalMenuButton from './library/VerticalMenuButton';
import { VerticalMenuStylesheet } from './VerticalMenu.style';

/**
 * Vertical menu component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/VerticalMenu
 * @see /npm-mintlab-ui/documentation/consumer/manual/VerticalMenu.html
 *
 * @param {*} props
 * @param {Array<Action>} props.items
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const VerticalMenu = ({ items, classes, scope }) => (
  <nav className={classes.menu} {...addScopeAttribute(scope, 'vertical-menu')}>
    {items &&
      items.map((itemProps, index) => (
        <VerticalMenuButton
          key={index}
          {...itemProps}
          {...addScopeProp(scope, 'vertical-menu', itemProps.label)}
        />
      ))}
  </nav>
);

export default withStyles(VerticalMenuStylesheet)(VerticalMenu);
