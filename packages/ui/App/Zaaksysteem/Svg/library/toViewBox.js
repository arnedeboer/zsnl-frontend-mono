// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const { isArray } = Array;

const ZERO = 0;
const DIMENSIONS = 2;
const VIEWBOX = 4;

/**
 * @param {Array} array
 * @return {Array}
 */
function fill(array) {
  if (array.length === VIEWBOX) {
    return array;
  }

  if (array.length === DIMENSIONS) {
    return [ZERO, ZERO, ...array];
  }

  throw new Error('expected length to be 2 or 4');
}

/**
 * `Svg` component utility function.

 * @param {Array|number} value
 *   An number or an array of 2 or 4 numbers
 * @return {Array}
 */
export function toViewBox(value) {
  if (isArray(value)) {
    return fill(value);
  }

  if (typeof value == 'number') {
    return [ZERO, ZERO, value, value];
  }

  throw new TypeError('expected an Array or a number');
}
