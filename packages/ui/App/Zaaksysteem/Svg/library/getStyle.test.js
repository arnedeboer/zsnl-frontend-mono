// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getStyle } from './getStyle';

const WIDTH = 100;
const HEIGHT = 50;

describe('The `Svg` component’s `getStyle` utility function', () => {
  test('returns an object with both dimensions if they are provided', () => {
    const actual = getStyle([WIDTH, HEIGHT], '50px', '100px');
    const asserted = {
      width: '50px',
      height: '100px',
    };

    expect(actual).toEqual(asserted);
  });

  test('returns an object with a calculated height if only width is provided', () => {
    const actual = getStyle([WIDTH, HEIGHT], '50px');
    const asserted = {
      width: '50px',
      height: '25px',
    };

    expect(actual).toEqual(asserted);
  });

  test('returns an object with a calculated width if only height is provided', () => {
    const actual = getStyle([WIDTH, HEIGHT], '', '100px');
    const asserted = {
      width: '200px',
      height: '100px',
    };

    expect(actual).toEqual(asserted);
  });

  test('returns `null` if a class name and no dimensions are provided', () => {
    const actual = getStyle([WIDTH, HEIGHT], '', '', 'foobar');
    const asserted = null;

    expect(actual).toBe(asserted);
  });

  test('returns the viewbox values as CSS px lengths if that is the only argument', () => {
    const actual = getStyle([WIDTH, HEIGHT]);
    const asserted = {
      width: `${WIDTH}px`,
      height: `${HEIGHT}px`,
    };

    expect(actual).toEqual(asserted);
  });
});
