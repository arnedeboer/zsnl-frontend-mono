// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { parseSize } from './parseSize';

/**
 * `Svg` component utility function.
 *
 * @param {Array<number>} viewBox
 *   SVG viewbox width/height tuple
 * @param {string} height
 *   CSS length or percentage
 * @return {{width: string, height: string}}
 */
export function getStyleFromHeight(viewBox, height) {
  const [viewBoxWidth, viewBoxHeight] = viewBox;
  const [size, unit] = parseSize(height);

  if (unit === '%') {
    const HALF = 0.5;
    const horizontal = (viewBoxWidth / viewBoxHeight) * (size * HALF);

    return {
      paddingLeft: `${horizontal}${unit}`,
      height,
    };
  }

  return {
    width: `${(viewBoxWidth / viewBoxHeight) * size}${unit}`,
    height,
  };
}
