// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * `Svg` component utility function.
 *
 * @param {Object} [style]
 * @return {boolean}
 */
export function isResponsiveStyle(style) {
  if (style) {
    return (
      Object.prototype.hasOwnProperty.call(style, 'paddingLeft') ||
      Object.prototype.hasOwnProperty.call(style, 'paddingTop')
    );
  }

  return false;
}
