// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import { useCounterStyles } from './Counter.style';

type CountPropsType = {
  count: number;
};

const Counter: React.FunctionComponent<CountPropsType> = ({
  count,
  children,
}) => {
  const classes = useCounterStyles();

  if (!count) {
    return <>{children}</>;
  }

  return (
    <div className={classes.wrapper}>
      {children}
      <div className={classes.counter}>{Math.min(count, 99)}</div>
    </div>
  );
};

export default Counter;
