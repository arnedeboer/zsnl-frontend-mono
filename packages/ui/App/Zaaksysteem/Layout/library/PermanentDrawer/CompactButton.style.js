// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const diameter = 64;
const DOUBLE = 2;

/**
 * Style Sheet for the CompactButton component
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const compactButtonStyleSheet = ({
  mintlab: { greyscale },
  palette: { primary },
}) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: `${diameter}px`,
    height: `${diameter}px`,
    borderRadius: `${diameter / DOUBLE}px`,
    color: greyscale.evenDarker,
    '&:hover, &:focus': {
      color: primary.main,
    },
    '&:hover': {
      '& :nth-child(2)': {
        visibility: 'visible',
      },
    },
  },
  active: {
    color: primary.main,
  },
  label: {
    display: 'block',
    visibility: 'hidden',
    color: primary.main,
    fontSize: '0.6rem',
    marginTop: '2px',
  },
});
