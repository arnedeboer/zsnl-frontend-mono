// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Style Sheet for the PermanentDrawer component
 *
 * @return {JSS}
 */
export const permanentDrawerStyleSheet = () => ({
  paper: {
    position: 'static',
    background: 'transparent',
    border: 'none',
    padding: '8px 2px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  list: {
    margin: 0,
    padding: 0,
    listStyle: 'none',
    marginBottom: '15px',
  },
});
