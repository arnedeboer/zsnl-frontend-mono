// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@material-ui/styles';
import { addScopeProp, addScopeAttribute } from '../../library/addScope';
import AppBar from './library/AppBar';
import PermanentDrawer from './library/PermanentDrawer/PermanentDrawer';
import TemporaryDrawer from './library/TemporaryDrawer/TemporaryDrawer';
import Banners from './library/Banners/Banners';
import { layoutStyleSheet } from './Layout.style';

const DEFAULT_INDEX = 0;

/**
 * Global Layout component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Layout
 * @see /npm-mintlab-ui/documentation/consumer/manual/Layout.html
 *
 * @param {Object} props
 * @param {number} [props.active=0]
 * @param {*} props.children
 * @param {Object} props.classes
 * @param {string} props.customer
 * @param {Object} props.drawer
 * @param {Array} props.drawer.primary
 *    Array of Actions.
 * @param {Array} [props.drawer.secondary]
 *    Array of Actions.
 * @param {Object} [props.drawer.about]
 *    Single Action.
 * @param {string} props.identity
 * @param {boolean} props.isDrawerOpen
 * @param {string} props.menuLabel
 * @param {Function} props.toggleDrawer
 * @param {string} props.userLabel
 * @param {Array} props.userActions
 * @param {Object} props.banners
 * @param {Object} [props.searchComponent]
 * @return {ReactElement}
 */
export const Layout = ({
  active = DEFAULT_INDEX,
  children,
  classes,
  customer,
  drawer,
  identity,
  isDrawerOpen,
  menuLabel,
  toggleDrawer,
  userLabel,
  userName,
  userActions,
  banners,
  scope,
  searchComponent,
}) => (
  <div className={classes.root} {...addScopeAttribute(scope, 'layout')}>
    <AppBar
      gutter={2}
      menuLabel={menuLabel}
      onMenuClick={toggleDrawer}
      position="absolute"
      userLabel={userLabel}
      userName={userName}
      userActions={userActions}
      searchComponent={searchComponent}
      {...addScopeProp(scope, 'layout')}
    >
      {identity}
    </AppBar>

    <Banners banners={banners} {...addScopeProp(scope, 'layout')} />

    <TemporaryDrawer
      active={active}
      navigation={drawer}
      open={isDrawerOpen}
      subtitle={customer}
      title={identity}
      toggle={toggleDrawer}
      {...addScopeProp(scope, 'layout', 'drawer')}
    />

    <div className={classes.drawerAndContentContainer}>
      <PermanentDrawer
        active={active}
        navigation={drawer}
        {...addScopeProp(scope, 'layout', 'navigation')}
      />

      <div className={classes.content}>{children}</div>
    </div>
  </div>
);

export default withStyles(layoutStyleSheet)(Layout);
