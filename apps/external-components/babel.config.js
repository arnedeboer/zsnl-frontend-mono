// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

module.exports = {
  presets: [
    '@babel/preset-env',
    '@babel/preset-typescript',
    '@babel/preset-react',
  ],
  plugins: [
    '@babel/plugin-proposal-optional-chaining',
    [
      '@babel/plugin-transform-runtime',
      {
        regenerator: true,
      },
    ],
  ],
  env: {
    test: {
      plugins: [
        'dynamic-import-node',
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-proposal-optional-chaining',
      ],
    },
  },
};
