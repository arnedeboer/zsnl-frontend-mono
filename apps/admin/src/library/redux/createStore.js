// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { applyMiddleware, combineReducers, compose, createStore } from 'redux';

/**
 * @param {Object} reducers
 * @param {Object} preloadedState
 * @param {Array} middlewares
 * @return {Store<any>}
 */

export function createReduxStore(reducers, preloadedState, middlewares) {
  const { __REDUX_DEVTOOLS_EXTENSION_COMPOSE__ } = window;
  const composeEnhancers = __REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const enhancer = composeEnhancers(applyMiddleware(...middlewares));

  return createStore(combineReducers(reducers), preloadedState, enhancer);
}
