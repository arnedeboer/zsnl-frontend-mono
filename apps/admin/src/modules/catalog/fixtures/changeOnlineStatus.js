// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { TEXT } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

const formDefinition = [
  {
    name: 'reason',
    type: TEXT,
    value: '',
    required: true,
    label: 'changeOnlineStatus:fields.reason.label',
    placeholder: 'changeOnlineStatus:fields.reason.placeholder',
  },
];

export default formDefinition;
