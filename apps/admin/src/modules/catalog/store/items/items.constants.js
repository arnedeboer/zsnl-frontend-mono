// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_FETCH = createAjaxConstants('CATALOG_FETCH');
export const CATALOG_TOGGLE_ITEM = 'CATALOG_TOGGLE_ITEM';
export const CATALOG_CLEAR_SELECTED = 'CATALOG_CLEAR_SELECTED';
