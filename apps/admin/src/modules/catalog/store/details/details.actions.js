// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  CATALOG_FETCH_DETAILS,
  CATALOG_TOGGLE_DETAIL_VIEW,
} from './details.constants';

const fetchItemAjaxAction = createAjaxAction(CATALOG_FETCH_DETAILS);

export const fetchCatalogItem = (type, id) =>
  fetchItemAjaxAction({
    url: buildUrl('/api/v2/admin/catalog/get_entry_detail', {
      type,
      item_id: id,
    }),
    method: 'GET',
  });

export const toggleCatalogDetailView = () => ({
  type: CATALOG_TOGGLE_DETAIL_VIEW,
});
