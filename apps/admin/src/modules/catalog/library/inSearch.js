// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getSegments } from '@mintlab/kitchen-sink/source';

export const inSearch = route => {
  if (!route) return false;
  const segments = getSegments(route) || [];
  const [, query] = route.split('?');
  return segments.includes('catalogus') && query;
};
