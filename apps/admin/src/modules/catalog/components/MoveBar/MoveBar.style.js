// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const moveBarStyleSheet = ({
  palette: { common, primary },
  typography,
  mintlab: { shadows },
}) => ({
  growWrapper: {
    width: 'calc(100% - 24px - 24px)',
    margin: '20px auto',
  },
  wrapper: {
    flexGrow: '1',
    margin: 'auto',
    maxWidth: 'calc(1600px - 24px - 24px)',
    height: '42px',
    display: 'flex',
    padding: '16px 24px',
    backgroundColor: primary.main,
    borderRadius: '8px',
    zIndex: 100,
    boxShadow: shadows.medium,
  },
  left: {
    display: 'flex',
    width: 'auto',
    flexDirection: 'column',
    alignItems: 'normal',
    justifyContent: 'center',
  },
  right: {
    display: 'flex',
    flex: 'auto',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  buttonCancel: {
    color: common.white,
    fontWeight: typography.fontWeightRegular,
  },
  buttonMove: {
    backgroundColor: common.white,
    marginLeft: '20px',
    '&:hover': {
      backgroundColor: common.white,
    },
  },
  label: {
    color: common.white,
  },
  labelTitle: {
    fontWeight: typography.fontWeightRegular,
    marginBottom: '2px',
  },
  labelDescription: {
    fontWeight: typography.fontWeightLight,
  },
});
