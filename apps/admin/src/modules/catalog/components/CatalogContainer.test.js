// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getPathToItem } from '../library/pathGetters';
import { addRowData } from './CatalogContainer';

describe('The `CatalogContainer` module', () => {
  describe('The `addRowData` function', () => {
    const data = {
      id: '1',
      type: 'folder',
    };

    test('adds a `path` string to the row data', () => {
      const { path } = addRowData([], [])(data);
      expect(path).toEqual(getPathToItem(data.id));
    });

    test('adds a `isNavigable` boolean to the row data', () => {
      const { isNavigable } = addRowData([], [])(data);
      expect(isNavigable).toBeTruthy();
    });

    test('creates `selected` properties according to the `selected` array', () => {
      const selected = ['a', 'c'];
      const actual = [
        {
          ...addRowData(
            selected,
            []
          )({
            id: 'a',
            type: 'folder',
          }),
        },
        {
          ...addRowData(
            selected,
            []
          )({
            id: 'b',
            type: 'folder',
          }),
        },
        {
          ...addRowData(
            selected,
            []
          )({
            id: 'c',
            type: 'case_type',
          }),
        },
      ];

      expect(actual[0].selected).toEqual(true);
      expect(actual[1].selected).toEqual(false);
      expect(actual[2].selected).toEqual(true);
    });
  });
});
