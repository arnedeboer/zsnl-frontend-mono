// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@material-ui/styles';
import Breadcrumbs from '@mintlab/ui/App/Material/Breadcrumbs';
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import Title from '../../../../components/Header/Title';
import SubAppHeader from '../../../../components/Header/SubAppHeader';
import { catalogHeaderStyleSheet } from './CatalogHeader.style';
import ButtonBar from './../ButtonBar/ButtonBarContainer';

/**
 * @reactProps {Object} classes
 * @reactProps {Array<Object>} breadcrumbs
 * @reactProps {Function} doNavigate
 * @reactProps {Function} toggleFilterAction
 * @reactProps {string} query
 * @reactProps {Function} exitFilterAction
 * @reactProps {Function} doFilterAction
 * @reactProps {Function} t
 * @return {ReactElement}
 */
const CatalogHeader = ({ classes, breadcrumbs, doNavigate }) => (
  <SubAppHeader>
    <div className={classes.headerWrapper}>
      <div className={classes.navigation}>
        {breadcrumbs.length > 1 ? (
          <Breadcrumbs
            maxItems={4}
            items={breadcrumbs}
            onItemClick={preventDefaultAndCall(event => {
              doNavigate(event.currentTarget.getAttribute('href'));
            })}
            scope="catalog"
          />
        ) : (
          <Title>{breadcrumbs[0].label}</Title>
        )}
      </div>

      <div className={classes.center} />

      <div className={classes.actions}>
        <ButtonBar />
      </div>
    </div>
  </SubAppHeader>
);

export default withStyles(catalogHeaderStyleSheet)(CatalogHeader);
