// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const formStylesheet = ({ mintlab: { greyscale } }) => ({
  wrapper: {
    '&:not(:last-child)': {
      marginBottom: '20px',
    },
  },
  dialogTitle: {
    '&>*:nth-child(1)': {
      color: greyscale.darkest,
    },
  },
});
