// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { v4 } from 'uuid';
import { get, unique } from '@mintlab/kitchen-sink/source';
import { AJAX_STATE_PENDING } from '../../../../../library/redux/ajax/createAjaxConstants';
import { saveDocumentTemplate } from '../../../store/documentTemplate/documentTemplate.actions';
import formDefinition from '../../../fixtures/documentTemplate';
import DocumentTemplate from './DocumentTemplate';

const { keys } = Object;

/* eslint-disable getter-return */
const mangleValuesForSave = ({ values, id, currentFolderUUID }) => {
  const isNew = !id;
  const documentTemplateId = isNew ? v4() : id;

  // Modify values where needed, before they get
  // sent to the save action, and to the API endpoint.

  /* eslint complexity: [2, 11] */
  let fields = keys(values).reduce((accumulator, key) => {
    switch (key) {
      case 'integration_uuid':
        accumulator[key] = values[key] === 'default' ? null : values[key];
        return accumulator;
      default:
        accumulator[key] = values[key];
        return accumulator;
    }
  }, {});

  fields.category_uuid = currentFolderUUID;

  if (fields.file) {
    fields.file_uuid = fields.file.value;
    delete fields.file;
  }

  return {
    fields,
    documentTemplateId,
    isNew,
  };
};

const mapStateToProps = (
  {
    catalog: {
      documentTemplate,
      documentTemplate: { integrations, values, id },
      items: { currentFolderUUID },
    },
  },
  { t }
) => {
  // Insert translations, requests and other values into FormDefinition
  const mode = id ? 'edit' : 'create';

  const mapFormDefinition = field => {
    const fileProps = () => {
      if (field.name !== 'file') return {};

      const props = [
        'selectInstructions',
        'dragInstructions',
        'dropInstructions',
        'orLabel',
      ].reduce((acc, current) => {
        acc[current] = t(field[current]);
        return acc;
      }, {});
      return props;
    };

    const choicesProps = () => {
      if (field.name !== 'integration_uuid') return {};
      const merged = [...field.choices, ...integrations];
      return {
        choices: merged.map(integration => ({
          value: integration.value,
          label: t(integration.label),
          data: { ...integration.data },
        })),
      };
    };

    return {
      ...field,
      label: t(field.label),
      placeholder: t(field.placeholder),
      hint: t(field.hint),
      help: t(field.help),
      get value() {
        const value = get(values, field.name) || field.value;

        switch (field.name) {
          case 'file':
            return get(values, 'file_uuid')
              ? {
                  value: get(values, 'file_uuid'),
                  label: get(values, 'file_name'),
                  key: unique(),
                }
              : null;
          case 'integration_uuid':
            return value === null ? 'default' : value;
          case 'commit_message':
            return mode === 'create'
              ? t('catalog:defaultCreate')
              : t('catalog:defaultEdit');
          default:
            return value;
        }
      },
      get integrations() {
        switch (field.name) {
          case 'integration_uuid':
            return integrations;
          default:
            break;
        }
      },
      get disabled() {
        return mode === 'edit' && field.name === 'integration_uuid';
      },
      ...fileProps(),
      ...choicesProps(),
    };
  };
  const newProps = {
    ...documentTemplate,
    formDefinition: formDefinition.map(mapFormDefinition),
    currentFolderUUID,
  };

  return {
    initializing:
      documentTemplate.state === AJAX_STATE_PENDING ||
      documentTemplate.integrationsState === AJAX_STATE_PENDING,
    saving: documentTemplate.savingState === AJAX_STATE_PENDING,
    ...newProps,
  };
};

const mapDispatchToProps = dispatch => ({
  dispatchSave: params => dispatch(saveDocumentTemplate(params)),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentFolderUUID, id } = stateProps;
  const { dispatchSave } = dispatchProps;

  return {
    ...stateProps,
    ...ownProps,
    saveAction: ({ values }) =>
      dispatchSave(
        mangleValuesForSave({
          values,
          currentFolderUUID,
          id,
        })
      ),
  };
};

const connectedDialog = withTranslation()(
  connect(mapStateToProps, mapDispatchToProps, mergeProps)(DocumentTemplate)
);

export default connectedDialog;
