// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { hideDialog } from '../../../../../store/ui/ui.actions';
import { deleteItem } from '../../../store/deleteItem/deleteItem.actions';
import { getSelectedItem } from '../../../store/items/items.selectors';
import ConfirmDelete from './ConfirmDelete';

const mapStateToProps = state => ({
  selectedItem: getSelectedItem(state),
});

const mapDispatchToProps = dispatch => ({
  deleteItem: (item, reason) => dispatch(deleteItem(item, reason)),
  onCancel: () => dispatch(hideDialog()),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { selectedItem } = stateProps;

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    onConfirm: reason => dispatchProps.deleteItem(selectedItem, reason),
  };
};

const ConfirmDeleteContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ConfirmDelete);

export default ConfirmDeleteContainer;
