// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { asArray } from '@mintlab/kitchen-sink/source';
import validate from '../../../../library/validation/validation';

const { keys } = Object;

/**
 * @param {string} needle
 * @param {Array} fieldSets
 * @return {Object}
 */
export const getFieldByName = (needle, fieldSets) => {
  const findField = ({ name }) => name === needle;
  return getFieldList(fieldSets).find(findField);
};

/**
 * Returns a flattened array of field objects
 *
 * @param {Array|Object} input
 *  A fieldset object, or an array of these objects
 * @return {Array}
 */
export const getFieldList = input =>
  asArray(input).reduce((accumulator, { fields }) => {
    accumulator.push(...fields);
    return accumulator;
  }, []);

/**
 *
 * Perform the form validation by:
 *
 * 1) creating an array of all fieldsets based on the fieldsets
 * so that the constraints and other options may be found for
 * every field.
 * 2) running all form values through the `validate` function
 * with the constraints and options.
 * 3) returning an object containing field names as keys
 * and the error message as the value.
 *
 * @param {Object} arguments
 * @param {Function} arguments.t
 * @param {Object} arguments.fieldSets
 * @param {Object} arguments.values
 * @returns {Object}
 */
export const doValidation = ({ t, fieldSets, values }) => {
  const errors = keys(values).reduce((accumulator, name) => {
    const { constraints, config } = getFieldByName(name, fieldSets);

    const error = validate(values[name], constraints, config);

    if (error) {
      accumulator[name] = t(`validation:${error}`);
    }

    return accumulator;
  }, {});

  return errors;
};
