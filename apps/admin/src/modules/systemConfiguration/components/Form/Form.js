// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Component } from 'react';
import deepMerge from 'deepmerge';
import deepEqual from 'fast-deep-equal';
import { withStyles } from '@material-ui/styles';
import withWidth from '@material-ui/core/withWidth';
import Card from '@mintlab/ui/App/Material/Card';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { reduceMap, get, cloneWithout } from '@mintlab/kitchen-sink/source';
import { sharedStylesheet } from '../../../../components/Shared.style';
import { getFieldByName } from '../library/library';
import { formStylesheet } from './Form.style';
import * as typeMap from './typemap';

const { keys } = Object;
const identifier = 'systemConfigForm';

/**
 * Renders a System Configuration form, normally a single
 * category, based on the supplied fieldSets.
 * @see README.md
 *
 * @reactProps {Object} banners
 * @reactProps {Function} showBanner
 * @reactProps {Function} hideBanner
 * @reactProps {Function} t
 * @reactProps {Array} fieldSets
 * @reactProps {boolean} discard
 * @reactProps {Function} onDiscard
 *
 * Supplied by Formik:
 * @reactProps {Object} errors
 * @reactProps {boolean} dirty
 * @reactProps {Object} values
 * @reactProps {Object} initialValues
 * @reactProps {Function} resetForm
 * @reactProps {Function} setFieldValue
 */
export class Form extends Component {
  // Lifecycle methods:
  /**
   * @param {Object} prevProps
   */
  componentDidUpdate(prevProps) {
    const {
      props: {
        banners,
        showBanner,
        hideBanner,
        t,
        errors,
        dirty,
        resetForm,
        initialValues,
        onDiscard,
      },
      save,
      shouldProcessBanners,
      shouldProcessDiscard,
    } = this;

    // Show/hide banner
    if (shouldProcessBanners({ prevProps })) {
      const undo = [
        {
          action: () => resetForm(),
          label: t('form:undo'),
          icon: 'undo',
        },
      ];

      const bannerTypes = {
        changed: {
          identifier,
          variant: 'secondary',
          label: t('form:unsavedChanges'),
          primary: {
            action: save,
            label: t('form:save'),
            icon: 'close',
          },
          secondary: undo,
        },
        error: {
          identifier,
          label: t('form:errorsInForm'),
          variant: 'danger',
          secondary: undo,
        },
      };

      const hasErrors = Boolean(keys(errors).length);

      const map = new Map([
        [
          () => hasErrors,
          () => {
            if (
              get(banners, `${identifier}.variant`) !==
              bannerTypes.error.variant
            ) {
              showBanner(bannerTypes.error);
            }
          },
        ],
        [
          () => dirty,
          () => {
            if (
              get(banners, `${identifier}.variant`) !==
              bannerTypes.changed.variant
            ) {
              showBanner(bannerTypes.changed);
            }
          },
        ],
        [
          () => Object.prototype.hasOwnProperty.call(banners, identifier),
          () => hideBanner({ identifier }),
        ],
      ]);

      reduceMap({
        map,
      });
    }

    // Reset the form with the initial values, and execute the onDiscard callback.
    if (
      shouldProcessDiscard({
        prevProps,
      })
    ) {
      resetForm(initialValues);
      onDiscard();
    }
  }

  componentWillUnmount() {
    this.props.hideBanner({ identifier });
  }

  render() {
    const {
      props: { classes, fieldSets },
      getFieldSet,
    } = this;

    return (
      <form className={classes.sheet}>
        <div className={classes.form}>
          {fieldSets && fieldSets.map(getFieldSet, this)}
        </div>
      </form>
    );
  }

  // Custom methods:

  /**
   * @param {Object} fieldSet
   * @param {number} index
   * @return {ReactElement}
   */
  getFieldSet({ title, description, fields }, index) {
    const {
      getFormControl,
      props: { classes },
    } = this;
    return (
      <Card
        title={title}
        description={description}
        key={`${identifier}-${index}`}
        classes={{
          header: classes.cardHeader,
        }}
      >
        {fields.map(getFormControl, this)}
      </Card>
    );
  }

  /**
   * @param {Object} props
   * @param {string} [props.constraints=[]]
   * @param {string} props.name
   * @param {string} props.type
   * @param {string} props.label
   * @param {boolean} props.loading
   * @return {ReactElement}
   */
  getFormControl({
    constraints = [],
    name,
    type,
    help,
    hint,
    label,
    disableFilterOption,
    ...rest
  }) {
    const {
      props: { classes, values, errors, loading, width },
      mangleAndSet,
    } = this;

    const error = errors[name];
    const required = constraints.includes('required');
    const value = values[name];
    const filterOption = disableFilterOption ? option => option : undefined;
    const isCompact = ['xs', 'sm', 'md'].includes(width);

    const isHidden = () =>
      value === undefined ||
      (name === 'case_distributor_role' &&
        !values['case_distributor_group']?.value);

    // eslint-disable-next-line no-confusing-arrow
    const checked = () => (typeof value === 'boolean' ? value : undefined);

    const InputComponent = typeMap[type];

    if (isHidden()) return null;

    return (
      <div key={`${identifier}-${name}`} className={classes.formRow}>
        <FormControlWrapper
          error={error}
          required={required}
          label={label}
          help={help}
          hint={hint}
          compact={isCompact}
          scope="systemConfig"
          applyBackgroundColor={type !== 'checkbox'}
        >
          <InputComponent
            checked={checked()}
            error={error}
            name={name}
            onChange={mangleAndSet}
            required={required}
            value={value}
            key={`${identifier}-field-${name}`}
            filterOption={filterOption}
            loading={loading}
            {...cloneWithout(rest, 'value')}
          />
        </FormControlWrapper>
      </div>
    );
  }

  /**
   * @param {SyntheticEvent} event
   */
  mangleAndSet = event => {
    const base = get(event, 'target', event);
    const { name, value, checked } = base;

    const { fieldSets, setFieldValue, updateChoices } = this.props;

    const { constraints, type } = getFieldByName(name, fieldSets);

    const mangle = mangleValue => {
      const map = new Map([
        [() => type === 'checkbox', () => checked],
        [
          () => constraints && constraints.includes('number'),
          () => {
            const valueToNumber = Number(mangleValue);
            return Number.isNaN(valueToNumber) ? mangleValue : valueToNumber;
          },
        ],
        [
          () => name === 'case_distributor_group',
          () => {
            updateChoices({
              name: 'case_distributor_role',
              input: value.value,
            });
            return value;
          },
        ],
      ]);

      return reduceMap({
        map,
        fallback: mangleValue,
      });
    };

    setFieldValue(name, mangle(value));
  };

  save = () => {
    const { values, initialValues, fieldSets, save } = this.props;

    const items = keys(values).reduce((accumulator, key) => {
      if (!deepEqual(values[key], initialValues[key])) {
        const { reference } = getFieldByName(key, fieldSets);
        accumulator.push({
          name: key,
          value: values[key],
          reference,
        });
      }
      return accumulator;
    }, []);

    if (keys(items).length) {
      save(items);
    }
  };

  /**
   * @param {Object} prevProps
   * @return {boolean}
   */
  shouldProcessBanners = ({ prevProps }) =>
    !deepEqual(prevProps.values, this.props.values) ||
    !deepEqual(prevProps.dirty, this.props.dirty) ||
    !deepEqual(prevProps.errors, this.props.errors);

  /**
   * @param {Object} props
   * @param {Object} props.prevProps
   * @return {boolean}
   */
  shouldProcessDiscard = ({ prevProps }) =>
    !deepEqual(prevProps.discard, this.props.discard) &&
    this.props.discard === true;
}

/**
 * @param {Object} theme
 * @return {Object}
 */
const mergedStyles = theme =>
  deepMerge(sharedStylesheet(theme), formStylesheet(theme));

export default withWidth()(withStyles(mergedStyles)(Form));
