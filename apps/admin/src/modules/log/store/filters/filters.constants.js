// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const FILTERS_APPLY = 'LOG:FILTERS_APPLY';
export const FILTERS_FETCH_USER_LABEL = createAjaxConstants(
  'LOG:FILTERS_FETCH_USER_LABEL'
);
