// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { FILTERS_APPLY, FILTERS_FETCH_USER_LABEL } from './filters.constants';

const fetchLabelAction = createAjaxAction(FILTERS_FETCH_USER_LABEL);

/**
 * @param {String} uuid
 * @returns {Promise}
 */
export const fetchUserLabel = uuid =>
  fetchLabelAction({
    url: `/api/v1/subject/${uuid}`,
    method: 'GET',
  });

/**
 * @param {Object} data
 * @param {string} data.keyword
 * @param {string} data.caseNumber
 * @param {Object} data.user
 * @param {string} data.user.uuid
 * @param {string} [data.user.label]
 * @return {Object}
 */
export const filtersApply = ({ keyword, caseNumber, user } = {}) => ({
  type: FILTERS_APPLY,
  payload: {
    keyword,
    caseNumber,
    user,
  },
});
