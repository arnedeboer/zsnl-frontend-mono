// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/styles';
import Button from '@mintlab/ui/App/Material/Button';
import { exportButtonStyleSheet } from './ExportButton.style';

/**
 * @reactProps {Object} classes
 * @reactProps {string} baseClassName
 * @reactProps {Function} action
 * @reactProps {Object} exportParams
 * @reactProps {string} value
 * @return {ReactElement}
 */
export const ExportButton = ({ classes, baseClassName, action, value }) => (
  <div className={classNames(baseClassName, classes.wrapper)}>
    <Button action={() => action()} presets={['secondary', 'contained']}>
      {value}
    </Button>
  </div>
);

export default withStyles(exportButtonStyleSheet)(ExportButton);
