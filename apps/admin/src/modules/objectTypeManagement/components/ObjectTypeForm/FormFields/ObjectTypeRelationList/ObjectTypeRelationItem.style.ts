// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useRightItemStyle = makeStyles(
  ({ typography, palette: { primary } }: Theme) => ({
    mainContainer: {
      padding: '0 5px',
      display: 'flex',
    },
    fieldsContainer: {
      width: '100%',
    },
  })
);
