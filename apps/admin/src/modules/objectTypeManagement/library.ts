// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { APICaseManagement } from '@zaaksysteem/generated';
import { CustomFieldTypeType } from '@zaaksysteem/common/src/types/CustomFields';
import { normalizeValue } from '@zaaksysteem/common/src/library/normalizeValue';
import { ObjectTypeFormShapeType } from './components/ObjectTypeForm/ObjectTypeForm.types';
import { ObjectTypeType } from './types/ObjectType.types';

export type PayloadType = ObjectTypeFormShapeType & {
  folderUuid?: string;
};

type UpdateOrCreateType =
  | APICaseManagement.CreateCustomObjectTypeRequestBody
  | APICaseManagement.UpdateCustomObjectTypeRequestBody;

/**
 * This converts the Create Object form values to valid values
 * for the backend. Used in both create and update scenario's.
 *
 * Backend always requires something to be sent for
 * custom_field_definitions and
 * authorization_definition
 */
export function createDataFromPayload({
  name,
  title,
  subtitle,
  custom_fields,
  authorizations,
  caseTypeRelations,
  objectTypeRelations,
  changes,
  components_changed,
  external_reference,
  folderUuid,
}: PayloadType): UpdateOrCreateType {
  const relations = [
    ...(caseTypeRelations || []).map(({ id }) => ({ case_type_uuid: id })),
    ...(objectTypeRelations || []).map(({ id }) => ({
      custom_object_type_uuid: id,
    })),
  ];

  const data = {
    name,
    title,
    subtitle,
    uuid: v4(),
    external_reference,
    custom_field_definition: custom_fields?.length
      ? {
          custom_fields: custom_fields.map(
            ({
              attribute_uuid,
              label,
              description,
              external_description,
              is_required,
              is_hidden_field,
              use_on_map,
              name,
            }) => {
              return {
                attribute_uuid,
                label,
                is_required,
                is_hidden_field,
                external_description,
                description,
                ...(use_on_map === undefined
                  ? {}
                  : { custom_field_specification: { use_on_map } }),
                custom_field_type: '' as CustomFieldTypeType,
                name,
              };
            }
          ),
        }
      : {},
    relationship_definition: relations.length
      ? { relationships: relations }
      : {},
    authorization_definition: authorizations?.length
      ? {
          authorizations: authorizations.map(
            ({ permission, department, role }) => ({
              authorization: permission,
              department: {
                uuid: (normalizeValue(department) as string) || '',
              },
              role: {
                uuid: role,
              },
            })
          ),
        }
      : {},
    audit_log: {
      description: changes,
      updated_components: components_changed,
    },
    catalog_folder_uuid: folderUuid,
  };

  return data;
}

export const createIntitialValuesFromObjectType = ({
  name,
  title,
  subtitle,
  custom_field_definition,
  audit_log,
  external_reference,
  objectTypeRelations,
  caseTypeRelations,
  authorizations,
}: ObjectTypeType): Partial<ObjectTypeFormShapeType> => {
  return {
    name,
    title,
    subtitle,
    custom_fields: custom_field_definition?.custom_fields.map(
      (customField: any) => ({
        ...(customField?.custom_field_specification || {}),
        ...customField,
      })
    ),
    components_changed: audit_log?.updated_components,
    changes: audit_log?.description,
    external_reference,
    objectTypeRelations,
    caseTypeRelations,
    authorizations,
  };
};
