// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ObjectTypeManagementRootStateType } from '../objectTypeManagement.reducer';

export const busyUpdatingSelector = (
  state: ObjectTypeManagementRootStateType
): boolean => state.objectTypeManagement.update.state === 'pending';
