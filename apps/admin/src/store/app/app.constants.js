// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const APP_BOOTSTRAP_INIT = 'APP:BOOTSTRAP:INIT';
export const APP_BOOTSTRAP_VALID = 'APP:BOOTSTRAP:VALID';
export const APP_BOOTSTRAP_ERROR = 'APP:BOOTSTRAP:ERROR';

export const APP_SECTION_CHANGE = 'APP:SECTION:CHANGE';
