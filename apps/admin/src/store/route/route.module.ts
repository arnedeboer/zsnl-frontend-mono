// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { route } from './route.reducer';
import { routeMiddleware } from './route.middleware';

export const getRouterModule = () => ({
  id: 'route',
  reducerMap: {
    route: route as any,
  },
  middlewares: [routeMiddleware],
});

export default getRouterModule;
