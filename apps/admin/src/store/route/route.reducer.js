// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ROUTE_RESOLVE } from './route.constants';

const initialState = '/';

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function route(state = initialState, action) {
  switch (action.type) {
    case ROUTE_RESOLVE:
      return action.payload.path;
    default:
      return state;
  }
}
