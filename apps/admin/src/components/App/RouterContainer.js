// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import {
  openOverlay,
  closeOverlay,
  loadWindow,
  unloadWindow,
} from '../../store/ui/ui.actions';
import { invoke } from '../../store/route/route.actions';
import Router from './Router';

const mapStateToProps = ({
  ui: {
    iframe: { loading, overlay },
  },
  route,
}) => ({
  hasIframeOverlay: overlay,
  isIframeLoading: loading,
  requestUrl: route,
});

const mapDispatchToProps = dispatch => ({
  onIframeOpen: () => dispatch(openOverlay()),
  onIframeClose: () => dispatch(closeOverlay()),
  onIframeLoad: () => dispatch(loadWindow()),
  onIframeUnload: () => dispatch(unloadWindow()),
  route: path => dispatch(invoke(path)),
});

const connectWithTranslation = withTranslation();
const connectWithStore = connect(mapStateToProps, mapDispatchToProps);

const RouterContainer = connectWithTranslation(connectWithStore(Router));

export default RouterContainer;
