// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormatDateContext } from './FormatDate';

export function withFormatDate(Component) {
  return function WrapperComponent(props) {
    return (
      <FormatDateContext.Consumer>
        {formatDate => <Component {...props} formatDate={formatDate} />}
      </FormatDateContext.Consumer>
    );
  };
}
