// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @return {JSS}
 */
export const layoutStylesheet = () => ({
  next: {
    padding: '0 20px',
    width: '100%',
  },
  legacy: {
    width: '100%',
  },
});
