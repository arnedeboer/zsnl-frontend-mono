// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import LoadableConfiguration from './modules/systemConfiguration';
import LoadableLog from './modules/log';
import LoadableCatalog from './modules/catalog';
import LoadableObjecttype from './modules/objectTypeManagement';

/**
 * iFrame base segment dictionary.
 *
 * @type {Object}
 */
const routes = {
  // Main navigation.
  catalogus: LoadableCatalog,
  objecttype: LoadableObjecttype,
  gebruikers: '/medewerker',
  logboek: LoadableLog,
  transacties: '/beheer/sysin/transactions',
  koppelingen: '/beheer/sysin/overview',
  gegevens: '/beheer/object/datastore',
  configuratie: LoadableConfiguration,

  // Orphans. The URLs do *not* reflect their
  // position in the user interface hierarchy.
  import: '/beheer/import',
  objecttypen: '/beheer/objecttypen',
  object: '/beheer/object/import',
  search: '/beheer/object/search',
  woz: '/beheer/woz',
  zaaktypen: '/beheer/zaaktypen',
};

export default routes;
