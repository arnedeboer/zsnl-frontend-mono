// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type TypePath = (path?: string) => string;

export const createPath =
  (basePath: string): TypePath =>
  (path = '') => {
    return `${basePath}${path}`;
  };
