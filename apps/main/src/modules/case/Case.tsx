// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar.cmp';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import BreadcrumbBar, {
  BreadcrumbBarPropsType,
} from '../../components/BreadcrumbBar/BreadcrumbBar';
import NotificationBar from '../../components/NotificationBar/NotificationBar';
import Relations from './components/views/relations';
import Timeline from './components/views/timeline';
import Communication from './components/views/communication';
import Phases from './components/views/phases';
import SideMenu from './components/sideMenu/SideMenu';
import { CaseObjType, CaseTypeType, DialogsType, JobType } from './Case.types';
import { useCaseStyles } from './Case.style';
import {
  dialogIsCaseAction,
  formatBreadcrumbLabel,
  getNotifications,
  getUnfinishedJobs,
} from './Case.library';
import AboutDialog from './components/sideMenu/info/AboutDialog';
import Documents from './components/views/documents';
import ConfidentialityDialog from './components/sideMenu/info/ConfidentialityDialog';
import CaseActionsDialog from './components/sideMenu/caseActions/CaseActionsDialog';
import CaseActionDialog from './components/sideMenu/caseActions/CaseActionDialog';

export interface CasePropsType {
  session: SessionType;
  caseObj: CaseObjType;
  refreshCaseObj: () => void;
  caseType: CaseTypeType;
  jobs: JobType[];
  rootPath: string;
}

/* eslint complexity: [2, 9] */
const Case: React.ComponentType<CasePropsType> = ({
  session,
  caseObj,
  refreshCaseObj,
  caseType,
  jobs,
  rootPath,
}) => {
  const classes = useCaseStyles();
  const [t] = useTranslation('case');
  const [dialog, setDialog] = useState<DialogsType>();
  const [snackOpen, setSnackOpen] = useState(false);

  const closeDialog = () => setDialog(undefined);

  const breadcrumbs: BreadcrumbBarPropsType['breadcrumbs'] = [
    {
      label: t('main:modules.dashboard'),
      path: '/intern',
    },
    { label: formatBreadcrumbLabel(t, caseObj, caseType), path: rootPath },
  ];

  const notifications = getNotifications(
    t,
    session,
    caseObj,
    refreshCaseObj,
    jobs,
    setSnackOpen
  );

  return (
    <div className={classes.wrapper}>
      <Snackbar
        handleClose={(event: React.ChangeEvent, reason: string) => {
          if (reason === 'clickaway') return;
          setSnackOpen(false);
        }}
        message={`${t('snack.jobs')}: (${getUnfinishedJobs(jobs).length}/${
          jobs.length
        })`}
        open={snackOpen}
      />
      <BreadcrumbBar breadcrumbs={breadcrumbs} />
      <NotificationBar notifications={notifications} />
      <div className={classes.content}>
        <SideMenu
          rootPath={rootPath}
          caseObj={caseObj}
          setDialog={setDialog}
          refreshCaseObj={refreshCaseObj}
        />
        <div className={classes.view}>
          <Switch>
            <Redirect exact from={`${rootPath}`} to={`${rootPath}/phases`} />
            <Route
              path={`${rootPath}/phases`}
              render={({ match }) => (
                <Phases
                  rootPath={match.url}
                  caseObj={caseObj}
                  caseType={caseType}
                />
              )}
            />
            <Route
              path={`${rootPath}/documents`}
              render={() => <Documents caseId={caseObj.number} />}
            />
            <Route
              path={`${rootPath}/communication`}
              render={({ match }) => (
                <Communication
                  caseObj={caseObj}
                  caseType={caseType}
                  rootPath={match.url}
                />
              )}
            />
            <Route
              path={`${rootPath}/timeline`}
              render={() => <Timeline caseObj={caseObj} />}
            />
            <Route
              path={`${rootPath}/relations`}
              render={() => (
                <Relations
                  session={session}
                  caseObj={caseObj}
                  caseType={caseType}
                />
              )}
            />
          </Switch>
        </div>
      </div>
      <AboutDialog
        caseObj={caseObj}
        caseType={caseType}
        onClose={closeDialog}
        open={dialog === 'about'}
      />
      <ConfidentialityDialog
        caseObj={caseObj}
        onClose={closeDialog}
        open={dialog === 'confidentiality'}
        refreshCaseObj={refreshCaseObj}
      />
      <CaseActionsDialog
        caseObj={caseObj}
        onClose={closeDialog}
        open={dialog === 'caseActions'}
        setDialog={setDialog}
      />
      {dialog && dialogIsCaseAction(dialog) && (
        <CaseActionDialog
          caseObj={caseObj}
          caseType={caseType}
          session={session}
          onClose={closeDialog}
          caseAction={dialog}
          refreshCaseObj={refreshCaseObj}
        />
      )}
    </div>
  );
};

export default Case;
