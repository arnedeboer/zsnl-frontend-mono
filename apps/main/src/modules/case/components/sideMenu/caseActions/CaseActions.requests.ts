// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { APICaseManagement } from '@zaaksysteem/generated';
import { FormikValues } from 'formik';
import { setPaymentStatus } from '../../../Case.requests';
import { CaseObjType } from '../../../Case.types';
import { reverseDate, getCapabilities } from './Caseactions.library';

const v0requestUpdate = async (caseObj: CaseObjType, data: any) => {
  const url = `/api/v0/case/${caseObj.number}/update`;

  return await request('POST', url, data);
};

const v0requestSettings = async (caseObj: CaseObjType, data: any) => {
  const url = `/zaak/${caseObj.number}/update/set_settings`;

  return await request('POST', url, {
    commit: 1,
    no_redirect: 1,
    selected_case_ids: caseObj.number,
    selection: 'one_case',
    ...data,
  });
};

type ActionRequestType = ({
  values,
  caseObj,
  session,
}: {
  values: FormikValues;
  caseObj: CaseObjType;
  session: SessionType;
}) => Promise<any>;

/* eslint complexity: [2, 8] */
const assignRequest: ActionRequestType = async ({
  caseObj,
  values,
  session,
}) => {
  const type = values.allocationType;
  let data;

  switch (type) {
    case 'departmentRole': {
      data = {
        change_allocation: 'group',
        group_uuid: values.department.value,
        role_uuid: values.role,
      };
      break;
    }
    case 'employee': {
      data = {
        change_allocation: 'behandelaar',
        subject_uuid: values.employee.value,
        change_department: values.changeDepartment ? 1 : 0,
        add_as_assignee: values.addAsAssignee ? 1 : 0,
        notify: values.notify ? 1 : 0,
      };
      break;
    }
    case 'self': {
      data = {
        change_allocation: 'behandelaar',
        subject_uuid: session?.logged_in_user.uuid,
        change_department: values.changeDepartment ? 1 : 0,
      };
      break;
    }
    default:
      return;
  }

  const url = buildUrl(`/zaak/${caseObj.number}/update/allocation`, {
    comment: values.comment,
    selected_case_ids: caseObj.number,
    commit: 1,
    context: 'case',
    selection: 'one_case',
    ...data,
  });

  return await request('POST', url);
};

const stallRequest: ActionRequestType = async ({ caseObj, values }) => {
  const url = '/api/v2/cm/case/pause';
  const type = values.suspensionType;
  const data = {
    case_uuid: caseObj.uuid,
    suspension_reason: values.reason,
    ...(type === 'indefinite'
      ? {
          suspension_term_type: type,
        }
      : {
          suspension_term_type: values.termType.value,
          suspension_term_value:
            values.termType === 'fixed_date'
              ? values.fixedDate
              : Number(values.termLength),
        }),
  };

  return await request<APICaseManagement.PauseCaseRequestBody>(
    'POST',
    url,
    data
  );
};

const resumeRequest: ActionRequestType = async ({ caseObj, values }) => {
  const url = '/api/v2/cm/case/resume';
  const data = {
    case_uuid: caseObj.uuid,
    resume_reason: values.reason,
    stalled_since_date: values.since,
    stalled_until_date: values.until,
  };

  return await request<APICaseManagement.ChangeCaseCoordinatorRequestBody>(
    'POST',
    url,
    data
  );
};

const resolvePrematurelyRequest: ActionRequestType = async ({
  caseObj,
  values,
}) => {
  const url = `/zaak/${caseObj.number}/update/afhandelen`;

  return await request('POST', url, {
    reden: values.reason,
    system_kenmerk_resultaat_id: values.result,
    update: 1,
  });
};

const prolongRequest: ActionRequestType = async ({ caseObj, values }) => {
  const url = `/zaak/${caseObj.number}/update/verlengen`;
  const type = values.prolongType;

  return await request('POST', url, {
    selected_case_ids: caseObj.number,
    selection: 'one_case',
    commit: 1,
    reden: values.reason,
    prolongation_type: type,
    ...(type === 'fixedDate'
      ? { datum: reverseDate(values.fixedDate) }
      : {
          term_type: values.termType,
          amount: values.amount,
        }),
  });
};

const copyRequest: ActionRequestType = async ({ caseObj }) => {
  const url = `/zaak/duplicate/${caseObj.number}`;

  return await request('POST', url, {
    confirmed: 1,
    no_redirect: 1,
  });
};

const setRequestorRequest: ActionRequestType = async ({ caseObj, values }) =>
  await v0requestSettings(caseObj, {
    aanvrager_id: values.requestor.uuid,
  });

const setAssigneeRequest: ActionRequestType = async ({ caseObj, values }) => {
  const url = '/api/v2/cm/case/assign_case_to_user';
  const data: APICaseManagement.AssignCaseToUserRequestBody = {
    case_uuid: caseObj.uuid,
    user_uuid: values.assignee.value,
  };

  return await request<APICaseManagement.AssignCaseToUserResponseBody>(
    'POST',
    url,
    data
  );
};

const setCoordinatorRequest: ActionRequestType = async ({
  caseObj,
  values,
}) => {
  const url = '/api/v2/cm/case/change_case_coordinator';
  const data: APICaseManagement.ChangeCaseCoordinatorRequestBody = {
    case_uuid: caseObj.uuid,
    coordinator_uuid: values.coordinator.value,
  };

  return await request<APICaseManagement.ChangeCaseCoordinatorRequestBody>(
    'POST',
    url,
    data
  );
};

const setDepartmentAndRoleRequest: ActionRequestType = async ({
  caseObj,
  values,
}) => {
  const url = `/api/v2/cm/case/assign_case_to_department`;
  const data: APICaseManagement.AssignCaseToDepartmentRequestBody = {
    case_uuid: caseObj.uuid,
    department_uuid: values.department.value,
    role_uuid: values.role,
  };

  return await request<APICaseManagement.AssignCaseToDepartmentResponseBody>(
    'POST',
    url,
    data
  );
};

const setRegistrationDateRequest: ActionRequestType = async ({
  caseObj,
  values,
}) => {
  const url = '/api/v2/cm/case/set_registration_date';
  const data = {
    case_uuid: caseObj.uuid,
    target_date: values.registrationDate,
  };

  return await request('POST', url, data);
};

const setTargetCompletionDateRequest: ActionRequestType = async ({
  caseObj,
  values,
}) => {
  const url = '/api/v2/cm/case/set_target_completion_date';
  const data = {
    case_uuid: caseObj.uuid,
    target_date: values.targetCompletionDate,
  };

  return await request('POST', url, data);
};

const setCompletionDateRequest: ActionRequestType = async ({
  caseObj,
  values,
}) => {
  const url = '/api/v2/cm/case/set_completion_date';
  const data = {
    case_uuid: caseObj.uuid,
    target_date: values.completionDate,
  };

  return await request('POST', url, data);
};

const setDestructionDateRequest: ActionRequestType = async ({
  caseObj,
  values,
}) => {
  const type = values.termType;

  return await v0requestSettings(caseObj, {
    vernietigingsdatum_reden: values.reason.value,
    ...(type === 'fixedDate'
      ? {
          vernietigingsdatum_type: 'termijn',
          vernietigingsdatum: reverseDate(values.fixedDate),
        }
      : {
          // always 'bewaren', regardless of term length. Otherwise API breaks
          vernietigingsdatum_type: 'bewaren',
          vernietigingsdatum_recalculate: values.term.value,
        }),
  });
};

const setPhaseRequest: ActionRequestType = async ({ caseObj, values }) =>
  await v0requestSettings(caseObj, {
    // example:
    //   phase 5 has a milestone 5
    //   the milestone of the case is the milestone of the last completed phase
    //   so when you want to go to phase 5, the milestone of the case needs to be 4
    milestone: values.milestone - 1,
  });

const setStatusRequest: ActionRequestType = async ({ caseObj, values }) =>
  await v0requestUpdate(caseObj, { status: values.status });

const setPaymentStatusRequest: ActionRequestType = async ({
  caseObj,
  values,
}) => await setPaymentStatus(caseObj.uuid, values.paymentStatus);

const customiseAuthorizationsRequest: ActionRequestType = async ({
  values,
}) => {
  const url = '/api/v1/case/${caseObj.uuid}/acl/update';
  const data = {
    values: values.authorizations.map((auth: any) => ({
      capabilities: getCapabilities(auth.capabilities),
      entity_id: `${auth.department.value}|${auth.role}`,
      entity_type: 'position',
      scope: 'instance',
    })),

    cascade_to: {
      continuation: values.cascadeTo.includes('continuation'),
      partial: values.cascadeTo.includes('partial'),
      related: values.cascadeTo.includes('related'),
    },
  };

  return await request('POST', url, data);
};

const setResultRequest: ActionRequestType = async ({ caseObj, values }) =>
  await v0requestSettings(caseObj, {
    resultaat: values.result,
  });

const updateFieldValueRequest: ActionRequestType = async ({
  caseObj,
  values,
}) => {
  return true;
};

const changeCaseTypeRequest: ActionRequestType = async ({
  caseObj,
  values,
}) => {
  const response = await v0requestSettings(caseObj, {
    casetype_uuid: values.casetype.value,
  });

  // when a requested action is _purposefully_ not executed it is considered a 'success'
  // so the message is the only way to know whether your action was truly successful
  // @ts-ignore
  const message = response?.json?.messages[0]?.message;

  if (
    message === 'Geen acties uitgevoerd, mogelijk i.v.m onvoldoende rechten.'
  ) {
    throw {
      response: { ...response, url: '/api/v2/fake' },
      errors: [{ code: 'case/v1/insufficient_rights' }],
    };
  }
};

export default {
  assign: assignRequest,
  stall: stallRequest,
  resume: resumeRequest,
  resolvePrematurely: resolvePrematurelyRequest,
  prolong: prolongRequest,
  copy: copyRequest,
  setRequestor: setRequestorRequest,
  setAssignee: setAssigneeRequest,
  setCoordinator: setCoordinatorRequest,
  setDepartmentAndRole: setDepartmentAndRoleRequest,
  setRegistrationDate: setRegistrationDateRequest,
  setTargetCompletionDate: setTargetCompletionDateRequest,
  setCompletionDate: setCompletionDateRequest,
  setDestructionDate: setDestructionDateRequest,
  setPhase: setPhaseRequest,
  setStatus: setStatusRequest,
  setPaymentStatus: setPaymentStatusRequest,
  customiseAuthorizations: customiseAuthorizationsRequest,
  setResult: setResultRequest,
  updateFieldValue: updateFieldValueRequest,
  changeCaseType: changeCaseTypeRequest,
};
