// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import AddElement from '@zaaksysteem/common/src/components/dialogs/AddElement/AddElement';
import { SectionType } from '@zaaksysteem/common/src/components/dialogs/AddElement/AddElement.types';
import { CaseActionType, CaseObjType, DialogsType } from '../../../Case.types';
import { caseActionIcons } from './Caseactions.library';

type CaseActionsDialogPropsType = {
  caseObj: CaseObjType;
  open: boolean;
  onClose: () => void;
  setDialog: (type: DialogsType) => void;
};

const CaseActionsDialog: React.ComponentType<CaseActionsDialogPropsType> = ({
  caseObj,
  open,
  onClose,
  setDialog,
}) => {
  const [t] = useTranslation('case');
  const title = t('caseActions.selection.title');
  const { status, canEdit, caseOpen, hasManageRights } = caseObj;
  const isStalled = status === 'stalled';

  const regularActions: CaseActionType[] = [
    'assign',
    'stall',
    'resume',
    'resolvePrematurely',
    'prolong',
    'copy',
  ];

  const manageActions: CaseActionType[] = [
    // 'setRequestor',
    'setAssignee',
    'setCoordinator',
    'setDepartmentAndRole',
    'setRegistrationDate',
    'setTargetCompletionDate',
    'setCompletionDate',
    'setDestructionDate',
    'setPhase',
    // 'setStatus',
    'setPaymentStatus',
    // 'customiseAuthorizations',
    'setResult',
    // 'updateFieldValue',
    'changeCaseType',
  ];

  const disableWhen: {
    [key: string]: boolean;
  } = {
    assign: !canEdit,
    stall: !canEdit || isStalled,
    resume: !canEdit || !isStalled,
    resolvePrematurely: !canEdit,
    prolong: !canEdit,
    setCompletionDate: caseOpen,
    setResult: caseOpen,
  };

  const mapActions = (action: CaseActionType): SectionType => ({
    type: action,
    title: t(`caseActions.${action}.title`),
    action: () => {
      setDialog(action);
    },
    disabled: disableWhen[action],
    //@ts-ignore
    icon: <Icon size="large">{caseActionIcons[action]}</Icon>,
  });

  if (!open) {
    return null;
  }

  return (
    <AddElement
      t={t}
      hide={onClose}
      title={title}
      titleIcon={iconNames.build}
      sections={[
        regularActions.map(mapActions),
        ...(hasManageRights ? [manageActions.map(mapActions)] : []),
      ]}
    />
  );
};

export default CaseActionsDialog;
