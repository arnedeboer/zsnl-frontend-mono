// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import * as i18next from 'i18next';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';

export type AuthorizationFormPropsType = {
  openServerErrorDialog: OpenServerErrorDialogType;
  t: i18next.TFunction;
};

export type GetFormDefinitionType = {
  openServerErrorDialog: OpenServerErrorDialogType;
  t: i18next.TFunction;
};

export type AuthorizationType = {
  department: null | string | ValueType<string>;
  role: string;
  capabilities: 'search' | 'read' | 'readwrite' | 'admin';
};
