// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useSideMenuStyles = makeStyles(
  ({
    mintlab: { greyscale, shadows },
    palette: { common, primary },
  }: Theme) => ({
    wrapper: {
      position: 'relative',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      maxWidth: '260px',
      '&>*:not(:last-child)': {
        borderBottom: `1px solid ${greyscale.dark}`,
      },
      borderRight: `1px solid ${greyscale.dark}`,
    },
    caseActionsWrapper: {
      padding: '8px 8px',
      height: 55,
      display: 'flex',
      alignItems: 'center',
    },
    caseActions: {
      width: '100%',
    },
    caseActionsIcon: {
      backgroundColor: primary.main,
      color: common.white,
    },
    folded: {
      width: 55,
    },
    foldButton: {
      position: 'absolute',
      top: '90%',
      right: -23,
      zIndex: 1,
      backgroundColor: greyscale.dark,
      border: greyscale.darkest,
      '&:hover': {
        backgroundColor: greyscale.darker,
      },
      boxShadow: shadows.sharp,
    },
  })
);
