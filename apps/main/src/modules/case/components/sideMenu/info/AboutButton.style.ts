// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useAboutButtonStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
  },
  about: {
    width: '100%',
    margin: '8px 54px 8px 16px',
  },
  aboutIcon: {},
}));
