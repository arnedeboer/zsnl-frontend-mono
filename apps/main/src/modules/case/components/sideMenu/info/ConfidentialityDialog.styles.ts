// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useConfidentialityDialogStyles = makeStyles(
  ({ typography }: Theme) => ({
    wrapper: {
      width: '500px',
      fontFamily: typography.fontFamily,
      display: 'flex',
      flexDirection: 'column',
    },
    warning: {
      margin: '8px 0',
    },
  })
);
