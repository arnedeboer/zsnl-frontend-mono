// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { capitalize } from '@mintlab/kitchen-sink/source';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import IconButton from '@material-ui/core/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { CaseObjType, DialogsType } from '../../../Case.types';
import { assignToSelf } from '../../../Case.requests';
import { useInfoStyles } from './Info.styles';
import {
  Result,
  Status,
  Requestor,
  Recipient,
  Assignee,
  Department,
  RegistrationDate,
  TargetDate,
  CompletionDate,
  Location,
  Confidentiality,
  PaymentStatus,
} from './Icons';
import { getSubjectUrl } from './Info.library';
import AboutButton from './AboutButton';
import { formatDate } from './../../../Case.library';

export interface InfoPropsType {
  caseObj: CaseObjType;
  setDialog: (type: DialogsType) => void;
  refreshCaseObj: () => void;
  folded: boolean;
}

type ItemType = {
  id: string;
  icon: React.ReactElement;
  label?: string;
  show: boolean;
  tooltip?: string;
  warning?: boolean;
  href?: string;
  action?: () => void;
  labelAction?: () => void;
  disabled?: boolean;
};

type LinkComponentPropsType = {
  url: string;
  children: string | React.ReactNode;
};

const LinkComponent = React.forwardRef<
  HTMLAnchorElement,
  LinkComponentPropsType
>((props, ref) => <Link innerRef={ref} to={props.url} {...props} />);
LinkComponent.displayName = 'InfoLink';

/* eslint complexity: [2, 19] */
const Info: React.ComponentType<InfoPropsType> = ({
  caseObj,
  setDialog,
  refreshCaseObj,
  folded,
}) => {
  const classes = useInfoStyles();
  const [t] = useTranslation('case');
  const history = useHistory();
  const {
    caseOpen,
    result,
    resultDescription,
    status,
    requestor,
    presetClient,
    recipient,
    assignee,
    department,
    registrationDate,
    targetDate,
    completionDate,
    location,
    confidentiality,
    payment,
  } = caseObj;

  const items: ItemType[] = [
    {
      id: 'result',
      icon: <Result />,
      label: resultDescription || capitalize(result || ''),
      show: Boolean(result && !caseOpen),
    },
    {
      id: 'status',
      icon: <Status status={status} />,
      label: t(`status.${status}`),
      show: true,
    },
    {
      id: 'requestor',
      icon: <Requestor type={requestor?.type} presetClient={presetClient} />,
      label: requestor?.name,
      show: true,
      tooltip: presetClient ? t('info.presetClient') : t(`info.requestor`),
      href: requestor ? getSubjectUrl(requestor) : '',
    },
    {
      id: 'recipient',
      icon: <Recipient type={recipient?.type} />,
      label: recipient?.name,
      show: Boolean(recipient),
      href: recipient ? getSubjectUrl(recipient) : '',
    },
    {
      id: 'assignee',
      icon: <Assignee />,
      label: assignee?.name,
      show: Boolean(assignee),
      href: assignee ? getSubjectUrl(assignee) : '',
    },
    // this action is available to those with read rights
    {
      id: 'assignToSelf',
      icon: <Assignee />,
      label: t('info.assignToSelf'),
      show: !assignee,
      labelAction: () =>
        assignToSelf(caseObj.uuid).then(() => refreshCaseObj()),
    },
    {
      id: 'department',
      icon: <Department />,
      label: department,
      show: true,
    },
    {
      id: 'registrationDate',
      icon: <RegistrationDate />,
      label: formatDate(registrationDate),
      show: true,
    },
    {
      id: 'targetDate',
      icon: <TargetDate />,
      label: formatDate(targetDate),
      show: true,
      warning: new Date() > new Date(targetDate || ''),
    },
    {
      id: 'completionDate',
      icon: <CompletionDate />,
      label: formatDate(completionDate),
      show: Boolean(completionDate),
    },
    {
      id: 'location',
      icon: <Location />,
      label: location,
      show: Boolean(location),
    },
    {
      id: 'confidentiality',
      icon: <Confidentiality confidentiality={confidentiality} />,
      label: t(`confidentiality.${confidentiality}`),
      show: true,
      action: () => setDialog('confidentiality'),
      disabled: !caseObj.canEdit,
    },
    {
      id: 'paymentStatus',
      icon: <PaymentStatus t={t} />,
      label: payment.amount || '-',
      show: Boolean(payment.status),
      tooltip:
        t('info.paymentStatus') + payment.status
          ? t(`paymentStatus.${payment.status}`)
          : '',
    },
  ].filter(item => Boolean(item.show));

  return (
    <div className={classes.wrapper}>
      {items.map(item => {
        const tooltip = item.tooltip || t(`info.${item.id}`) || '';
        const foldedTooltip = `${tooltip}: ${item.label}`;

        if (folded) {
          return (
            <Tooltip
              key={item.id}
              title={foldedTooltip}
              placement="right"
              className={classes.tooltip}
            >
              <>
                {item.href && (
                  <LinkComponent url={item.href}>
                    <ListItemIcon className={classes.iconButton}>
                      {item.icon}
                    </ListItemIcon>
                  </LinkComponent>
                )}

                {(item.action || item.labelAction) && (
                  <IconButton
                    className={classes.iconButton}
                    onClick={
                      item.href
                        ? () => history.push(item.href || '')
                        : item.action || item.labelAction
                    }
                  >
                    {item.icon}
                  </IconButton>
                )}

                {!item.href && !item.action && !item.labelAction && (
                  <ListItemIcon className={classes.iconButton}>
                    {item.icon}
                  </ListItemIcon>
                )}
              </>
            </Tooltip>
          );
        }

        return (
          <Tooltip
            key={item.id}
            title={tooltip}
            placement="right"
            className={classes.tooltip}
          >
            <div className={classes.item}>
              <ListItemIcon>{item.icon}</ListItemIcon>

              <div
                className={classNames(classes.label, {
                  [classes.warning]: Boolean(item.warning),
                })}
              >
                {item.href && (
                  <LinkComponent url={item.href}>
                    {item.label || ''}
                  </LinkComponent>
                )}

                {item.labelAction && (
                  <Button
                    className={classes.labelAction}
                    action={item.labelAction}
                    presets={['outlined']}
                  >
                    {item.label}
                  </Button>
                )}

                {!item.href && !item.labelAction && item.label}
              </div>

              {item.href && (
                <a
                  href={item.href}
                  target="_blank"
                  rel="noopener noreferrer"
                  className={classes.openInNewIcon}
                >
                  <Icon size="tiny">{iconNames.open_in_new}</Icon>
                </a>
              )}

              {item.action && (
                <Button
                  className={classes.actionButton}
                  presets={['icon', 'tiny']}
                  action={item.action}
                  disabled={item.disabled}
                >
                  settings
                </Button>
              )}
            </div>
          </Tooltip>
        );
      })}
      <AboutButton setDialog={setDialog} folded={folded} />
    </div>
  );
};

export default Info;
