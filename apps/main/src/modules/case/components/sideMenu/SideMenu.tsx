// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import classNames from 'classnames';
//@ts-ignore
import IconButton from '@material-ui/core/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Navigation from './navigation/Navigation';
import Info from './info/Info';
import CaseActionButton from './caseActions/CaseActionButton';
import { useSideMenuStyles } from './SideMenu.style';
import { CaseObjType, DialogsType } from './../../Case.types';

export type SideMenuProps = {
  rootPath: string;
  caseObj: CaseObjType;
  setDialog: (string: DialogsType) => void;
  refreshCaseObj: () => void;
};

const SideMenu: React.FunctionComponent<SideMenuProps> = ({
  rootPath,
  caseObj,
  setDialog,
  refreshCaseObj,
}) => {
  const classes = useSideMenuStyles();
  const [folded, setFolded] = useState<boolean>(false);

  return (
    <div className={classNames(classes.wrapper, { [classes.folded]: folded })}>
      <CaseActionButton
        caseObj={caseObj}
        setDialog={setDialog}
        folded={folded}
      />
      <Navigation rootPath={rootPath} caseObj={caseObj} folded={folded} />
      <Info
        caseObj={caseObj}
        setDialog={setDialog}
        refreshCaseObj={refreshCaseObj}
        folded={folded}
      />
      <IconButton
        className={classes.foldButton}
        onClick={() => {
          setFolded(!folded);
        }}
      >
        <Icon size="tiny">
          {folded ? iconNames.chevron_right : iconNames.chevron_left}
        </Icon>
      </IconButton>
    </div>
  );
};

export default SideMenu;
