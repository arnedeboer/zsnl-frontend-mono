// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';
import { searchObjects } from '../requests';

export const OBJECT_FINDER = 'objectFinder';

export interface ObjectFinderPropsType extends FormFieldPropsType {}

export const ObjectFinder: FormFieldComponentType<
  ValueType<string>,
  { objectTypeName: string }
> = ({ config, name, ...restProps }) => {
  const [input, setInput] = useState('');
  const objectTypeName = config?.objectTypeName;

  return (
    <React.Fragment>
      <DataProvider
        providerArguments={[objectTypeName, input]}
        autoProvide={input !== ''}
        provider={searchObjects}
      >
        {({ data, busy }) => {
          return (
            <Select
              {...restProps}
              name={name}
              choices={data || []}
              getChoices={setInput}
              isClearable={true}
              isSearchable={true}
              loading={busy}
            />
          );
        }}
      </DataProvider>
    </React.Fragment>
  );
};
