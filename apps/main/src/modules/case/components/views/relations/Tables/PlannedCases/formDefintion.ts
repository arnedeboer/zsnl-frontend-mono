// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  hideFields,
  showFields,
  isTruthy,
} from '@zaaksysteem/common/src/components/form/rules';
import { PlannedCaseType } from '../../Relations.types';
import { isOnce } from './library';

/* eslint complexity: [2, 9] */
export const getFormDefinition = (
  t: i18next.TFunction,
  plannedCase?: PlannedCaseType
) => {
  const currentPlannedCase = {
    label: plannedCase?.casetype_title,
    value: plannedCase?.casetype_uuid,
  };
  const periodChoices = ['days', 'weeks', 'months', 'years'].map(value => ({
    value,
    label: t(`plannedCases.values.interval_period.${value}`),
  }));
  const repeatingValue = plannedCase
    ? !isOnce(plannedCase.interval_period)
    : false;
  const period = plannedCase?.interval_period;
  const intervalPeriodValue =
    period && period !== 'once'
      ? periodChoices.find(periodChoice => periodChoice.value === period)
      : periodChoices[0];

  return [
    {
      name: 'casetype',
      type: fieldTypes.CASE_TYPE_FINDER,
      value: plannedCase ? currentPlannedCase : null,
      disabled: Boolean(plannedCase),
      required: true,
      placeholder: t('plannedCases.dialog.placeholders.casetype'),
      label: t('plannedCases.dialog.labels.casetype'),
    },
    ...(plannedCase
      ? []
      : [
          {
            name: 'copy_relations',
            type: fieldTypes.CHECKBOX,
            value: false,
            label: t('plannedCases.dialog.labels.copy_relations'),
            suppressLabel: true,
            applyBackgroundColor: false,
          },
        ]),
    {
      name: 'next_run',
      type: fieldTypes.DATEPICKER,
      value: plannedCase?.next_run || '',
      label: t('plannedCases.dialog.labels.next_run'),
      required: true,
    },
    {
      name: 'repeating',
      type: fieldTypes.CHECKBOX,
      value: repeatingValue,
      label: t('plannedCases.dialog.labels.repeating'),
      suppressLabel: true,
      applyBackgroundColor: false,
    },
    {
      name: 'interval_value',
      type: fieldTypes.NUMERIC,
      value: plannedCase?.interval_value || '',
      label: t('plannedCases.dialog.labels.interval_value'),
      required: true,
    },
    {
      name: 'interval_period',
      type: fieldTypes.SELECT,
      value: intervalPeriodValue,
      clearable: false,
      label: t('plannedCases.dialog.labels.interval_period'),
      required: true,
      choices: periodChoices,
    },
    {
      name: 'runs_left',
      type: fieldTypes.NUMERIC,
      value: plannedCase?.runs_left || '',
      label: t('plannedCases.dialog.labels.runs_left'),
      required: true,
    },
  ];
};

export const getRules = () => [
  new Rule()
    .when('repeating', isTruthy)
    .then(showFields(['interval_value', 'interval_period', 'runs_left']))
    .else(hideFields(['interval_value', 'interval_period', 'runs_left'])),
];
