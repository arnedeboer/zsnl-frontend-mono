// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { CaseRelationType, CaseTablesPropsType } from '../../Relations.types';
import FamilyTable from './FamilyTable';
import RelatedTable from './RelatedTable';
import { getCases, refreshRelatedCases } from './library';

const CaseTables: React.ComponentType<CaseTablesPropsType> = ({
  caseObj,
  caseType,
}) => {
  const [family, setFamily] = useState<CaseRelationType[]>([]);
  const [related, setRelated] = useState<CaseRelationType[]>([]);

  useEffect(() => {
    getCases(caseObj, caseType, setFamily, setRelated);
  }, []);

  return (
    <>
      <FamilyTable cases={family} />
      <RelatedTable
        cases={related}
        caseUuid={caseObj.uuid}
        refresh={() => refreshRelatedCases(caseObj.uuid, setRelated)}
      />
    </>
  );
};

export default CaseTables;
