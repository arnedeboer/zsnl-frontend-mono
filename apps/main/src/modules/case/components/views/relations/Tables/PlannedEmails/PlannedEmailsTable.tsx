// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import { H2 } from '@mintlab/ui/App/Material/Typography';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import {
  PlannedEmailType,
  PlannedEmailsTablePropsType,
} from '../../Relations.types';
import { useRelationsStyles } from '../../Relations.style';
import { getPlannedEmails, rescheduleEmails, formatDate } from './library';

const PlannedEmailsTable: React.ComponentType<PlannedEmailsTablePropsType> = ({
  caseNumber,
  canEdit,
}) => {
  const [t] = useTranslation('caseRelations');
  const classes = useRelationsStyles();
  const tableStyles = useSortableTableStyles();
  const [emails, setPlannedEmails] = useState<PlannedEmailType[]>([]);
  const [saving, setSaving] = useState<boolean>(false);

  useEffect(() => {
    getPlannedEmails(caseNumber, setPlannedEmails);
  }, []);

  const rows = emails;
  const columns = [
    {
      name: 'date',
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: PlannedEmailType }) =>
        formatDate(rowData.date),
    },
    {
      name: 'template',
    },
    {
      name: 'recipient',
    },
  ].map(row => ({
    ...row,
    width: 1,
    flexGrow: 1,
    label: t(`plannedEmails.columns.${row.name}`),
  }));

  return (
    <div className={classes.section}>
      <H2 classes={{ root: classes.header }}>{t('plannedEmails.title')}</H2>
      <div
        style={{ flex: '1 1 auto', height: `calc(${rows.length + 1} * 53px)` }}
      >
        <SortableTable
          rows={rows}
          //@ts-ignore
          columns={columns}
          loading={false}
          rowHeight={53}
          noRowsMessage={t('noRowsMessage')}
          styles={tableStyles}
          sorting="none"
        />
      </div>
      {canEdit && (
        <div className={classes.actionFooter}>
          <Button
            presets={['primary', 'contained']}
            action={async () => {
              setSaving(true);

              await rescheduleEmails(caseNumber, setPlannedEmails);

              setSaving(false);
            }}
            disabled={saving}
          >
            {t('plannedEmails.reschedule')}
          </Button>
        </div>
      )}
    </div>
  );
};

export default PlannedEmailsTable;
