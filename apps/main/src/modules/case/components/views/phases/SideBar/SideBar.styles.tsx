// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useSideBarStyles = makeStyles(
  ({ typography, mintlab: { greyscale }, palette: { primary } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
    },
    buttonWrapper: {
      display: 'flex',
      borderBottom: `1px solid ${greyscale.dark}`,
      padding: 0,
    },
    item: {
      padding: '8px 30px',
      width: 'auto',
    },
    itemSelected: {
      fontWeight: typography.fontWeightBold,
      borderBottom: `1px solid ${primary.main}`,
      '&&': {
        color: primary.main,
        backgroundColor: greyscale.dark,
      },
    },
    icon: {
      minWidth: 40,
    },
    iconSelected: {
      color: primary.main,
      minWidth: 40,
    },
    content: {
      flexGrow: 1,
      display: 'flex',
    },
  })
);

export type ClassesType = ReturnType<typeof useSideBarStyles>;
