// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { CaseObjType, CaseTypeType } from '../../../../../Case.types';

export type CustomFieldType = { type: string; value: any };

export type CustomFieldsType = {
  [key: string]: CustomFieldType;
};

export type ObjectType = {
  uuid: string;
  customFieldsValues: {
    [k: string]: CustomFieldType;
  };
  relatedCasesUuids?: string[];
  status: 'active' | 'inactive';
  authorizations: ('read' | 'readwrite' | 'admin')[];
  [key: string]: any;
};

export type ObjectTypeType = {
  versionUuid: string;
  versionIndependentUuid: string;
  customFieldsDefinition: APICaseManagement.CustomObjectTypeCustomFieldDefinition[];
  catalog_folder_id?: number;
  date_created?: string;
  date_deleted?: string;
  external_reference?: string;
  is_active_version?: boolean;
  last_modified?: string;
  name?: string;
  status?: string;
  title?: string;
  version?: number;
  version_independent_uuid?: string;
  authorizations: ('read' | 'readwrite' | 'admin')[];
};

export type ObjectTypeFormShapeType = {
  [key: string]: any;
};

export type CaseType = APICaseManagement.GetCaseBasicResponseBody;

export type GetObjectTypeAndObject = (
  setObjectType: any,
  objectTypeUuid: string,
  setObject: any,
  objectUuid?: string
) => void;

export type CreateObjectType = (
  objectTypeUuid: string,
  values: { [key: string]: any }
) => void;

export type UpdateCaseType = (
  objectUuid: string,
  result: 'created' | 'updated' | 'deactivated'
) => void;

export type UpdateObjectType = (
  relatedCasesUuids: string[],
  objectUuid: string,
  values: { [key: string]: any }
) => void;

export type DeactivateObjectType = (
  relatedCasesUuids: string[],
  objectUuid: string,
  values: { [key: string]: any }
) => void;

export type MappingType = {
  [key: string]: string;
};

export type GetMapSettingsType = (
  caseType: CaseTypeType,
  attributeId: string
) => MappingType;

export type IsMagicStringType = (value: string) => Boolean;

export type GetSystemValueType = (
  magicString: string,
  caseObj: CaseObjType
) => any;

export type MapCustomFieldsType = (
  caseObj: CaseObjType,
  caseType: CaseTypeType,
  attributeId: string
) => CustomFieldsType;

export type GetIsAllowedSystemAttributesType = (
  session: SessionType,
  objectType: ObjectTypeType
) => boolean;
