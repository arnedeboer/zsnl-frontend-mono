// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';

export const fetchTasks = async (
  caseUuid: string,
  phase: number
): Promise<APICaseManagement.GetTaskListResponseBody> => {
  const url = buildUrl<APICaseManagement.GetTaskListRequestParams>(
    '/api/v2/cm/task/get_task_list',
    {
      filter: {
        'attributes.phase': phase,
        'relationships.case.id': caseUuid,
      },
      page_size: 100,
      page: 1,
    }
  );

  const response = await request<APICaseManagement.GetTaskListResponseBody>(
    'GET',
    url
  );

  return response;
};

export const createTaskRequest = async (
  caseUuid: string,
  task_uuid: string,
  phaseNumber: number,
  title: string
) => {
  const url = buildUrl<any>('/api/v2/cm/task/create', {
    case_uuid: caseUuid,
    phase: phaseNumber,
  });

  const data: APICaseManagement.CreateTaskRequestBody = {
    case_uuid: caseUuid,
    task_uuid,
    phase: phaseNumber,
    title,
  };

  return await request<APICaseManagement.CreateTaskResponseBody>(
    'POST',
    url,
    data
  );
};

export const setTaskCompletionRequest = async (
  task_uuid: string,
  completed: boolean
) => {
  const url = '/api/v2/cm/task/set_completion';

  const data: APICaseManagement.SetTaskCompletionRequestBody = {
    task_uuid,
    completed,
  };

  return await request<APICaseManagement.SetTaskCompletionResponseBody>(
    'POST',
    url,
    data
  );
};

export const updateTaskRequest = async (
  data: APICaseManagement.UpdateTaskRequestBody
) => {
  const url = '/api/v2/cm/task/update';

  return await request<APICaseManagement.UpdateTaskResponseBody>(
    'POST',
    url,
    data
  );
};

export const deleteTaskRequest = async (task_uuid: string) => {
  const url = '/api/v2/cm/task/delete';

  const data: APICaseManagement.DeleteTaskRequestBody = { task_uuid };

  return await request<APICaseManagement.DeleteTaskResponseBody>(
    'POST',
    url,
    data
  );
};
