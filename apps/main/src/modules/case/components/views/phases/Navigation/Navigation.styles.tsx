// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useNavigationStyles = makeStyles(
  ({ typography, mintlab: { greyscale }, palette: { primary } }: Theme) => ({
    wrapper: {
      display: 'flex',
      borderBottom: `1px solid ${greyscale.dark}`,
      padding: 0,
      height: '55px',
    },
    item: {
      justifyContent: 'center',
      padding: '10px 20px',
    },
    itemSmall: {
      padding: '10px 20px',
      width: 'auto',
    },
    itemSelected: {
      fontWeight: typography.fontWeightBold,
      borderBottom: `1px solid ${primary.main}`,
      '&&': {
        color: primary.main,
        backgroundColor: greyscale.dark,
      },
    },
    icon: {
      minWidth: 30,
    },
    iconSelected: {
      color: primary.main,
      minWidth: 30,
    },
    itemLabel: {
      flex: 'none',
    },
  })
);

export type ClassesType = ReturnType<typeof useNavigationStyles>;
