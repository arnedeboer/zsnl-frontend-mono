// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useActionsStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    cursor: 'default',
  },
  placeholder: {
    margin: 20,
  },
  action: {
    display: 'flex',
    alignContent: 'flex-start',
    padding: '10px 20px',
  },
  actionActive: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  checkboxWrapper: {
    width: 40,
  },
  checkbox: {
    pointerEvents: 'none',
  },
  titleWrapper: {
    flexGrow: 1,
    display: 'flex',
    flexWrap: 'nowrap',
    alignItems: 'center',
    gap: 10,
  },
  titleWrapperDisabled: {
    opacity: 0.5,
  },
  title: {
    flexGrow: 1,
  },
}));

export type ClassesType = ReturnType<typeof useActionsStyles>;
