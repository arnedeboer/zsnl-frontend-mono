// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectType } from '../../../../../Case.types';
import {
  GetObjectTypeAndObject,
  UpdateCaseType,
  CreateObjectType,
  UpdateObjectType,
  DeactivateObjectType,
  GetMapSettingsType,
  IsMagicStringType,
  GetSystemValueType,
  MapCustomFieldsType,
  CustomFieldsType,
} from './ObjectForm.types';
import {
  fetchObject,
  createObjectAction,
  updateObjectAction,
  deactivateObjectAction,
  fetchObjectType,
  fetchPersistentObjectType,
} from './ObjectForm.requests';

export const getObjectTypeAndObject: GetObjectTypeAndObject = async (
  setObjectType,
  objectTypeUuid,
  setObject,
  objectUuid
) => {
  const objectTypeFetcher = objectUuid
    ? fetchObjectType
    : fetchPersistentObjectType;
  let objectTypeVersionUuid;

  if (objectUuid) {
    const object = await fetchObject(objectUuid);

    const {
      id,
      attributes: { custom_fields, status },
      relationships,
      meta,
    } = object.data;

    const relatedCasesUuids = relationships?.cases
      ?.map(caseRelation => caseRelation.data.id || '')
      .filter(Boolean);

    setObject({
      // from the case_type call we receive the version_independent_id
      // but for object_create we need the version_id
      uuid: id,
      status,
      customFieldsValues: custom_fields,
      relatedCasesUuids,
      authorizations: meta?.authorizations || [],
    });

    // when creating we want the latest version of the objectType
    // when editing we want the specific version for the object
    objectTypeVersionUuid = relationships?.custom_object_type?.data.id;
  }

  const objectType = await objectTypeFetcher(
    objectTypeVersionUuid || objectTypeUuid
  );

  const {
    id,
    attributes: { custom_field_definition, version_independent_uuid },
    meta,
  } = objectType.data;

  setObjectType({
    versionUuid: id,
    versionIndependentUuid: version_independent_uuid,
    customFieldsDefinition: custom_field_definition?.custom_fields,
    authorizations: meta?.authorizations,
  });
};

const updateCase: UpdateCaseType = (objectUuid, result) => {
  window.top?.postMessage(
    { type: 'handleObjectSuccess', data: { uuid: objectUuid, result } },
    '*'
  );
};

export const createObject: CreateObjectType = async (
  objectTypeUuid,
  values
) => {
  const result = await createObjectAction(objectTypeUuid, values);
  const objectUuid = result.data?.id;

  if (objectUuid) {
    updateCase(objectUuid, 'created');
  }
};

export const updateObject: UpdateObjectType = async (
  relatedCasesUuids,
  objectTypeUuid,
  values
) => {
  const result = await updateObjectAction(
    relatedCasesUuids,
    objectTypeUuid,
    values
  );
  const objectUuid = result.data?.id;

  if (objectUuid) {
    updateCase(objectUuid, 'updated');
  }
};

export const deactivateObject: DeactivateObjectType = async (
  relatedCasesUuids,
  objectTypeUuid,
  values
) => {
  const result = await deactivateObjectAction(
    relatedCasesUuids,
    objectTypeUuid,
    values
  );
  const objectUuid = result.data?.id;

  if (objectUuid) {
    updateCase(objectUuid, 'deactivated');
  }
};

const supportedSystemAttributes = [
  'case.number',
  'case.date_of_registration',
  'case.requestor',
  'case.assignee',
  'case.recipient',
  'case.requestor.coc',
  'case.requestor.establishment_number',
];

const getMapSettings: GetMapSettingsType = (caseType, attributeId) => {
  const fields = caseType.fields;
  const field = fields?.find((field: any) => field?.id === Number(attributeId));

  return field?.properties?.custom_object?.attributes || {};
};

const isMagicString: IsMagicStringType = value => value[0] === '[';

const createSubjectValue = (subject?: SubjectType) => {
  if (!subject) return;

  return {
    value: subject.uuid,
    label: subject.name,
  };
};

const getSystemValue: GetSystemValueType = (magicString, caseObj) => {
  const { assignee, requestor, recipient } = caseObj;

  switch (magicString) {
    case 'case.number': {
      return caseObj.number;
    }
    case 'case.date_of_registration': {
      return caseObj.registrationDate;
    }
    case 'case.requestor': {
      return createSubjectValue(requestor);
    }
    case 'case.assignee': {
      return createSubjectValue(assignee);
    }
    case 'case.recipient': {
      return createSubjectValue(recipient);
    }
    case 'case.requestor.coc': {
      return requestor?.cocNumber;
    }
    case 'case.requestor.establishment_number': {
      return requestor?.cocLocationNumber;
    }
    default: {
      return;
    }
  }
};

/* eslint complexity: [2, 14] */
export const mapCustomFields: MapCustomFieldsType = (
  caseObj,
  caseType,
  attributeId
) => {
  const customFields = caseObj?.customFields;
  const mapSettings = getMapSettings(caseType, attributeId);

  const mappedFields = Object.keys(mapSettings).reduce<CustomFieldsType>(
    (acc, key) => {
      const value = mapSettings[key];
      const strippedValue = value.replace(/\[|\]/g, '');
      const magicString = strippedValue || key;
      const existsInCase = Object.prototype.hasOwnProperty.call(
        customFields,
        magicString
      );

      // map the same attribute
      if (!value && existsInCase)
        return {
          ...acc,
          [key]: customFields[key],
        };

      // no map
      if (!value) return acc;

      // map custom value
      if (!isMagicString(value))
        return {
          ...acc,
          [key]: { value, type: 'unknown' },
        };

      const isSystemAttribute = supportedSystemAttributes.includes(magicString);

      // map system attribute
      if (isSystemAttribute) {
        const systemValue = getSystemValue(magicString, caseObj);

        return {
          ...acc,
          [key]: {
            value: systemValue,
            type: 'systemAttribute',
          },
        };
      }

      // bad map
      if (!existsInCase) return acc;

      // map a case attribute
      const fieldSetting = customFields[magicString];

      return {
        ...acc,
        [key]: fieldSetting,
      };
    },
    {}
  );

  return mappedFields;
};
