// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { CaseObjType, CaseTypeType } from '../../../Case.types';

export const getNavigationItems = (
  pathWithoutPhaseNumber: string,
  caseObj: CaseObjType,
  caseType: CaseTypeType,
  phaseNumber: number
) =>
  caseType.phases.map((phase: any) => {
    const selected = phase.milestone == phaseNumber;
    const href = `${pathWithoutPhaseNumber}/${phase.milestone}`;
    let icon;

    if (Number(phase.milestone < caseObj.phase)) {
      icon = iconNames.done;
    } else if (phase.milestone === caseObj.phase) {
      icon = iconNames.play_arrow;
    }

    return {
      selected,
      href,
      icon,
      label: phase.phase,
      fullWidth: false,
    };
  });

export const getSideBarItems = (
  t: i18next.TFunction,
  rootPath: string,
  activeBar: string,
  phaseNumber: number,
  checkedActionsCount?: number,
  openTasksCount?: number
) => [
  {
    selected: activeBar === 'actions',
    href: `${rootPath}/actions`,
    icon: iconNames.play_circle_filled,
    label: t('actions'),
    count: checkedActionsCount,
  },
  ...(phaseNumber !== 1
    ? [
        {
          selected: activeBar === 'tasks',
          href: `${rootPath}/tasks`,
          icon: iconNames.library_add_check_icon,
          label: t('tasks'),
          count: openTasksCount,
        },
      ]
    : []),
];
