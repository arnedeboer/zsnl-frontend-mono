// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Tasks, { TasksPropsType } from './Tasks';
import locale from './Tasks.locale';

const TasksView: React.ComponentType<TasksPropsType> = props => (
  <I18nResourceBundle resource={locale} namespace="caseTasks">
    <Tasks {...props} />
  </I18nResourceBundle>
);

export default TasksView;
