// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useCaseTimelineStyles = makeStyles(() => ({
  wrapper: {
    height: 'calc(100% - 16px)',
    overflow: 'hidden',
    margin: '16px 16px 0 16px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  buttonV1: {
    height: 37,
  },
  timelineWrapper: {
    height: 'calc(100% - 37px)',
    width: '100%',
  },
}));

export type ClassesType = ReturnType<typeof useCaseTimelineStyles>;
