// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { APICaseManagement } from '@zaaksysteem/generated';
import { caseActionIcons } from './components/sideMenu/caseActions/Caseactions.library';

export type SubjectV2Type = APICaseManagement.GetContactResponseBody;
export type SubjectType = {
  type: SubjectTypeType;
  uuid: string;
  name?: string;
  status: string;
  cocNumber?: string;
  cocLocationNumber?: number;
  isSecret: boolean;
  correspondenceAdress: any;
  dateOfDeath: string | null;
  investigation: string | null;
};
export type SystemRolesType = {
  assignee?: SubjectType;
  requestor?: SubjectType;
  recipient?: SubjectType;
  coordinator?: SubjectType;
};

export type CaseV2Type = APICaseManagement.GetCaseResponseBody;
export type CaseV1Type = any;
export type CaseObjType = {
  uuid: string;
  name: string;
  number: number;
  progress_status: number;
  presetClient: boolean;
  result: string | null;
  resultDescription: string | null;
  status: string;
  caseOpen: boolean;
  canEdit: boolean;
  canManage: boolean;
  hasEditRights: boolean;
  hasManageRights: boolean;
  summary: string;
  htmlEmailTemplateName?: string;
  assignee?: SubjectType;
  requestor?: SubjectType;
  recipient?: SubjectType;
  coordinator?: SubjectType;
  department: string;
  role: string;
  registrationDate: string | null;
  targetDate: string | null;
  completionDate: string | null;
  destructionDate: string | null;
  stalledSinceDate: string | null;
  stalledUntilDate: string | null;
  contactChannel:
    | 'behandelaar'
    | 'balie'
    | 'telefoon'
    | 'post'
    | 'email'
    | 'webformulier'
    | 'sociale media';
  confidentiality: 'public' | 'internal' | 'confidential';
  location?: string;
  payment: {
    amount: string | null;
    status: 'success' | 'failed' | 'pending' | 'offline' | null;
  };
  customFields?: any;
  caseV1?: CaseV1Type;
  caseTypeVersionUuid: string;
  caseTypeUuid: string;
  caseTypeVersion: number;
  milestone: number;
  phase: number;
};

export type LeadTimeType = {
  type?: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
  value?: string | number;
};

export type CaseTypeV2Type =
  APICaseManagement.GetCaseTypeActiveVersionResponseBody;
export type CaseTypeV1Type = any;
export type CaseTypeType = {
  phases: any;
  settings: any;
  name?: string;
  metaData: {
    legalBasis?: string;
    localBasis?: string;
    designationOfConfidentiality?:
      | 'Openbaar'
      | 'Beperkt openbaar'
      | 'Intern'
      | 'Zaakvertrouwelijk'
      | 'Vertrouwelijk'
      | 'Confidentieel'
      | 'Geheim'
      | 'Zeer geheim';
    responsibleRelationship?: string;
    processDescription?: string;
  };
  leadTimeLegal: LeadTimeType;
  leadTimeService: LeadTimeType;
  objectFields?: any;
  fields?: any[];
  results: any[];
};

export type CaseTypeTypeForComponents = Omit<CaseTypeType, 'phaseActions'>;

export type JobTypeV1 = {
  instance: {
    status: 'finished' | 'pending';
  };
};

export type JobType = {
  status: 'finished' | 'pending';
};

export type CaseActionType = keyof typeof caseActionIcons;
export type CaseActionDialogType = CaseActionType;
export type OtherDialogsType = 'about' | 'confidentiality' | 'caseActions';

export type DialogsType = CaseActionType | OtherDialogsType;
