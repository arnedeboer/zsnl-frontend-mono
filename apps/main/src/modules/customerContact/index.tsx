// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import CustomerContact from './components/CustomerContact';

export default function CustomerContactModule() {
  return <CustomerContact />;
}
