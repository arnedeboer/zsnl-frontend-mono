// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { RouteComponentProps } from 'react-router';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import locale from '../case/Case.locale';
import { provideForCase } from '../case/Case.library';
import Routes from './Routes';

const CaseComponentsModule: React.FunctionComponent<
  RouteComponentProps<{
    caseUuid: string;
  }>
> = ({
  match: {
    params: { caseUuid },
  },
}) => (
  <I18nResourceBundle resource={locale} namespace="case">
    <DataProvider
      provider={provideForCase}
      providerArguments={[caseUuid]}
      autoProvide={true}
    >
      {({ data, busy }) => {
        if (busy) {
          return <Loader />;
        }

        return (
          data && <Routes caseObj={data.caseObj} caseType={data.caseType} />
        );
      }}
    </DataProvider>
  </I18nResourceBundle>
);

export default CaseComponentsModule;
