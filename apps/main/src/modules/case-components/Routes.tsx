// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
//@ts-ignore
import { objectifyParams } from '@mintlab/kitchen-sink/source/url';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Relations from '../case/components/views/relations';
import Timeline from '../case/components/views/timeline';
import Communication from '../case/components/views/communication';
import Tasks from '../case/components/views/phases/Tasks';
import ObjectForm from '../case/components/views/phases/Dialogs/ObjectForm';
import { CaseObjType, CaseTypeType } from '../case/Case.types';
import { sessionSelector } from '../case/Case.selectors';
import { refreshTasks } from './library';

export interface RoutesPropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
}

/* eslint complexity: [2, 9] */
const Routes: React.ComponentType<RoutesPropsType> = ({
  caseObj,
  caseType,
}) => {
  const session = useSelector(sessionSelector);

  if (!session) {
    return <Loader />;
  }

  return (
    <React.Fragment>
      <Switch>
        <Route
          path={`/:prefix/case-component/:caseId/communication`}
          render={({ match }) => (
            <Communication
              caseObj={caseObj}
              caseType={caseType}
              rootPath={match.url}
            />
          )}
        />
        <Route
          path={`/:prefix/case-component/:caseUuid/phase/:phase/tasks`}
          render={({
            match: {
              params: { phase },
              url,
            },
          }: any) => (
            <Tasks
              caseUuid={caseObj.uuid}
              phaseNumber={phase}
              rootPath={url}
              setOpenTasksCount={refreshTasks}
              iframe={true}
            />
          )}
        />
        <Route
          path={`/:prefix/case-component/:caseId/relations`}
          render={() => (
            <Relations
              session={session}
              caseObj={caseObj}
              caseType={caseType}
            />
          )}
        />
        <Route
          path={`/:prefix/case-component/:caseUuid/object/:type(create|update)/:objectUuid?`}
          render={({
            history: {
              location: { search },
            },
            match: {
              params: { type, objectUuid },
            },
          }: any) => {
            const [, queryParam] = search.split('?');
            const { objectTypeUuid, attributeId } = objectifyParams(queryParam);

            return (
              <ObjectForm
                type={type}
                session={session}
                caseObj={caseObj}
                caseType={caseType}
                objectTypeUuid={objectTypeUuid}
                objectUuid={objectUuid}
                attributeId={attributeId}
              />
            );
          }}
        />
        <Route
          path={`/:prefix/case-component/:caseUuid/timeline`}
          render={() => {
            return <Timeline caseObj={caseObj} />;
          }}
        />
      </Switch>
    </React.Fragment>
  );
};

export default Routes;
