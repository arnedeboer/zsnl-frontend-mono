// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ItemValueType } from '@zaaksysteem/common/src/components/form/fields/Options/Options.types';
import { FilterType } from './Tasks/types/Tasks.types';

type PostMessageArgumentsType = {
  widgetUuid: string;
  filters: FilterType[];
  columns: ItemValueType[];
};

export const updateWidgetPostMessage = ({
  widgetUuid,
  filters,
  columns,
}: PostMessageArgumentsType) => {
  window.top?.postMessage(
    {
      type: 'updateWidget',
      data: { widgetUuid, settings: { filters, columns } },
    },
    window.location.origin
  );
};

export const deleteWidgetPostMessage = (widgetUuid: string) => {
  window.top?.postMessage(
    { type: 'deleteWidget', data: { widgetUuid } },
    window.location.origin
  );
};
