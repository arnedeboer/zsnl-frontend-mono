// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

const widgetHeader = {
  'white-space': 'nowrap',
  alignItems: 'center',
  background: '#fff',
  borderBottom: '1px solid #e5e5e5',
  borderRadius: '3px 3px 0 0',
  display: 'flex',
  height: '4rem',
  minWidth: 0,
  padding: '1rem .67rem 1rem 1.33rem',
  width: '100%',
};

export const useWidgetStyles = makeStyles({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    minHeight: '500px',
    width: '100%',
  },
});

export const useWidgetHeaderStyles = makeStyles({
  widgetHeader: {
    ...widgetHeader,
  },
  title: {
    alignItems: 'center',
    display: 'flex',
    fontSize: '1.35rem',
    paddingRight: 10,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  flexibleSpace: {
    marginLeft: 'auto',
  },
  delete: {
    borderRadius: '100%',
    flexShrink: 0,
    height: 40,
    lineHeight: 40,
    textAlign: 'center',
    width: 40,
  },
});

export const useWidgetSearchStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    overlay: {
      ...widgetHeader,
      left: 0,
      position: 'absolute',
      top: 0,
      zIndex: 1,
    },
    formControl: {
      background: 'transparent',
      borderRadius: 0,
      paddingLeft: 0,
    },
    focus: {
      background: 'transparent',
      color: greyscale.black,
    },
  })
);

export const useWidgetFilterStyles = makeStyles(
  ({ mintlab: { greyscale, shadows } }: Theme) => ({
    open: {
      background: greyscale.dark,
      borderRadius: 20,
      boxShadow: shadows.sharp,
      color: greyscale.darkest,
      marginRight: 'auto',
      padding: '0 10px',
    },
    overlay: {
      background: greyscale.lighter,
      height: '100%',
      left: 0,
      position: 'absolute',
      top: 0,
      width: '100%',
      zIndex: 1,
    },
    deleteLink: {
      color: greyscale.darkest,
      fontWeight: 'normal',
      '&:hover': {
        background: 'transparent',
      },
    },
  })
);
