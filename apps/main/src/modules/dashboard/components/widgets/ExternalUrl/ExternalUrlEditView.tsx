// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';

import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import {
  TEXT,
  WEB_ADDRESS,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { Button } from '@mintlab/ui/App/Material/Button';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { TFunction } from 'i18next';

const ExternalUrlEditView = ({
  t,
  submit,
}: {
  t: TFunction;
  submit: (
    values: {
      title: any;
    } & {
      url: any;
    }
  ) => void;
}) => {
  const {
    fields,
    formik: { isValid, values },
  } = useForm({
    formDefinition: [
      {
        name: 'title',
        type: TEXT,
        required: true,
        label: t('externalUrl:form.title'),
        readOnly: false,
        value: null,
      },
      {
        name: 'url',
        type: WEB_ADDRESS,
        required: true,
        label: t('externalUrl:form.url'),
        readOnly: false,
        value: null,
      },
    ],
  });
  return (
    <>
      {fields.map(({ FieldComponent, name, value, ...rest }) => {
        return (
          <FormControlWrapper {...rest} compact={true} key={name}>
            <FieldComponent name={name} value={value} key={name} {...rest} />
          </FormControlWrapper>
        );
      })}
      <Button action={() => submit(values)} disabled={!isValid}>
        {t('externalUrl:form.save')}
      </Button>
    </>
  );
};

export default ExternalUrlEditView;
