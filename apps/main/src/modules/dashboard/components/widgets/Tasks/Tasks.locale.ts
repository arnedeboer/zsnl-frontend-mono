// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    title: 'Taken ({{counter}})',
    columns: {
      display_number: {
        label: 'Zaaknummer',
      },
      title: {
        label: 'Taak',
      },
      display_name: {
        label: 'Behandelaar',
      },
      due_date: {
        label: 'Deadline',
      },
      days_left: {
        label: 'Dagen',
      },
    },
    columnsSettings: 'Kolommen instellen',
    no_rows: 'Er zijn geen taken.',
    noRowsFilters: 'Er zijn geen taken met de opgegeven filters.',
  },
};

export default locale;
