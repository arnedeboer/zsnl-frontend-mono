// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { H2 } from '@mintlab/ui/App/Material/Typography';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Button from '@mintlab/ui/App/Material/Button';
import generateCustomFieldFormDefinition from '@zaaksysteem/common/src/components/form/library/generateCustomFieldFormDefinition';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import {
  Rule,
  updateFields,
  isTruthy,
} from '@zaaksysteem/common/src/components/form/rules';
import generateCustomFieldValues from '@zaaksysteem/common/src/components/form/library/generateCustomFieldValues';
import { isAdminSelector } from '@zaaksysteem/common/src/store/session/session.selectors';
import { PanelLayout } from '../../../../components/PanelLayout/PanelLayout';
import { Panel } from '../../../../components/PanelLayout/Panel';
import { ObjectType, ObjectTypeType } from '../../ObjectView.types';
import { updateObjectAction, activateObject } from './../../ObjectView.library';
import { useAttributesStyles } from './Attributes.style';
import { getSystemAttributesFormDefinition } from './systemAttributes.formDefinition';

export type AttributeType = {
  type: string;
  name: string;
  value: string;
  label?: string;
  description?: string;
};

export type AttributesPropsType = {
  object: ObjectType;
  objectType: ObjectTypeType;
  refreshObject: () => void;
};

/* eslint complexity: [2, 11] */
const Attributes: React.FunctionComponent<AttributesPropsType> = ({
  object,
  objectType,
  refreshObject,
}) => {
  const classes = useAttributesStyles();
  const [t] = useTranslation(['objectView']);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const isAdmin = useSelector(isAdminSelector);
  const isObjectTypeAdmin = objectType.authorizations.includes('admin');
  const isAllowedSystemAttributes = isAdmin || isObjectTypeAdmin;

  const customAttributesFormDefinition = generateCustomFieldFormDefinition({
    customFieldsDefinition: objectType.customFieldsDefinition,
    customFieldsValues: object.customFieldsValues,
    readOnly: true,
    t,
    config: {
      context: {
        type: 'ObjectView' as const,
        data: {
          magic_string: '',
          objectType,
          object,
        },
      },
    },
  });

  customAttributesFormDefinition.forEach(field => {
    field.config = {
      ...field.config,
      context: {
        type: 'ObjectView' as const,
        data: {
          ...field.config.context.data,
          magic_string: field.name,
        },
      },
    };
    if (isAllowedSystemAttributes) {
      field.hidden = false;
    }
  });

  const fieldNames = customAttributesFormDefinition.map(field => field.name);
  const rules = [
    new Rule<any>()
      .when('readOnly', isTruthy)
      .then(fields =>
        fields.map(
          updateFields(fieldNames, {
            readOnly: true,
          })
        )
      )
      .else(fields =>
        fields.map(
          updateFields(fieldNames, {
            readOnly: false,
          })
        )
      ),
  ];

  const customAttributesReadOnlyStatus = [
    {
      name: 'readOnly',
      type: fieldTypes.CHECKBOX,
      value: true,
      hidden: true,
    },
  ];

  const {
    fields: customFields,
    formik: { values: customValues, setValues: setCustomValues },
  } = useForm({
    rules,
    formDefinition: [
      ...customAttributesFormDefinition,
      ...customAttributesReadOnlyStatus,
    ],
  });

  const readOnly = customValues.readOnly;
  const toggleReadOnly = () =>
    setCustomValues({ ...customValues, readOnly: !readOnly });

  const systemAttributesFormDefinition = getSystemAttributesFormDefinition(
    t,
    object
  );

  const {
    fields: systemFields,
    formik: { values: systemValues },
  } = useForm({ formDefinition: systemAttributesFormDefinition });

  if (!object || !objectType) {
    return <Loader />;
  }

  const canActivate =
    object.status === 'inactive' && object.authorizations.includes('admin');

  return (
    <PanelLayout>
      {ServerErrorDialog}
      <Panel className={classes.wrapper}>
        <div className={classes.header}>
          <H2 classes={{ root: classes.headerTitle }}>
            {t('attributes.title')}
          </H2>
          <div className={classes.buttons}>
            {readOnly && canActivate && (
              <Button
                action={() => {
                  activateObject(
                    object.relatedCasesUuids || [],
                    object.uuid,
                    generateCustomFieldValues(customValues, customFields)
                  )
                    .then(refreshObject)
                    .catch(openServerErrorDialog);
                }}
                presets={['contained']}
                className={classes.button}
              >
                {t('attributes.actions.activate')}
              </Button>
            )}
            {readOnly && object.status !== 'inactive' && (
              <Button
                action={() => {
                  toggleReadOnly();
                }}
                presets={['contained']}
                className={classes.button}
              >
                {t('attributes.actions.edit')}
              </Button>
            )}
            {!readOnly && (
              <Button
                action={() => {
                  toggleReadOnly();
                }}
                presets={['contained']}
                className={classes.button}
              >
                {t('attributes.actions.cancel')}
              </Button>
            )}
            {!readOnly && (
              <Button
                action={() => {
                  updateObjectAction(
                    object.relatedCasesUuids || [],
                    object.uuid,
                    generateCustomFieldValues(
                      customValues,
                      customAttributesFormDefinition
                    )
                  )
                    .then(() => {
                      refreshObject();
                      toggleReadOnly();
                    })
                    .catch(openServerErrorDialog);
                }}
                presets={['contained']}
                className={classes.button}
              >
                {t('attributes.actions.save')}
              </Button>
            )}
          </div>
        </div>
        <SubHeader
          title={t('attributes.customAttributes.title')}
          description={t('attributes.customAttributes.description')}
        />
        <div className={classes.formWrapper}>
          {customFields.map(
            ({ FieldComponent, name, error, touched, value, ...rest }) => {
              const restValues = {
                ...cloneWithout(rest, 'type', 'classes'),
                disabled: customValues.completed,
              };

              return (
                <FormControlWrapper
                  {...restValues}
                  error={error}
                  touched={touched}
                  key={name}
                  contentColumnClassName={classes.colContent}
                >
                  <FieldComponent
                    name={name}
                    value={value}
                    key={name}
                    {...restValues}
                  />
                </FormControlWrapper>
              );
            }
          )}
        </div>
        <SubHeader
          title={t('attributes.systemAttributes.title')}
          description={t('attributes.systemAttributes.description')}
        />
        <div className={classes.formWrapper}>
          {systemFields.map(
            ({ FieldComponent, name, error, touched, value, ...rest }) => {
              const restValues = {
                ...cloneWithout(rest, 'type', 'classes'),
                disabled: systemValues.completed,
              };

              return (
                <FormControlWrapper
                  {...restValues}
                  error={error}
                  touched={touched}
                  key={name}
                >
                  <FieldComponent
                    name={name}
                    value={value}
                    key={name}
                    {...restValues}
                  />
                </FormControlWrapper>
              );
            }
          )}
        </div>
      </Panel>
      <Panel className={classes.sidePanel} type="side" />
    </PanelLayout>
  );
};

export default Attributes;
