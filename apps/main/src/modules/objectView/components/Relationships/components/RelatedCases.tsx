// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useTranslation } from 'react-i18next';
import { H4 } from '@mintlab/ui/App/Material/Typography';
import { RelatedCaseRowType } from '../../../ObjectView.types';
import { getCasesColumns } from '../Relationships.library';

type RelatedCasesPropsType = {
  cases: RelatedCaseRowType[];
};

const RelatedCases: React.FunctionComponent<RelatedCasesPropsType> = ({
  cases,
}) => {
  const [t] = useTranslation('main');

  return (
    <div>
      <div
        style={{
          minHeight: 100,
          height: 25 + 50 + 50 * cases.length,
        }}
      >
        <H4>{t('objectView:relationships.cases.title')}</H4>
        <SortableTable
          rows={cases}
          columns={getCasesColumns({ t })}
          noRowsMessage={t('common:general.noResults')}
          loading={false}
          rowHeight={50}
          onRowClick={({ rowData }: { rowData: RelatedCaseRowType }) =>
            window.open(`/intern/zaak/${rowData.nr}`, '_blank')
          }
        />
      </div>
    </div>
  );
};

export default RelatedCases;
