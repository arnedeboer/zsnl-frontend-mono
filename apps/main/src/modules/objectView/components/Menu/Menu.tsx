// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Route } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { SideMenu } from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { getItems } from './Menu.library';
import { useSideMenuStyles } from './Menu.styles';

export type MenuPropsType = {
  rootPath: string;
};

const Menu: React.ComponentType<MenuPropsType> = ({ rootPath }) => {
  const [t] = useTranslation('objectView');
  const classes = useSideMenuStyles();

  return (
    <Route
      path={`${rootPath}/:view(attributes|map|relationships|timeline)`}
      render={({ match }) => {
        // @ts-ignore
        const items = getItems(t, rootPath, match.params.view);

        return (
          <div className={classes.wrapper}>
            <SideMenu items={items} />
          </div>
        );
      }}
    />
  );
};

export default Menu;
