// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => {
  return {
    wrapper: {
      padding: `30px 30px 0 30px`,
      height: '100%',
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    mainTitle: {
      marginBottom: 30,
    },
    timelineWrapper: {
      flexGrow: 1,
    },
  };
});
