// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { H2 } from '@mintlab/ui/App/Material/Typography';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import Timeline from '../../../../../../../packages/common/src/components/Timeline/Timeline';
import { useStyles } from './Timeline.styles';
import { getData } from './Timeline.library';

type ObjectViewTimelineProps = {
  uuid: string;
};

const ObjectViewTimeline: React.FunctionComponent<ObjectViewTimelineProps> = ({
  uuid,
}) => {
  const classes = useStyles();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const getDataFunction = getData({ openServerErrorDialog, uuid });
  const [t] = useTranslation('objectView');

  return (
    <div className={classes.wrapper}>
      <H2
        classes={{
          root: classes.mainTitle,
        }}
      >
        {t('timeline.title')}
      </H2>

      <div className={classes.timelineWrapper}>
        <Timeline getData={getDataFunction} />
      </div>
      {ServerErrorDialog}
    </div>
  );
};

export default ObjectViewTimeline;
