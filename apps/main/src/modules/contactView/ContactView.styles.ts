// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles(({ mintlab: { greyscale } }: Theme) => ({
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexFlow: 'column',
  },
  content: {
    flexGrow: 1,
    height: 1,
  },
  sideMenuPanel: {
    padding: 12,
  },
  viewWrapper: {
    display: 'flex',
    flexDirection: 'column',
  },
  viewToolbar: {
    padding: '20px',
    borderBottom: `1px solid ${greyscale.dark}`,
  },
  view: {
    flexGrow: 1,
  },
}));
