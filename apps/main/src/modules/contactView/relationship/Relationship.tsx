// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import locale from '../../objectView/ObjectView.locale';
import RelatedSubjects from './../../objectView/components/Relationships/components/RelatedSubjects';
import RelatedObjects from './../../objectView/components/Relationships/components/RelatedObjects';
import { fetchRelationTables } from './Relationship.actions';
import { RelationPropTypes } from './Relationship.types';
import { useRelationshipStyles } from './Relationship.style';

export const Relationship: React.FunctionComponent<RelationPropTypes> = ({
  uuid,
}) => {
  const classes = useRelationshipStyles();

  return (
    <div className={classes.wrapper}>
      <I18nResourceBundle resource={locale} namespace="objectView">
        <DataProvider
          provider={fetchRelationTables}
          providerArguments={[uuid]}
          autoProvide={true}
        >
          {({ data, busy }) => {
            if (busy) {
              return <Loader />;
            }

            return (
              data && (
                <>
                  <RelatedSubjects subjects={data.subjects} />
                  <RelatedObjects objects={data.objects} />
                </>
              )
            );
          }}
        </DataProvider>
      </I18nResourceBundle>
    </div>
  );
};

export default Relationship;
