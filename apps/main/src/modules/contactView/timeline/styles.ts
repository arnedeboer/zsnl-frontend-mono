// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles((theme: Theme) => {
  return {
    wrapper: {
      padding: '30px 30px 0 30px',
      height: '100%',
      width: '100%',
    },
    mainTitle: {
      marginBottom: 30,
    },
  };
});
