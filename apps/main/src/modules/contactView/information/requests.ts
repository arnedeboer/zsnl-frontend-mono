// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { FormikValues } from 'formik';

export const checkHasPhoneIntegration = async () => {
  const result = await request(
    'GET',
    '/api/v1/sysin/interface/get_by_module_name/kcc'
  );

  return result.result.instance.pager.rows === 1;
};

export const fetchRelatedObject = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectRequestParams>(
    '/api/v2/cm/custom_object/get_custom_object',
    {
      uuid,
    }
  );
  const result = await request('GET', url).catch(() => {
    // for when the user is given the uuid of the related object in the get_contact call
    // but is not allowed to know anything about the object
    return null;
  });

  return result?.data;
};

export const fetchObjectType = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
    `/api/v2/cm/custom_object_type/get_custom_object_type`,
    {
      uuid,
    }
  );
  const result = await request('GET', url);

  return result.data || [];
};

export const fetchSubjectSettings = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetContactSettingsRequestParams>(
    `/api/v2/cm/contact/get_settings`,
    {
      uuid,
    }
  );
  const result =
    await request<APICaseManagement.GetContactSettingsResponseBody>('GET', url);

  return result.data || [];
};

/* eslint complexity: [2, 8] */
export const saveAdditionalInfo = async (
  uuid: string,
  values: FormikValues,
  type: string
) => {
  const url = '/api/v2/cm/contact/save_contact_info';

  const result =
    await request<APICaseManagement.ChangePhoneExtensionResponseBody>(
      'POST',
      url,
      {
        uuid,
        type,
        contact_information: {
          phone_number: values.phoneNumber || '',
          mobile_number: values.mobileNumber || '',
          email: values.email || '',
          preferred_contact_channel: values.preferredContactChannel || '',
          internal_note: values.internalNote || '',
          is_an_anonymous_contact_person: values.anonymousUser || false,
        },
      }
    );

  return result;
};

export const savePhoneExtension = async (uuid: string, extension: string) => {
  const url = '/api/v2/cm/contact/change_phone_extension';

  const result =
    await request<APICaseManagement.ChangePhoneExtensionResponseBody>(
      'POST',
      url,
      {
        uuid,
        extension,
      }
    );

  return result;
};

export const uploadSignatureAction = async (
  subjectUuid: string,
  fileUuid: string
) => {
  const url = '/api/v2/cm/contact/save_signature';

  const result = await request<APICaseManagement.SaveSignatureRequestBody>(
    'POST',
    url,
    {
      uuid: subjectUuid,
      file_uuid: fileUuid,
    }
  );

  return result;
};

export const deleteSignatureAction = async (uuid: string) => {
  const url = '/api/v2/cm/contact/delete_signature';

  const result = await request<APICaseManagement.DeleteSignatureRequestBody>(
    'POST',
    url,
    {
      uuid,
    }
  );

  return result;
};

export const saveNotificationSettings = async (
  uuid: string,
  settings: FormikValues
) => {
  const url = '/api/v2/cm/contact/set_notification_settings';

  const result =
    await request<APICaseManagement.SetNotificationSettingsRequestBody>(
      'POST',
      url,
      {
        uuid,
        notification_settings: {
          new_document: Boolean(settings.newDocument),
          new_assigned_case: Boolean(settings.newAssignedCase),
          case_term_exceeded: Boolean(settings.caseTermExceeded),
          new_ext_pip_message: Boolean(settings.newExtPipMessage),
          new_attribute_proposal: Boolean(settings.newAttributeProposal),
          case_suspension_term_exceeded: Boolean(
            settings.caseSuspensionTermExceeded
          ),
        },
      }
    );

  return result;
};

type AltAuthResponseBodyType = {
  result: {
    id: number;
    type: SubjectTypeType;
    is_active: boolean;
    username: string;
    phone_number: string;
  }[];
};

export const fetchAltAuthIntegration = async () => {
  const result = await request(
    'GET',
    '/api/v1/sysin/interface/get_by_module_name/auth_twofactor'
  );

  return result.result.instance.rows[0].instance;
};

export const fetchAltAuth = async (uuid: string) => {
  const result: AltAuthResponseBodyType = await request(
    'GET',
    `/betrokkene/get_alternative_auth/${uuid}`
  );

  return result.result[0];
};

export const saveAltAuth = async (id: number, data: any) =>
  await request(
    'POST',
    `/betrokkene/generate_alternative_authentication/${id}`,
    data
  );

export const sendAltAuth = async (id: number, data: any) =>
  await request('POST', `/betrokkene/send_activation_link/${id}`, data);

/* eslint complexity: [2, 20] */
export const saveCommonValues = async (
  t: i18next.TFunction,
  uuid: string,
  values: FormikValues,
  type: string
) => {
  let body: any = {};

  body.type = type;
  body.uuid = uuid;

  const country = values.country?.label ? values.country.label : values.country;
  const correspondenceCountry = values.correspondenceCountry?.label
    ? values.correspondenceCountry.label
    : values.correspondenceCountry;

  if (type === 'person') {
    body.data = {
      first_name: values.firstNames,
      family_name: values.familyName,
      surname_prefix: values.insertions,
      noble_title: values.nobleTitle,
      inside_municipality:
        values.insideMunicipality?.toLowerCase() ===
        t(`special.insideMunicipalityValue.yes`)?.toLowerCase()
          ? true
          : false,
      gender: values.gender,
      mailing_address: false,
    };
    if (values.personalNumber) body.data['bsn'] = values.personalNumber;
    body.address =
      country.toLowerCase() === 'nederland'
        ? {
            street: values.residenceStreet,
            street_number: values.residenceHouseNumber,
            city: values.residenceCity,
            zipcode: values.residenceZipcode,
            country,
            street_number_letter: values.residenceHouseNumberLetter || null,
            street_number_suffix: values.residenceHouseNumberSuffix,
          }
        : {
            address_line_1: values.foreignAddress1,
            address_line_2: values.foreignAddress2,
            address_line_3: values.foreignAddress3,
            country: country,
          };

    if (
      values.hasCorrespondenceAddress?.toLowerCase() ===
      t(`common:yes`)?.toLowerCase()
    ) {
      body.correspondence_address =
        correspondenceCountry?.toLowerCase() === 'nederland'
          ? {
              street: values.correspondenceStreet,
              street_number: values.correspondenceHouseNumber,
              street_number_letter:
                values.correspondenceHouseNumberLetter || null,
              street_number_suffix: values.correspondenceHouseNumberSuffix,
              city: values.correspondenceCity,
              zipcode: values.correspondenceZipcode,
              country,
            }
          : {
              address_line_1: values.correspondenceForeignAddress1,
              address_line_2: values.correspondenceForeignAddress2,
              address_line_3: values.correspondenceForeignAddress3,
              country: correspondenceCountry,
            };
    }
  } else if (type === 'organization') {
    body.data = {
      name: values.name,
      coc_number: values.cocNumber,
      coc_location_number: values.cocLocationNumber,
      organization_type: values.organizationType,
      contact_last_name: values.contactFamilyName,
      contact_first_name: values.contactFirstName,
      contact_insertions: values.contactInsertions,
    };

    const locationCountry = values.locationCountry?.label
      ? values.locationCountry.label
      : values.locationCountry;
    const correspondenceCountry = values.correspondenceCountry?.label
      ? values.correspondenceCountry.label
      : values.correspondenceCountry;

    body.address =
      locationCountry?.toLowerCase() === 'nederland'
        ? {
            street: values.locationStreet,
            zipcode: values.locationZipcode,
            street_number: values.locationHouseNumber,
            street_number_letter: values.locationHouseNumberLetter || null,
            street_number_suffix: values.locationHouseNumberSuffix,
            city: values.locationCity,
            country: locationCountry,
          }
        : {
            address_line_1: values.locationForeignAddress1,
            address_line_2: values.locationForeignAddress2,
            address_line_3: values.locationForeignAddress3,
            country: locationCountry,
          };

    if (
      values.hasCorrespondenceAddress?.toLowerCase() ===
      t(`common:yes`).toLowerCase()
    ) {
      body.correspondence_address =
        correspondenceCountry?.toLowerCase() === 'nederland'
          ? {
              street: values.correspondenceStreet,
              street_number: values.correspondenceHouseNumber,
              street_number_letter:
                values.correspondenceHouseNumberLetter || null,
              street_number_suffix: values.correspondenceHouseNumberSuffix,
              city: values.correspondenceCity,
              zipcode: values.correspondenceZipcode,
              country: correspondenceCountry,
            }
          : {
              address_line_1: values.correspondenceForeignAddress1,
              address_line_2: values.correspondenceForeignAddress2,
              address_line_3: values.correspondenceForeignAddress3,
              country: correspondenceCountry,
            };
    }
  }

  return await request<APICaseManagement.UpdateNonAuthenticContactRequestBody>(
    'POST',
    '/api/v2/cm/contact/update_non_authentic_contact',
    body
  );
};
