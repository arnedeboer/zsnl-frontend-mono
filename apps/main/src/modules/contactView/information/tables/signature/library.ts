// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { SessionType } from '../../../ContactView.types';

type CanUploadType = (
  loggedInUserRoles: SessionType['loggedInUserRoles'],
  signatureUploadRole: SessionType['signatureUploadRole'],
  lookingAtSelf: boolean
) => boolean;

export const canUpload: CanUploadType = (
  loggedInUserRoles,
  signatureUploadRole,
  lookingAtSelf
) => {
  const isAdmin = loggedInUserRoles.includes('Administrator');
  const isZsBeheerder = loggedInUserRoles.includes('Zaaksysteembeheerder');

  return (
    isAdmin ||
    (signatureUploadRole === 'Zaaksysteembeheerder' && isZsBeheerder) ||
    (signatureUploadRole === 'Behandelaar' && lookingAtSelf)
  );
};

type FormatSubtitleType = (
  t: i18next.TFunction,
  canEdit: boolean,
  signatureUploadRole: SessionType['signatureUploadRole']
) => string;

export const formatSubtitle: FormatSubtitleType = (
  t,
  canEdit,
  signatureUploadRole
) => {
  if (canEdit) {
    return t('signature.subTitle.allowed');
  }

  const roleForEditing = t(`signature.roleForEditing.${signatureUploadRole}`);

  return t('signature.subTitle.notAllowed', { roleForEditing });
};
