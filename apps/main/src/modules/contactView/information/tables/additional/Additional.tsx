// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { GetFormDefinitionType } from '../../Information.types';
import { saveAdditionalInfo } from '../../requests';
import { useInformationStyles } from '../../Information.style';
import { hasEditRightsFromState } from '../../../library';
import { SessionType, SubjectType } from './../../../ContactView.types';

export interface AdditionalPropsType {
  subject: SubjectType;
  getFormDefinition: GetFormDefinitionType;
  refreshSubject: () => {};
  setSnackOpen: any;
  session: SessionType;
}

const Additional: React.FunctionComponent<AdditionalPropsType> = ({
  subject,
  getFormDefinition,
  refreshSubject,
  setSnackOpen,
  session,
}) => {
  const hasEditRights = useSelector(hasEditRightsFromState);
  const contactChannelEnabled = session.contactChannelEnabled;
  const [t] = useTranslation('contactView');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useInformationStyles();
  const formDefinition = getFormDefinition({
    t,
    subject,
    hasEditRights,
    contactChannelEnabled,
  });
  const validationMap = generateValidationMap(formDefinition);
  const [busy, setBusy] = useState(false);
  const {
    fields,
    formik: { values },
  } = useForm({
    formDefinition,
    validationMap,
  });

  return (
    <>
      {ServerErrorDialog}
      <SubHeader
        title={t('additional.title')}
        description={t('additional.subTitle')}
      />
      <div className={classes.formWrapper}>
        {fields.map(
          ({
            FieldComponent,
            name,
            error,
            touched,
            defaultValue,
            value,
            ...rest
          }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value || defaultValue || value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
        {(hasEditRights || (!hasEditRights && !subject.anonymousUser)) && (
          <Button
            action={() => {
              setBusy(true);
              saveAdditionalInfo(subject.uuid, values, subject.type)
                .then(() => {
                  refreshSubject();
                  setSnackOpen(true);
                })
                .catch(openServerErrorDialog)
                .finally(() => setBusy(false));
            }}
            className={classes.button}
            presets={['contained', 'primary']}
            disabled={busy}
          >
            {t('save')}
          </Button>
        )}
      </div>
    </>
  );
};

export default Additional;
