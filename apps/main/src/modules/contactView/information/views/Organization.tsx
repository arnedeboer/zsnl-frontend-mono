// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  AltAuthDataType,
  ObjectType,
  ObjectTypeType,
} from '../Information.types';
import Common from '../tables/common/Common';
import Additional from '../tables/additional/Additional';
import RelatedObject from '../tables/relatedObject/RelatedObject';
import AltAuth from '../tables/altAuth/AltAuth';
import Special from '../tables/special/Special';
import {
  commonRules,
  getCommonFormDefinition,
} from '../tables/common/common.formDefinition.organization';
import { getAdditionalFormDefinition } from '../tables/additional/additional.formDefinition.organization';
import { getSpecialFormDefinition } from '../tables/special/special.formDefinition.organization';
import { SessionType, SubjectType } from './../../ContactView.types';

export interface OrganizationViewPropsType {
  data: {
    subject: SubjectType;
    object: ObjectType;
    objectType: ObjectTypeType;
    altAuthData: AltAuthDataType;
  };
  refreshSubject: () => {};
  setSnackOpen: any;
  session: SessionType;
}

const OrganizationView: React.FunctionComponent<OrganizationViewPropsType> = ({
  data: { subject, object, objectType, altAuthData },
  refreshSubject,
  setSnackOpen,
  session,
}) => (
  <>
    <Common
      subject={subject}
      rules={commonRules}
      getFormDefinition={getCommonFormDefinition}
      refreshSubject={refreshSubject}
      setSnackOpen={setSnackOpen}
    />
    <Additional
      subject={subject}
      getFormDefinition={getAdditionalFormDefinition}
      refreshSubject={refreshSubject}
      setSnackOpen={setSnackOpen}
      session={session}
    />
    {altAuthData && <AltAuth altAuthData={altAuthData} />}
    {object && <RelatedObject object={object} objectType={objectType} />}
    <Special subject={subject} getFormDefinition={getSpecialFormDefinition} />
  </>
);

export default OrganizationView;
