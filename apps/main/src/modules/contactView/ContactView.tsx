// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import { useTranslation } from 'react-i18next';
import { Route, Switch, Redirect } from 'react-router-dom';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { H6 } from '@mintlab/ui/App/Material/Typography';
import { PanelLayout } from '../../components/PanelLayout/PanelLayout';
import { Panel } from '../../components/PanelLayout/Panel';
import BreadcrumbBar, {
  BreadcrumbBarPropsType,
} from '../../components/BreadcrumbBar/BreadcrumbBar';
import NotificationBar from '../../components/NotificationBar/NotificationBar';
import { createPath } from '../../library/createPath';
import Information from './information/Information';
import Communication from './communication/Communication';
import LocationModule from './location/Location';
import Relationship from './relationship/Relationship';
import Cases from './cases/Cases';
import { ContactViewSideMenu } from './ContactViewSideMenu';
import { useStyles } from './ContactView.styles';
import ContactTimeline from './timeline';
import {
  formatBreadcrumbLabel,
  formatToolbarContent,
  getNotifications,
} from './library';
import { SubjectType, SessionType } from './ContactView.types';
import { fetchSubject, fetchSession } from './requests';

export interface ContactViewPropsType {
  prefix: string;
  match: RouteComponentProps<{
    type: SubjectTypeType;
    uuid: string;
    selectedItem?:
      | 'data'
      | 'communication'
      | 'cases'
      | 'map'
      | 'relationships'
      | 'timeline';
  }>['match'];
}

const ContactView: React.FunctionComponent<ContactViewPropsType> = ({
  prefix,
  match: {
    params: { type, uuid, selectedItem },
  },
}) => {
  const classes = useStyles();
  const [t] = useTranslation('contactView');
  const [subject, setSubject] = useState<SubjectType>();
  const [session, setSession] = useState<SessionType>();

  const getSubject = async () => {
    const subj = await fetchSubject(uuid, type);

    setSubject(subj);
  };

  const getSession = async () => {
    const subj = await fetchSession();

    setSession(subj);
  };

  useEffect(() => {
    getSubject();
    getSession();
  }, []);

  if (!subject || !session) {
    return <Loader />;
  }

  const baseRoute = `${prefix}/contact-view/:type(person|organization|employee)/:uuid`;
  const path = createPath(`${prefix}/contact-view/${type}/${uuid}`)();

  const breadcrumbs: BreadcrumbBarPropsType['breadcrumbs'] = [
    {
      label: t('main:modules.dashboard'),
      path: '/intern',
    },
    { label: formatBreadcrumbLabel(subject), path: path },
  ];

  return (
    <div className={classes.wrapper}>
      <BreadcrumbBar breadcrumbs={breadcrumbs} />
      <NotificationBar notifications={getNotifications(t, subject)} />
      <div className={classes.content}>
        <PanelLayout>
          <Panel type="side">
            <ContactViewSideMenu
              basePath={path}
              uuid={uuid}
              selectedItem={selectedItem}
            />
          </Panel>
          <Panel className={classes.viewWrapper}>
            <div className={classes.viewToolbar}>
              <H6>{formatToolbarContent(subject)}</H6>
            </div>
            <div className={classes.view}>
              <Switch>
                <Route
                  exact
                  path={`${baseRoute}/`}
                  render={() => <Redirect to={`${path}/data`} />}
                />
                <Route
                  path={`${baseRoute}/data`}
                  render={() => (
                    <Information
                      subject={subject}
                      refreshSubject={getSubject}
                      session={session}
                    />
                  )}
                />
                <Route
                  path={`${baseRoute}/communication`}
                  render={() => (
                    <Communication uuid={uuid} url={`${path}/communication`} />
                  )}
                />
                <Route
                  path={`${baseRoute}/cases`}
                  render={() => <Cases subject={subject} />}
                />
                <Route
                  exact
                  path={`${baseRoute}/map`}
                  render={() => <LocationModule uuid={uuid} type={type} />}
                />
                <Route
                  exact
                  path={`${baseRoute}/relationships`}
                  render={() => <Relationship uuid={uuid} />}
                />
                <Route
                  exact
                  path={`${baseRoute}/timeline`}
                  render={() => <ContactTimeline uuid={uuid} type={type} />}
                />
              </Switch>
            </div>
          </Panel>
        </PanelLayout>
      </div>
    </div>
  );
};

export default ContactView;
