// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useWebodfEditorStyles = makeStyles(() => ({
  menu: {
    backgroundColor: 'white',
    padding: 25,
    boxShadow: '3px 2px 10px 1px #b2b2b2',
    margin: '14px',
    position: 'sticky',
    top: 14,
    zIndex: 20,
  },
  titleContainer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  toolbarContainer: {
    padding: '5px 0',
    display: 'flex',
    justifyContent: 'space-around',
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-around',
    maxWidth: 900,
  },
  canvasContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
}));
