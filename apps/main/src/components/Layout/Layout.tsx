// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import NavigationBar from '../NavigationBar/NavigationBar';
import { useLayoutStyles } from './Layout.styles';

export const Layout: React.ComponentType<{}> = ({ children }) => {
  const classes = useLayoutStyles();

  return (
    <div className={classes.wrapper}>
      <div>
        <NavigationBar />
      </div>
      <div className={classes.content}>{children}</div>
    </div>
  );
};
