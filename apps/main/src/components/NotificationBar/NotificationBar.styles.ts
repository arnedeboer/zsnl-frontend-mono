// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useNotificationBarStyles = makeStyles(
  ({ palette: { sahara }, mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      borderTop: `1px solid ${greyscale.darker}`,
    },
    notificationWrapper: {
      borderBottom: `1px solid ${greyscale.darker}`,
      backgroundColor: sahara.lightest,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    notification: {
      margin: '10px 20px',
    },
    actionButton: {
      marginLeft: 20,
    },
    closeButton: {
      margin: 10,
      color: greyscale.darkest,
    },
  })
);
