// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SortDirectionType } from 'react-virtualized';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';

export type DataParams = {
  pageLength: number;
  sortBy: string | null;
  sortDirection: SortDirectionType | null;
  openServerErrorDialog: OpenServerErrorDialogType;
  pageNum?: number;
  assignment?: AssignmentType;
  search?: string;
};

export type DialogsType =
  | 'fileUpload'
  | 'delete'
  | 'addToCase'
  | 'properties'
  | 'preview'
  | 'caseCreate'
  | 'assign'
  | 'reject';

export type AssignmentType = 'all' | 'assigned' | 'assignedToMe' | 'unassigned';
