// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
//@ts-ignore
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { DocumentItemType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';

type AssignmentPropsType = {
  assignment?: DocumentItemType['assignment'];
};

const Assignment: React.FunctionComponent<AssignmentPropsType> = ({
  assignment,
}) => {
  if (assignment?.employee?.name) {
    return (
      <Tooltip key="assignment" title={assignment.employee.name}>
        <Icon size="extraSmall">{iconNames.person}</Icon>
      </Tooltip>
    );
  } else if (assignment?.role?.name && assignment?.department?.name) {
    return (
      <Tooltip
        key="assignment"
        title={assignment.department.name + ' / ' + assignment.role.name}
      >
        <Icon size="extraSmall">{iconNames.people}</Icon>
      </Tooltip>
    );
  } else {
    return null;
  }
};

export default Assignment;
