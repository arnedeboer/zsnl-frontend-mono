// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useIntakeStyles = makeStyles(() => {
  return {
    wrapper: {
      margin: 'auto',
      maxWidth: '98%',
    },
    popper: {
      '& > div': {
        maxWidth: 'none',
        borderRadius: 2,
        padding: 4,
      },
    },
    contentWrapper: {
      height: '100%',
      display: 'flex',
    },
    contentPanel: {
      height: '100%',
      display: 'flex',
      position: 'relative',
      flex: '1',
    },
    documentPreview: {
      width: '35%',
    },
    documentPreviewNoSelection: {
      padding: 20,
    },
    locked: {
      pointerEvents: 'none',
    },
  };
});
