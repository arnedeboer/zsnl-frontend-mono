// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint react/no-danger: 0 */
import React, { Fragment, useState, useRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import fecha from 'fecha';
import { useSelector } from 'react-redux';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
// @ts-ignore
import documentExplorerLocale from '@zaaksysteem/common/src/components/DocumentExplorer/locale/locale';
import Button from '@mintlab/ui/App/Material/Button';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { getColumns } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/getColumns';
import { isDocument } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/isDocument';
import useInfiniteScroll from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import { hasUserDocumentIntakerRole } from '@zaaksysteem/common/src/store/session/session.selectors';
import { InfiniteLoader, SizeInfo, SortDirectionType } from 'react-virtualized';
import {
  DirectoryItemType,
  DocumentItemType,
} from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import { useDocumentExplorerStyles } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/DocumentExplorer.style';
import { DocumentPreviewModal } from '@zaaksysteem/common/src/components/DocumentPreview';
import { DocumentUploadDialog } from '@zaaksysteem/common/src/components/dialogs/DocumentUploadDialog/DocumentUploadDialog';
import InfiniteTableLoader from '@zaaksysteem/common/src/components/InfiniteTableLoader/InfiniteTableLoader';
import Search from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/library/Search';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import IconButton from '@material-ui/core/IconButton';
import { CaseCreateDialog } from '@zaaksysteem/common/src/components/dialogs/CaseCreate/CaseCreateDialog';
import intakeLocale from './locale';
import { AddToCaseDialog } from './AddToCase/AddToCaseDialog';
import Menu from './components/Menu/Menu';
import { deleteDocument } from './Delete/deleteDocument';
import PropertiesDialog from './Properties/PropertiesDialog';
import AssignDialog from './Assign/AssignDialog';
import Assignment from './components/Assignment';
import RejectDialog from './Reject/RejectDialog';
import { useIntakeStyles } from './Intake.styles';
import Preview from './Preview/Preview';
import DocumentPreviewPanel from './DocumentPreviewPanel/DocumentPreviewPanel';
import {
  getSelectedPreview,
  isDialogOpen,
  getData,
  mergeListSelected,
  toggleSelected,
} from './Intake.library';
import { DialogsType, AssignmentType, DataParams } from './Intake.types';

const INITIAL_DIALOGS: Record<DialogsType, boolean> = {
  delete: false,
  addToCase: false,
  properties: false,
  fileUpload: false,
  preview: false,
  caseCreate: false,
  assign: false,
  reject: false,
};

const PAGE_LENGTH = 20;
const THRESHOLD = 5;
const REMOTE_ROW_COUNT = 99999;

/* eslint complexity: [2, 12] */
const IntakeDocuments: React.ComponentType = () => {
  const classes = useDocumentExplorerStyles();
  const intakeClasses = useIntakeStyles();
  const [t] = useTranslation();
  const [initialLoading, setInitialLoading] = useState<boolean>(true);
  const infiniteLoaderRef = useRef<InfiniteLoader>(null);
  const tableStyles = useSortableTableStyles();
  const [tableDimensions, setTableDimensions] = useState<SizeInfo | null>(null);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [sortBy, setSortBy] = useState<string | null>(null);
  const [sortDirection, setSortDirection] = useState<SortDirectionType | null>(
    null
  );
  const dialogRef = useRef(null);
  const [previewItem, setPreviewItem] = useState<null | DocumentItemType>(null);
  const isIntaker = useSelector(hasUserDocumentIntakerRole);
  const [assignment, setAssignment] = useState<AssignmentType>(
    isIntaker ? 'unassigned' : 'assigned'
  );
  const [search, setSearch] = useState<string>('');
  const [dialogs, setDialogs] = useState(INITIAL_DIALOGS);
  const dialogIsOpen = isDialogOpen(dialogs);
  const [selected, setSelected] = useState<string[]>([]);

  const { list, isRowLoaded, loadMoreRows, resetList, loading } =
    useInfiniteScroll<DocumentItemType, DataParams>({
      ref: infiniteLoaderRef,
      pageLength: PAGE_LENGTH,
      getData,
      getDataParams: {
        pageLength: PAGE_LENGTH,
        search,
        assignment,
        sortBy,
        sortDirection,
        openServerErrorDialog,
      },
    });

  useEffect(() => {
    (async function () {
      if (initialLoading) {
        await loadMoreRows({ startIndex: 1 });
        setInitialLoading(false);
      }
    })();
  }, []);

  useEffect(() => {
    resetList();
  }, [search, assignment, sortBy, sortDirection]);

  const dialogChange = (type: DialogsType, setting: boolean) =>
    setDialogs({
      ...dialogs,
      [type]: setting,
    });

  const { name, mimeType, description } = getColumns({
    t,
    classes,
  });

  const listWithSelected = mergeListSelected(list, selected);
  const selectedItems = listWithSelected.filter(
    (item: DocumentItemType) => item.selected
  );
  const selectedUuid = selectedItems.length ? selectedItems[0].uuid : '';
  const selectedPreview = getSelectedPreview(
    selectedItems as DocumentItemType[]
  );

  const assignmentChoices = [
    {
      label: t('Intake:assignment.assigned'),
      value: 'assigned',
    },
    isIntaker
      ? {
          label: t('Intake:assignment.assignedToMe'),
          value: 'assignedToMe',
        }
      : null,
    {
      label: t('Intake:assignment.unassigned'),
      value: 'unassigned',
    },
    {
      label: t('Intake:assignment.all'),
      value: 'all',
    },
  ].filter(Boolean) as ValueType<AssignmentType>[];

  const columns = [
    name({
      fileNameAction: item => {
        if (item.preview) {
          return (event: React.MouseEvent) => {
            event.preventDefault();
            event.stopPropagation();
            if (item.preview) {
              setPreviewItem(item);
              dialogChange('preview', true);
            }
          };
        }
      },
      /* eslint-disable-next-line */
      iconRenderer: (item, icon) => {
        return item?.thumbnail?.url ? (
          <Tooltip
            classes={{
              popper: intakeClasses.popper,
            }}
            enterDelay={300}
            title={<Preview url={item.thumbnail.url} />}
          >
            {icon}
          </Tooltip>
        ) : (
          icon
        );
      },
      tooltipSuffix: item =>
        item.document_number ? ' | ' + item.document_number : undefined,
      showNotAccepted: false,
      showRejected: true,
    }),
    {
      label: '',
      name: 'assignment',
      width: 40,
      showFromWidth: 600,
      disableSort: true,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: { rowData: DocumentItemType }) => (
        <Assignment assignment={rowData.assignment} />
      ),
    },
    mimeType(),
    description(),
    {
      label: t('DocumentExplorer:columns.modified.label'),
      name: 'modified',
      width: 126,
      showFromWidth: 650,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: { rowData: DocumentItemType }) => {
        const modifiedDate = rowData.modified
          ? fecha.format(new Date(rowData.modified), 'DD-M-YYYY HH:mm')
          : '';
        const modifiedBy = rowData.modifiedBy;
        const byLabel = modifiedBy ? t('Intake:by') : '';
        return (
          <div>
            <Tooltip
              title={`${modifiedDate} ${byLabel} ${modifiedBy || ''}`}
              placement={'top-start'}
              enterDelay={400}
            >
              <span>{modifiedDate}</span>
            </Tooltip>
          </div>
        );
      },
    },
    {
      label: '',
      name: 'open',
      width: 66,
      disableSort: true,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: { rowData: DocumentItemType }) => {
        if (isDocument(rowData) && rowData.preview?.url) {
          return (
            <Tooltip
              title={t('DocumentExplorer:columns.open.tooltip')}
              placement={'top-start'}
              enterDelay={400}
            >
              <IconButton
                title={t('DocumentExplorer:columns.open.label')}
                color="inherit"
                onClick={(event: any) => {
                  event.preventDefault();
                  event.stopPropagation();
                  top?.window.open(rowData.preview?.url, '_blank');
                }}
              >
                <Icon size="small" color="primary">
                  {iconNames.open_in_new}
                </Icon>
              </IconButton>
            </Tooltip>
          );
        }

        return <Fragment />;
      },
    },
  ];

  return (
    <I18nResourceBundle
      resource={documentExplorerLocale}
      namespace="DocumentExplorer"
    >
      <I18nResourceBundle resource={intakeLocale} namespace="Intake">
        <div className={classNames(classes.wrapper, intakeClasses.wrapper)}>
          <DocumentPreviewModal
            open={dialogs.preview}
            title={previewItem?.name || ''}
            url={previewItem?.preview?.url || ''}
            downloadUrl={previewItem?.download?.url}
            contentType={previewItem?.preview?.contentType || ''}
            onClose={() => dialogChange('preview', false)}
          />
          {ServerErrorDialog}
          <div
            className={classNames(classes.topBar, {
              [intakeClasses.locked]: dialogIsOpen,
            })}
          >
            <div className={classes.searchAndFiltersContainer}>
              <Search
                onChange={(value: string) => setSearch(value)}
                onClose={() => setSearch('')}
              />
              <Select
                value={assignment}
                name="filter-assignment"
                isMulti={false}
                onChange={(ev: any) => {
                  setAssignment(ev.target.value.value);
                }}
                choices={assignmentChoices}
                generic={true}
              />
            </div>

            <Button
              action={() => {
                if (window.top) {
                  window.top.location.href = '/zaak/intake?scope=documents';
                }
              }}
              presets={['text', 'primary', 'large']}
              classes={{
                label: classes.actionButtonLabel,
              }}
              title={t('Intake:linkToV1')}
            >
              {t('Intake:linkToV1')}
            </Button>

            <Button
              action={() => dialogChange('fileUpload', true)}
              presets={['text', 'primary', 'large']}
              classes={{
                label: classes.actionButtonLabel,
              }}
              title={t('DocumentExplorer:addFiles.button')}
            >
              {t('DocumentExplorer:addFiles.button')}
            </Button>
          </div>
          <div
            className={classNames(classes.toolbar, {
              [intakeClasses.locked]: dialogIsOpen,
            })}
          >
            <div className={classes.actionButtons}>
              <Menu
                list={listWithSelected}
                actions={{
                  addToCase: () => dialogChange('addToCase', true),
                  delete: () => dialogChange('delete', true),
                  properties: () => dialogChange('properties', true),
                  caseCreate: () => dialogChange('caseCreate', true),
                  assign: () => dialogChange('assign', true),
                  reject: () => dialogChange('reject', true),
                }}
              />

              <DocumentUploadDialog
                onConfirm={() => {
                  resetList();
                  dialogChange('fileUpload', false);
                }}
                onClose={() => dialogChange('fileUpload', false)}
                open={dialogs.fileUpload}
              />
              <AddToCaseDialog
                selectedDocuments={selectedItems as DocumentItemType[]}
                open={dialogs.addToCase}
                onClose={() => dialogChange('addToCase', false)}
                onConfirm={() => {
                  dialogChange('addToCase', false);
                  resetList();
                }}
                container={dialogRef?.current}
              />
              <CaseCreateDialog
                open={dialogs.caseCreate}
                selectedDocumentUuid={selectedUuid}
                onClose={() => dialogChange('caseCreate', false)}
                container={dialogRef?.current}
                contactChannel="post"
              />
              <ConfirmDialog
                open={dialogs.delete}
                onConfirm={async () => {
                  const promises = selectedItems.map(item =>
                    deleteDocument({
                      document_uuid: item.uuid || '',
                      reason: t('Intake:delete.reason'),
                    })
                  );
                  Promise.all(promises)
                    .then(() => {
                      resetList();
                    })
                    .catch(error => {
                      openServerErrorDialog(error);
                    })
                    .finally(() => {
                      dialogChange('delete', false);
                    });
                }}
                onClose={() => dialogChange('delete', false)}
                title={t('Intake:delete.title')}
                body={
                  <div
                    dangerouslySetInnerHTML={{
                      __html: t('Intake:delete.body'),
                    }}
                  />
                }
                container={dialogRef?.current}
              />
              {dialogs.properties && (
                <PropertiesDialog
                  uuid={selectedUuid}
                  onClose={() => dialogChange('properties', false)}
                  onConfirm={() => {
                    dialogChange('properties', false);
                    resetList();
                  }}
                  onServerError={openServerErrorDialog}
                  container={dialogRef?.current}
                />
              )}
              {dialogs.assign && (
                <AssignDialog
                  selectedDocuments={selectedItems}
                  open={true}
                  onClose={() => dialogChange('assign', false)}
                  onConfirm={() => {
                    dialogChange('assign', false);
                    resetList();
                  }}
                  container={dialogRef?.current}
                />
              )}

              <RejectDialog
                selectedDocuments={selectedItems}
                open={dialogs.reject}
                onClose={() => dialogChange('reject', false)}
                onConfirm={() => {
                  dialogChange('reject', false);
                  resetList();
                }}
              />
            </div>
          </div>
          <div className={intakeClasses.contentWrapper}>
            <DocumentPreviewPanel
              url={selectedPreview?.url}
              contentType={selectedPreview?.contentType}
              t={t}
            />
            <div className={intakeClasses.contentPanel} ref={dialogRef}>
              <div className={classes.table}>
                <InfiniteTableLoader
                  loading={loading}
                  tableDimensions={tableDimensions}
                  offset={150}
                />
                {ServerErrorDialog}
                <InfiniteLoader
                  isRowLoaded={isRowLoaded}
                  loadMoreRows={loadMoreRows}
                  rowCount={REMOTE_ROW_COUNT}
                  threshold={THRESHOLD}
                  ref={infiniteLoaderRef}
                >
                  {({ onRowsRendered, registerChild }) => (
                    <SortableTable
                      externalRef={registerChild}
                      onRowsRendered={onRowsRendered}
                      rows={listWithSelected}
                      //@ts-ignore
                      columns={columns}
                      rowHeight={50}
                      noRowsMessage={
                        initialLoading || loading
                          ? t('Intake:loadingMessage')
                          : t('Intake:noRowsMessage')
                      }
                      styles={tableStyles}
                      sortDirectionDefault="DESC"
                      sortInternal={false}
                      sorting="column"
                      onSort={(
                        sortBy: string,
                        sortDirection: SortDirectionType
                      ) => {
                        setSortBy(sortBy);
                        setSortDirection(sortDirection);
                      }}
                      onResize={(table: SizeInfo) => setTableDimensions(table)}
                      selectable={true}
                      onRowClick={({
                        rowData,
                      }: {
                        rowData: DirectoryItemType;
                      }) => {
                        if (isDocument(rowData)) {
                          toggleSelected(selected, setSelected, rowData);
                        }
                      }}
                    />
                  )}
                </InfiniteLoader>
              </div>
            </div>
          </div>
        </div>
      </I18nResourceBundle>
    </I18nResourceBundle>
  );
};

export default IntakeDocuments;
