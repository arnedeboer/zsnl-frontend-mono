// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const fetch = require('node-fetch');
const bucketHost = process.env.BUCKET_HOST;
const hostDict = new Map();

setInterval(() => {
  hostDict.clear();
}, 10 * 60 * 1000);

module.exports = (nonce, unsafe, reqHost) => {
  const whitelistMain = [
    'https://cdnjs.cloudflare.com/ajax/libs/dompurify/2.2.6/purify.min.js',
    'https://cdn.form.io/formiojs/formio.full.min.js',
    'https://fonts.googleapis.com/css2',
    'https://fonts.gstatic.com',
    'https://*.bus.koppel.app',
    'https://editor.zaaksysteem.demo.datamask.nl/',
    'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2',
    'https://cdn.form.io/formiojs/fonts/fontawesome-webfont.woff2',
    bucketHost,
  ]
    .filter(Boolean)
    .join(' ');

  const whitelistStyles = [
    'https://cdn.form.io/formiojs/formio.full.min.css',
    'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css',
    'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
    'https://fonts.googleapis.com/css',
  ].join(' ');

  const whitelistImages = [
    'https://geodata.nationaalgeoregister.nl/',
    'https://c1-powerpoint-15.cdn.office.net',
    'https://c1-excel-15.cdn.office.net',
    'https://c1-word-view-15.cdn.office.net',
    bucketHost,
  ]
    .filter(Boolean)
    .join(' ');

  const whitelistFrames = [
    'https://*.bus.koppel.app',
    'https://login.live.com/',
    'https://ffc-word-edit.officeapps.live.com/',
    'https://ffc-word-view.officeapps.live.com/',
    bucketHost,
  ]
    .filter(Boolean)
    .join(' ');

  const buildCspHeader = whitelistDomains =>
    [
      `default-src 'self' ${whitelistMain} ${whitelistDomains}`,
      `style-src 'self' ${
        unsafe ? "'unsafe-inline'" : `'nonce-${nonce}'`
      } ${whitelistStyles} ${whitelistDomains}`,
      `frame-src 'self' ${whitelistFrames} ${whitelistDomains}`,
      `img-src 'self' blob: data: ${whitelistImages} ${whitelistDomains}`,
      `object-src 'none'`,
      `frame-ancestors 'self'`,
      `require-trusted-types-for 'script'`,
      `connect-src 'self' data: *`,
      `base-uri 'self'`,
    ].join('; ');

  const cspReq =
    hostDict.get(reqHost) ||
    fetch('https://' + reqHost + '/csp')
      .then(rs => rs.json())
      .then(res =>
        res.csp_hosts
          .filter(hostname => hostname !== '*')
          .map(hostname => 'https://' + hostname)
          .join(' ')
      )
      .catch(() => '');

  hostDict.set(reqHost, cspReq);

  return cspReq.then(buildCspHeader);
};
